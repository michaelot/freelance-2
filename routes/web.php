<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "GuestController@index")->name("index")->middleware("isMember");
Route::get('/gigs', "GuestController@gigs")->name("gigs")->middleware("isMember");
Route::get('/categories', "GuestController@categories")->name("categories")->middleware("isMember");
Route::get('/gigListing', "GuestController@gigListing")->name("gigListing")->middleware("isMember");

Route::get('/testRave','RaveController@form')->name('paywithpaypal');
Route::post('/paypal','RaveController@postPaymentWithpaypal')->name('paypal');
Route::get('/paypal','RaveController@getPaymentStatus')->name('status');



Auth::routes(['verify' => true]);
Route::group(['middleware'=>['auth','isMember',]], function(){
    //User
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get("/","DashboardController@index")->name("dashboard.index");
    });
    Route::group(['prefix' => 'user'], function () {
        Route::get("/","UserController@index")->name("user.index");
    });

    Route::group(['prefix' => 'withdraw'], function () {
        Route::get("/","WithdrawalController@index")->name("withdraw.index");

        Route::post('/',"WithdrawalController@store")->name("withdraw.store");

        Route::put("/setting","WithdrawalController@storeSetting")->name("withdraw.setting");
        Route::post("/{id}/pay","WithdrawalController@postPaymentWithpaypal")->name("withdraw.pay");
        Route::get('/paypal','WithdrawalController@getPaymentStatus')->name('withdraw.status');
        Route::put("/{id}/cancel","WithdrawalController@cancel")->name("withdraw.cancel");
    });

    //payment
    Route::group(['prefix' => 'payment'], function () {
        Route::get("/","PaymentController@index")->name("payment.index");
    });

    //notification
    Route::group(['prefix' => 'notification'], function () {
        Route::get("/","NotificationController@index")->name("notification.index");
    });

    //message
    Route::group(['prefix' => 'message'], function () {
        Route::get('/',"MessageController@index")->name("message.index");
        Route::get('/listing',"MessageController@listing")->name("message.listing");
        Route::get('/{receiver_id}',"MessageController@show")->name("message");
        Route::get('/{receiver_id}/listing',"MessageController@messages");


        Route::post('/{id}',"MessageController@store")->name("message.store");
    });

    //account
    Route::group(['prefix'=>'/account',],function(){
        //get requests
        Route::get("/join","AccountController@one")->name("account.one");
        Route::get("/join/2","AccountController@two")->name("account.two");
        Route::get("/join/3","AccountController@three")->name("account.three");
        Route::get("/join/4","AccountController@create")->name("account.create");
        Route::get("/edit","AccountController@edit")->name("account.edit");
        Route::get("/{id}","AccountController@show")->name("account.show");
        Route::get("/{id}/gigs","AccountController@gigs")->name("account.gigs");
        Route::get("/{id}/reviews","AccountController@reviews")->name("account.reviews");
        Route::get("/","AccountController@index")->name("account.index");
        Route::get("/pages/edit","AccountController@editPages")->name("account.edit.pages");

        //post request
        Route::post('/', "AccountController@store")->name("account.store");
        Route::post("/{id}/update","AccountController@update")->name("account.update");

        Route::put("/pages/edit","PageController@update")->name("account.update.pages");

    });

    //gig
    Route::group(['prefix'=>'/gig'], function(){
        Route::get('/manage',"GigController@manage")->name("gig.manage");
        Route::get('/create',"GigController@create")->name("gig.create");
        Route::get('/{id}/edit',"GigController@edit")->name("gig.edit");
        Route::get('/{id}',"GigController@show")->name("gig.show");
        Route::get('/{id}/delete',"GigController@destroy")->name("gig.delete");
    //     //post Request
        Route::post('/',"GigController@store")->name("gig.store");
    //     //put Request
        Route::put('/{id}/update',"GigController@update")->name("gig.update");
    });

    // purchase
    Route::group(['prefix'=>'/purchase'],function(){
        Route::get('/',"PurchaseController@index")->name("purchase.index");
        Route::get("/{plan}/{id}","PaymentController@cart")->name("purchase.cart");
        Route::get("/checkout/{plan}/{id}","PaymentController@checkout")->name("purchase.checkout");
        Route::get("/charge/{id}/success","PaymentController@success")->name("payment.success");

        Route::post("/charge","PaymentController@charge")->name("payment.charge");
    });

    //order
    Route::group(['prefix' => '/order'], function () {
        Route::get("/","OrderController@index")->name("order.index");
        Route::get("/{id}","OrderController@show")->name("order.show");
        Route::get('/{id}/comments',"OrderController@comments")->name("order.comment.get");

        Route::post("{id}/requirements/","OrderController@storeRequirement")->name("order.requirement.save");
        Route::post('/{id}/comments',"OrderController@storeComment")->name("order.comment.save");
        Route::post('/{id}/delivery',"DeliveryController@store")->name("order.delivery.save");

        Route::put('/{id}/requirements',"OrderController@updateRequirement")->name("order.requirement.update");
        Route::put('/{id}/delivery',"DeliveryController@update")->name("order.delivery.update");
    });

    //review
    Route::group(['prefix' => '/review'], function () {
        Route::post('/{id}/{order_id}/{amount}',"ReviewController@store")->name("review.save");
        Route::put('/{id}',"ReviewController@update")->name("review.update");
    });

    //download
    Route::group(['prefix' => '/download'], function () {
        Route::get('/{at}/{path}/{name}',"DownloadController@download")->name("download");
    });
});

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');;
