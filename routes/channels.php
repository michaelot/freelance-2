<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
Broadcast::channel('comment.{order}', function ($user) {
    return Auth::check();
});
Broadcast::channel('message.{receiver_id}', function ($user,$receiver_id) {
    return (int) $user->id == (int) $receiver_id;
    // return true;
});
Broadcast::channel('list.{receiver_id}', function ($user,$receiver_id) {
    return (int) $user->id == (int) $receiver_id;
    // return true;
});
