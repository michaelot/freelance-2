<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRequirement extends Model
{
    public function requirement(){
        return $this->belongsTo(GigRequirement::class,"gig_requirement_id");
    }
    public function files(){
        return $this->hasMany(OrderRequirementFile::class);
    }
}
