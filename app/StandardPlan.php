<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StandardPlan extends Model
{
    public function extras(){
        return $this->hasMany(StandardExtra::class);
    }
    public function gig(){
        return $this->belongsTo(Gig::class);
    }
}
