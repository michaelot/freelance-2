<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryFile extends Model
{
    protected $fillable = [
        'name', 'path', 'delivery_id'
    ];
}
