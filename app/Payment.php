<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function gig(){
        return $this->belongsTo(Gig::class);
    }
    public function basicPlan(){
        return $this->belongsTo(BasicPlan::class,'plan_id');
    }
    public function standardPlan(){
        return $this->belongsTo(StandardPlan::class,'plan_id');
    }
    public function premiumPlan(){
        return $this->belongsTo(PremiumPlan::class,'plan_id');
    }
    public function order(){
        return $this->hasOne(Order::class);
    }
}
