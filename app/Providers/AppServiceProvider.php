<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;
use App\Message;
use App\User;
use App\Notification;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // $chats = $this->getMessages();
        // view()->share('chats',Auth::id());


        //Admin blade directive
        Blade::directive('admin', function () {
            return "<?php if (auth()->user() && auth()->user()->type == 'admin'){ ?>";
        });
        Blade::directive('elseAdmin', function () {
            return "<?php } else{ ?>";
        });
        Blade::directive('endAdmin', function () {
            return "<?php } ?>";
        });

        //
        Blade::directive('public', function () {
            return "<?php if (( auth()->user()->type == 'seller' || auth()->user()->type == 'regular')){ ?>";
            });
            Blade::directive('elsePublic', function () {
                return "<?php } else{ ?>";
            });
            Blade::directive('endPublic', function () {
                return "<?php } ?>";
            });

        //seller blade directive
        Blade::directive('seller', function () {
            return "<?php if (auth()->user() && auth()->user()->type == 'seller'){ ?>";
        });
        Blade::directive('elseSeller', function () {
            return "<?php } else{ ?>";
        });
        Blade::directive('endSeller', function () {
            return "<?php } ?>";
        });

        //regular blade directive
        Blade::directive('regular', function () {
            return "<?php if (auth()->user() && auth()->user()->type == 'regular'){ ?>";
        });
        Blade::directive('elseRegular', function () {
            return "<?php } else{ ?>";
        });
        Blade::directive('endRegular', function () {
            return "<?php } ?>";
        });

        Blade::directive('orderBuyer', function () {
            return "<?php if (auth()->user() && \$order->user->id == auth()->id() ){ ?>";
        });
        Blade::directive('elseOrderBuyer', function () {
            return "<?php } else{ ?>";
        });
        Blade::directive('endOrderBuyer', function () {
            return "<?php } ?>";
        });

        Blade::directive('orderSeller', function () {
            return "<?php if (auth()->user() && \$order->gig->user->id == auth()->id() ){ ?>";
        });
        Blade::directive('elseOrderSeller', function () {
            return "<?php } else{ ?>";
        });
        Blade::directive('endOrderSeller', function () {
            return "<?php } ?>";
        });


    }

    
}
