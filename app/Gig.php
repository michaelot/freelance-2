<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gig extends Model
{
    public function skills(){
        return $this->hasMany(GigSkill::class);
    }
    public function basicPlan(){
        return $this->hasOne(BasicPlan::class);
    }
    public function standardPlan(){
        return $this->hasOne(StandardPlan::class);
    }
    public function premiumPlan(){
        return $this->hasOne(PremiumPlan::class);
    }
    public function faqs(){
        return $this->hasMany(GigFaq::class);
    }
    public function images(){
        return $this->hasMany(GigImage::class);
    }
    public function requirements(){
        return $this->hasMany(GigRequirement::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function orders(){
        return $this->hasMany(Order::class);
    }
    public function category(){
        return $this->belongsTo(Category::class,"gig_category_id");
    }
    public function subCategory(){
        return $this->belongsTo(SubCategory::class,"gig_sub_category_id");
    }

}
