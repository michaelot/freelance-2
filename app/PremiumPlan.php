<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PremiumPlan extends Model
{
    public function extras(){
        return $this->hasMany(PremiumExtra::class);
    }
    public function gig(){
        return $this->belongsTo(Gig::class);
    }
}
