<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePage extends Model
{
    public function features(){
        return $this->hasMany(Feature::class);
    }
    public function wcu(){
        return $this->hasMany(WhyChooseUs::class);
    }
    public function testimonials(){
        return $this->hasMany(Testimonial::class);
    }
}
