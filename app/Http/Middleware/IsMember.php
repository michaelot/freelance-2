<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Message;
use App\Income;
use App\Withdrawal;
use App\SiteSetting;

class IsMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(auth()->user()->type == "seller" || auth()->user()->type == "regular" || auth()->user()->type == "admin"){

                View::share("chats",$this->getMessages());
                View::share("balance",$this->getBalance());
                View::share('currency',$this->currency());
                return $next($request);
            }
        }else{
            View::share('currency',$this->currency());
            View::share("chats",[]);
            View::share("balance",0);

            return $next($request);
        }
        // return redirect()->to("/");
    }

    public function getBalance(){
        $available = Income::where("user_id",auth()->id())->sum("amount") - Withdrawal::where("user_id",auth()->id())->where("status","Paid")->sum("amount");
        return $available;
    }

    public function currency(){
        $currency = SiteSetting::find(1)->currency;
        return $currency;
    }

    public function getMessages(){
        $me = Message::select('reciever_id')->addSelect('created_at')->where('sender_id',auth()->id())->orderBy('created_at','desc')->distinct()->addSelect('message')->get();
        $them = Message::select('sender_id')->addSelect('created_at')->where('reciever_id',auth()->id())->orderBy('created_at','desc')->distinct()->addSelect('message')->get();
        $theArr = [];
        if(sizeof($me) > 0){
            foreach($me as $m){
                $theArr[] = $m['reciever_id'];
            }
        }
        if(sizeof($them) > 0){
            foreach($them as $m){
                $theArr[] = $m['sender_id'];
            }
        }
        //  return $them;
        $users = DB::table('users')->whereIn('id',$theArr)->get();
        foreach($users as $key => $user){
            $users[$key]->message = DB::table('messages')->where("sender_id",$user->id)->orWhere("reciever_id",$user->id)->orderBy("created_at","desc")->first()->message;
            $users[$key]->at = DB::table('messages')->where("sender_id",$user->id)->orWhere("reciever_id",$user->id)->orderBy("created_at","desc")->first()->created_at;
        }
        return $users;
    }
    // public function getNotifications
}
