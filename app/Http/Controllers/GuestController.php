<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Gig;
use App\Category;
use App\Message;
use App\Notification;
use Illuminate\Support\Facades\View;
use App\HomePage;
use App\GigPage;
use Illuminate\Support\Facades\Input;
use App\CategoryPage;


class GuestController extends Controller
{
    public function index(){
        if(Auth::check()){
            return redirect()->route("account.index");
        }else{
            $data = HomePage::find(1);
            $gigs = Gig::inRandomOrder()->get();
            $categories = Category::all();
            foreach($gigs as $gig){
                $gig->impressions = $gig->impressions+1;
                $gig->save();
            }
            return view('pages.guest.index',[
                'gigs' => $gigs,
                "data" => $data,
                'categories' => $categories
            ]);
        }
    }

    public function gigs(){
        $ord ="desc";
        $data = GigPage::find(1);
        $categories = Category::all();
        if(Input::get("b") == "less viewed"){
            $ord = "asc";
        }
        $gigs = Gig::with(["user","category","basicPlan","premiumPlan","orders"])->orderBy("visits",$ord)->where("gig_category_id","like","%".Input::get('a')."%")->get();

        // return $gigs;
        foreach($gigs as $gig){
            $gig->impressions = $gig->impressions+1;
            $gig->save();
        }
        return view("pages.guest.gigs",[
            'gigs' => $gigs,
            "categories" => $categories,
            'data' => $data
        ]);

    }
    // public function gigListing(){
    //     $gigs = Gig::with(["user","category","basicPlan","premiumPlan","orders"])->where("category_id","like","%{".Input::get('a')."}%")->get();
    //     return $gigs;
    // }
    public function categories(){
        $categories = Category::all();
        $data = GigPage::find(1);
        return view("pages.guest.categories",[
            "categories" => $categories,
            'data' => $data
        ]);
    }
}
