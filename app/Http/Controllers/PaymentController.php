<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App\User;
use App\BasicPlan;
use App\StandardPlan;
use App\PremiumPlan;
use App\Payment;
use App\Order;
use App\SiteSetting;
use App\PaymentSetting;
use App\PaymenSetting;
use App\Notification;
use App\Gig;
use Mail;

class PaymentController extends Controller
{
    private $link = "https://quickadminpanel.com/blog/stripe-payments-in-laravel-the-ultimate-guide/";
    public function cart($plan_name,$id){
        $setting = SiteSetting::find(1);
        switch($plan_name){
            case "basic":
            $plan = BasicPlan::findorfail($id);
            break;

            case "standard":
            $plan = StandardPlan::findorfail($id);
            break;

            case "premium":
            $plan = PremiumPlan::findorfail($id);
            break;
        }
        return view("pages.purchase.cart",[
            'plan' => $plan,
            "setting"=>$setting,
            'type' => $plan_name
        ]);
    }
    public function checkout($plan_name,$id){
        $setting = SiteSetting::find(1);
        $paymentSetting = PaymentSetting::find(1);
        switch($plan_name){
            case "basic":
            $plan = BasicPlan::findorfail($id);
            break;

            case "standard":
            $plan = StandardPlan::findorfail($id);
            break;

            case "premium":
            $plan = PremiumPlan::findorfail($id);
            break;
        }
        return view("pages.purchase.checkout",[
            'plan' => $plan,
            "setting"=>$setting,
            "paymentSetting"=>$paymentSetting,
            'type' => $plan_name
        ]);
    }
    public function charge(Request $request)
    {
        $setting = SiteSetting::find(1);
        $paymentSetting = PaymentSetting::find(1);
        try {
            Stripe::setApiKey($paymentSetting->secret_key);

            $customer = Customer::create(array(
                'email' => $request->stripeEmail,
                'source' => $request->stripeToken
            ));

            $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount' => $request->input("plan_price"),
                'currency' => $setting->currency
            ));

            $payment = new Payment();
            $payment->user_id = auth()->id();
            $payment->gig_id = $request->input("gig_id");
            $payment->plan_type = $request->input("plan_type");
            $payment->plan_id = $request->input("plan_id");
            $payment->amount = $request->input("plan_price")/100;
            $payment->payment_status = "Success";
            $payment->save();

            $lastPayment = Payment::orderBy('created_at','desc')->first();

            $order = new Order();
            $order->payment_id = $lastPayment->id;
            $order->gig_id = $request->input("gig_id");
            $order->user_id = auth()->id();
            $order->save();

            $lastOrder = Order::orderBy('created_at','desc')->first();
            // return $lastPayment;

            $notify = new Notification;
            $notify->user_id = Gig::find($request->input("gig_id"))->user->id;
            $notify->notification = auth()->user()->name." Purchased your Gig";
            $notify->link = "/orders/$lastOrder->id";
            $notify->save();

            $notify = new Notification;
            $notify->user_id = auth()->id();
            $notify->notification = "Your purchase of '".Gig::find($request->input("gig_id"))->title."'"." was successful";
            $notify->link = "/orders/$lastOrder->id";
            $notify->save();

            Mail::raw(auth()->user()->name.' Purchased your Gig', function ($message) use ($request) {
                $message->to(Gig::find($request->input("gig_id"))->user->email);
            });

            return redirect()->route("payment.success",$lastOrder->id);
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
    public function success($id){
        $order = Order::findorfail($id);
        $setting = SiteSetting::find(1);
        // return $order->payment->premiumPlan;
        return view("pages.purchase.success",[
            'order' => $order,
            "setting"=>$setting,
        ]);
    }
    public function index(){
        $setting = SiteSetting::find(1);
        $payments  = Payment::all();
        return view("pages.payment.index",[
            "setting"=>$setting,
            "payments" => $payments
        ]);
    }
    public function payOff(){

    }
}
