<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderRequirement;
use App\OrderRequirementFile;
use App\OrderComment;
use App\Events\CommentSent;
use App\Income;
use App\SiteSetting;
use App\Notification;

class OrderController extends Controller
{
    public function index(){
        $setting = SiteSetting::find(1);
        if(auth()->user()->type == "seller"){
            $gigs = auth()->user()->gigs;
            $gig_id = [];
            foreach($gigs as $gig){
                $gig_id[] = $gig->id;
            }
            $orders = Order::whereIn("gig_id",$gig_id)->orderBy("created_at","desc")->get();
        }elseif(auth()->user()->type == "admin"){
            $orders = Order::all();
        }
        return view("pages.order.index",[
            "orders"=>$orders,
            "setting"=>$setting,
            ]);
    }
    public function storeRequirement(Request $request,$id){

        $order = Order::findorfail($id);
        $order->brief = $request->input("brief");
        $order->save();

        foreach($request->input("requirement") as $key => $value) {
            if($value['answer_type'] == "Text"){
                $orderRequirement = new OrderRequirement();
                $orderRequirement->order_id = $id;
                $orderRequirement->gig_requirement_id = $value["id"];
                $orderRequirement->answer_text = $value['answer'];
                $orderRequirement->save();
            }
            if($value['answer_type'] == "File"){
                $order_requirement_id = OrderRequirement::insertGetId(
                    ['order_id' => $id, 'gig_requirement_id' => $value["id"],]
                );
                $files = $request->file('file');
                $files2 = $files[$key] ? $files[$key] : array();
                if(sizeof($files2) > 0){
                    foreach($files2 as $file){
                        $file_name = $file->getClientOriginalName();
                        $store_name = time().$file_name;
                        $path = $file->storeAs('public/requirement files',$store_name);
                        $orderRequirementFile  = new OrderRequirementFile();
                        $orderRequirementFile->order_requirement_id = $order_requirement_id;
                        $orderRequirementFile->name = $file_name;
                        $orderRequirementFile->path = $store_name;
                        $orderRequirementFile->save();
                    }
                }
            }
        }

        $notify = new Notification;
        $notify->user_id = $order->gig->user->id;
        $notify->notification = auth()->user()->name." Submited Gig Requirement";
        $notify->link = "/orders/$order->id";
        $notify->save();

        return redirect()->to("/order/".$order->id);
    }

    public function updateRequirement(Request $request,$id){
        $order = Order::findorfail($id);
        $order->brief = $request->input("brief");
        $order->save();

        foreach($request->input("requirement") as $key => $value) {
            if($value['answer_type'] == "Text"){
                $orderRequirement = OrderRequirement::find($value["id"]);
                $orderRequirement->answer_text = $value['answer'];
                $orderRequirement->save();
            }
            if($value['answer_type'] == "File"){
                // if($request->hasFile('file')){
                    $files = $request->file('file');
                    $files2 = $files[$key] ? $files[$key] : array();
                    if(sizeof($files2) > 0){
                        foreach($files2 as $file){
                            $file_name = $file->getClientOriginalName();
                            $store_name = time().$file_name;
                            $path = $file->storeAs('public/requirement files',$store_name);
                            $orderRequirementFile  = new OrderRequirementFile();
                            $orderRequirementFile->order_requirement_id = $value["id"];
                            $orderRequirementFile->name = $file_name;
                            $orderRequirementFile->path = $store_name;
                            $orderRequirementFile->save();
                        }
                    }

                // }
            }
        }

        $notify = new Notification;
        $notify->user_id = $order->gig->user->id;
        $notify->notification = auth()->user()->name." Updated Gig Requirement";
        $notify->link = "/orders/$order->id";
        $notify->save();

        return redirect()->to("/order/".$order->id);
    }


    public function show($id){
        $currency = SiteSetting::find(1)->currency;
        $order = Order::findorfail($id);
        return view("pages.order.show",[
            "order" => $order,
            "currency"=>$currency,
        ]);
    }


    public function comments($id){
        $comments = OrderComment::where("order_id",$id)->with(['user','order'])->get();
        return $comments;
    }

    public function storeComment(Request $request,$id){
        $user = auth()->user();

        $order = Order::find($id);

        $comment  = new OrderComment();
        $comment->user_id = auth()->id();
        $comment->order_id = $id;
        $comment->comment = $request->input("comment");
        $comment->save();

        if(auth()->id() != $order->gig->user->id){
            $notify = new Notification;
            $notify->user_id = $order->gig->user->id;
            $notify->notification = auth()->user()->name." made a comment his order";
            $notify->link = "/orders/$order->id";
            $notify->save();
        }
        if(auth()->id() == $order->gig->user->id){
            $notify = new Notification;
            $notify->user_id = $order->user->id;
            $notify->notification = auth()->user()->name." made a comment Your order";
            $notify->link = "/orders/$order->id";
            $notify->save();
        }

        broadcast(new CommentSent($user, $comment,$order))->toOthers();

        return "Success";
    }
}
