<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use App\Payment as UserPayment;


use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Illuminate\Support\Facades\Input;
use App\Order;
use App\Notification;
use App\Gig;
use Mail;

use App\BillingInfo;
use App\Income;
use App\SiteSetting;
use App\PaymentSetting;
use App\Withdrawal;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App\User;

class WithdrawalController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $paymentSetting;
    public function __construct()
    {
        // parent::__construct();

        /** setup PayPal api context **/
        // $paymentSetting = PaymentSetting::find(1);
        // $paypal_conf = \Config::get('paypal');
        // $this->_api_context = new ApiContext(new OAuthTokenCredential($paymentSetting->publishable_key, $paymentSetting->secret_key));
        // $this->_api_context->setConfig($paypal_conf['settings']);
    }
    public function index(){
        $available = Income::where("user_id",auth()->id())->sum("amount") - Withdrawal::where("user_id",auth()->id())->where("status","Paid")->sum("amount");
        $currency = SiteSetting::find(1)->currency;
        $withdrawals = Withdrawal::orderBy("created_at","desc")->get();
        $publishable_key = PaymentSetting::find(1)->publishable_key;
        return  view("pages.withdrawal.index",[
            "available" => $available,
            "currency" => $currency,
            "withdrawals" => $withdrawals,
            "publishable_key" => $publishable_key
        ]);
    }
    public function storeSetting(Request $request){
        $billing = BillingInfo::where("user_id",auth()->id())->first();
        if($billing != null){
            $billing->secret_key = $request->input("secret_key");
            $billing->publishable_key = $request->input("client_id");
        }else{
            $billing = new BillingInfo();
            $billing->user_id = auth()->id();
            $billing->secret_key = $request->input("secret_key");
            $billing->publishable_key = $request->input("client_id");
        }
        $billing->save();
        return redirect()->back();
    }
    public function store(Request $request){

            $withdrawal = new Withdrawal();
            $withdrawal->user_id = auth()->id();
            if($request->input("withdrawal_type") == "available"){
                $withdrawal->amount = Income::where("user_id",auth()->id())->sum("amount") - Withdrawal::where("user_id",auth()->id())->sum("amount");
            }else{
                $withdrawal->amount = $request->input("amount");
            }
            $withdrawal ->save();
            return redirect()->back();
    }
    public function cancel($id){
        $item = Withdrawal::find($id);
        $item->status = "Canceled";
        $item->save();
        return redirect()->back();
    }

    public function postPaymentWithpaypal(Request $request)
    {
        $paymentSetting = PaymentSetting::find(1);
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(User::find($request->input('user_id'))->paymentInfo->publishable_key, User::find($request->input('user_id'))->paymentInfo->secret_key));
        $this->_api_context->setConfig($paypal_conf['settings']);

        $setting = SiteSetting::find(1);
        $paymentSetting = PaymentSetting::find(1);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

    	$item_1 = new Item();

        $item_1->setName($request->input("name")) /** item name **/
            ->setCurrency($setting->currency)
            ->setQuantity(1)
            ->setPrice($request->get('amount')); /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($setting->currency)
            ->setTotal($request->get('amount'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Paymnet to "'.$request->input("name").'"');

        Session::put('withdrawal_id',$request->input("id"));
        Session::put('user_id',$request->input("user_id"));

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('withdraw.status')) /** Specify return URL **/
            ->setCancelUrl(URL::route('withdraw.status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
            /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error','Connection timeout');
                return Redirect::route('withdraw.index');
                /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                /** $err_data = json_decode($ex->getData(), true); **/
                /** exit; **/
            } else {
                \Session::put('error','Some error occur, sorry for inconvenient');
                return Redirect::route('withdraw.index');
                /** die('Some error occur, sorry for inconvenient'); **/
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }

        \Session::put('error','Unknown error occurred');
    	return Redirect::route('withdraw.index');
    }

    public function getPaymentStatus()
    {
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(User::find(Session::get('user_id'))->paymentInfo->publishable_key, User::find(Session::get('user_id'))->paymentInfo->secret_key));
        $this->_api_context->setConfig($paypal_conf['settings']);
        /** Get the payment ID before session clear **/
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error','Payment failed');
            return Redirect::route('paywithpaypal');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/
        if ($result->getState() == 'approved') {

            /** it's all right **/
            /** Here Write your database logic like that insert record or value in database if you want **/

            \Session::put('success','Payment success');
            // return Session::get('withdrawal_id');

            $withdrawal = Withdrawal::findorfail(Session::get('withdrawal_id'));
            $withdrawal->status = "Paid";
            $withdrawal->save();
            // Redirect::route('withdraw.index');
            // $payment = new UserPayment();
            // $payment->user_id = auth()->id();
            // $payment->gig_id = Session::get('gig_id');
            // $payment->plan_type = Session::get('plan_type');
            // $payment->plan_id = Session::get('plan_id');
            // $payment->amount = Session::get('amount');
            // $payment->payment_status = "Success";
            // $payment->save();

            // $lastPayment = UserPayment::orderBy('created_at','desc')->first();

            // $order = new Order();
            // $order->payment_id = $lastPayment->id;
            // $order->gig_id = Session::get('gig_id');
            // $order->user_id = auth()->id();
            // $order->save();

            // $lastOrder = Order::orderBy('created_at','desc')->first();
            // // return $lastPayment;

            // $notify = new Notification;
            // $notify->user_id = Gig::find(Session::get('gig_id'))->user->id;
            // $notify->notification = auth()->user()->name." Purchased your Gig";
            // $notify->link = "/orders/$lastOrder->id";
            // $notify->save();

            // $notify = new Notification;
            // $notify->user_id = auth()->id();
            // $notify->notification = "Your purchase of '".Gig::find(Session::get('gig_id'))->title."'"." was successful";
            // $notify->link = "/orders/$lastOrder->id";
            // $notify->save();

            // Mail::raw(auth()->user()->name.' Purchased your Gig', function ($message) {
            //     $message->to(Gig::find(Session::get('gig_id'))->user->email);
            // });

            // return redirect()->route("payment.success",$lastOrder->id);

            return Redirect::route('withdraw.index');
        }
        \Session::put('error','Payment failed');

		return Redirect::route('paywithpaypal');
    }


    public function pay(Request $request, $id){
        $withdrawal = Withdrawal::findorfail($id);

        $setting = SiteSetting::find(1);
        try {
            Stripe::setApiKey("sk_test_7yEjX6Vly4vLJF9AMhCmULSS");

            $customer = Customer::create(array(
                'email' => $request->stripeEmail,
                'source' => $request->stripeToken
            ));

            $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount' => $withdrawal->amount,
                'currency' => $setting->currency
            ));
            $withdrawal->status = "Paid";
            $withdrawal->save();
            return redirect()->back();
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
}
