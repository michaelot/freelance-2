<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HomePage;
use App\Feature;
use App\WhyChooseUs;
use App\Testimonial;
use App\LoginPage;
use App\RegisterPage;
use App\CategoryPage;
use App\GigPage;


class PageController extends Controller
{
    public function update(Request $request){
        // return $reqx uest;
        $homePage = HomePage::findorfail(1);
        $homePage->heading = $request->input('heading');
        $homePage->sub_heading = $request->input('sub_heading');
        if($request->hasFile("background")){
            $this->validate($request,[
                "background" => "required|mimes:jpg,jpeg,png",
            ]);
            $image = $request->file('background');
            $image_name = $image->getClientOriginalName();
            $image_store_name = time().$image_name;
            $path = $image->storeAs('public/page background',$image_store_name);

            $homePage->background = $image_store_name;
        }

        if($request->hasFile("call_to_action_background")){
            $this->validate($request,[
                "call_to_action_background" => "required|mimes:jpg,jpeg,png",
            ]);
            $image = $request->file('call_to_action_background');
            $image_name = $image->getClientOriginalName();
            $image_store_name = time().$image_name;
            $path = $image->storeAs('public/page background',$image_store_name);

            $homePage->action_background = $image_store_name;

        }
        $homePage->action_heading = $request->input('call_to_action_heading');
        $homePage->action_sub_heading = $request->input('call_to_action_sub_heading');
        $homePage->save();

        foreach ($request->input("feature_title") as $key => $value) {
            if($value != ""){
                if($request->input("feature_type")[$key] == "old"){
                    $feature = Feature::find($request->input("feature_id")[$key]);
                    $feature->home_page_id = $homePage->id;
                    $feature->title = $value;
                    $feature->text = $request->input("feature_text")[$key];
                    if($request->hasFile("feature_icon")){
                        foreach ($request->file("feature_icon") as $t => $value) {
                            if($t == $key){
                            $file_name = $value->getClientOriginalName();
                                $store_name = time().$file_name;
                                $path = $value->storeAs('public/page icon',$store_name);
                                $feature->icon = $store_name;
                            }
                        }
                    }
                    $feature->save();
                }else{
                    $feature = new Feature;
                    $feature->home_page_id = $homePage->id;
                    $feature->title = $value;
                    $feature->text = $request->input("feature_text")[$key];
                    if($request->hasFile("feature_icon")){
                        foreach ($request->file("feature_icon") as $t => $value) {
                            if($t == $key){
                            $file_name = $value->getClientOriginalName();
                                $store_name = time().$file_name;
                                $path = $value->storeAs('public/page icon',$store_name);
                                $feature->icon = $store_name;
                            }
                        }
                    }
                    $feature->save();
                }
            }
        }
        // return $request->input("wcu_title");
        // foreach ($request->input("wcu_title") as $key => $value) {
        //     if($value != ""){
        //         if($request->input("wcu_type")[$key] == "old"){
        //             return $value;
        //             $wcu = WhyChooseUs::find($request->input("wcu_id")[$key]);
        //             $wcu->home_page_id = $homePage->id;
        //             $wcu->title = $value;
        //             $wcu->text = $request->input("wcu_text")[$key];
        //             if($request->hasFile("wcu_icon")){
        //                 foreach ($request->file("wcu_icon") as $t => $value) {
        //                     if($t == $key){
        //                     $file_name = $value->getClientOriginalName();
        //                         $store_name = time().$file_name;
        //                         $path = $value->storeAs('public/page icon',$store_name);
        //                         $wcu->icon = $store_name;
        //                     }
        //                 }
        //             }
        //             $wcu->save();
        //             // return "p";
        //         }else if($request->input("wcu_type")[$key] == "new"){
        //             return "Jp";
        //             $wcu = new WhyChooseUs();
        //             $wcu->home_page_id = $homePage->id;
        //             $wcu->title = $value;
        //             $wcu->text = $request->input("wcu_text")[$key];
        //             if($request->hasFile("wcu_icon")){
        //                 foreach ($request->file("wcu_icon") as $t => $value) {
        //                     if($t == $key){
        //                     $file_name = $value->getClientOriginalName();
        //                         $store_name = time().$file_name;
        //                         $path = $value->storeAs('public/page icon',$store_name);
        //                         $wcu->icon = $store_name;
        //                     }
        //                 }
        //             }
        //             $wcu->save();
        //         }
        //     }
        // }
            // return "jskdj";
        foreach ($request->input("wcu_title") as $key => $value) {
            if($value != ""){
                if($request->input("wcu_type")[$key] == "old"){
                    $wcu = WhyChooseUs::find($request->input("wcu_id")[$key]);
                    $wcu->home_page_id = $homePage->id;
                    $wcu->title = $value;
                    $wcu->text = $request->input("wcu_text")[$key];
                    if($request->hasFile("wcu_icon")){

                        foreach ($request->file("wcu_icon") as $t => $value2) {
                            if($t == $key){
                            $file_name = $value2->getClientOriginalName();
                                $store_name = time().$file_name;
                                $path = $value2->storeAs('public/page icon',$store_name);
                                $wcu->icon = $store_name;
                            }
                        }
                    }
                    $wcu->save();
                }else if($request->input("wcu_type")[$key] == "new"){
                    $wcu = new WhyChooseUs;
                    $wcu->home_page_id = $homePage->id;
                    $wcu->title = $value;
                    $wcu->text = $request->input("wcu_text")[$key];
                    if($request->hasFile("wcu_icon")){
                        foreach ($request->file("wcu_icon") as $t => $value2) {
                            if($t == $key){
                            $file_name = $value2->getClientOriginalName();
                                $store_name = time().$file_name;
                                $path = $value2->storeAs('public/page icon',$store_name);
                                $wcu->icon = $store_name;
                            }
                        }
                    }
                    $wcu->save();
                }
            }
        }

        foreach ($request->input("comment_name") as $key => $value) {
            if($value != ""){
                if($request->input("comment_type")[$key] == "old"){
                    $testimonial = Testimonial::find($request->input("comment_id")[$key]);
                    $testimonial->name = $value;
                    $testimonial->title = $request->input("comment_title")[$key];
                    $testimonial->text = $request->input("comment")[$key];
                    if($request->hasFile("comment_image")){
                        foreach ($request->file("comment_image") as $t => $value) {
                            if($t == $key){
                            $file_name = $value->getClientOriginalName();
                                $store_name = time().$file_name;
                                $path = $value->storeAs('public/page icon',$store_name);
                                $testimonial->image = $store_name;
                            }
                        }
                    }
                    $testimonial->save();
                }else{
                    $testimonial = new Testimonial();
                    $testimonial->home_page_id = $homePage->id;
                    $testimonial->name = $value;
                    $testimonial->title = $request->input("comment_title")[$key];
                    $testimonial->text = $request->input("comment")[$key];
                    if($request->hasFile("comment_image")){
                        foreach ($request->file("comment_image") as $t => $value) {
                            if($t == $key){
                            $file_name = $value->getClientOriginalName();
                                $store_name = time().$file_name;
                                $path = $value->storeAs('public/page icon',$store_name);
                                $testimonial->image = $store_name;
                            }
                        }
                    }
                    $testimonial->save();
                }
            }
        }

        $this->saveLoginPage($request);
        $this->saveRegisterPage($request);
        $this->saveGigsPage($request);
        $this->saveCategoryPage($request);

        return redirect()->back();
    }

    public function saveLoginPage($request){
        $login = LoginPage::find(1);
        $login->heading = $request->input("login_header");
        $login->sub_heading = $request->input("login_sub_header");

        if($request->hasFile("login_background")){
            $this->validate($request,[
                "login_background" => "required|mimes:jpg,jpeg,png",
            ]);
            $image = $request->file('login_background');
            $image_name = $image->getClientOriginalName();
            $image_store_name = time().$image_name;
            $path = $image->storeAs('public/page background',$image_store_name);

            $login->background = $image_store_name;
        }
        $login->save();
    }

    public function saveRegisterPage($request){
        $page = RegisterPage::find(1);
        $page->heading = $request->input("register_heading");
        $page->sub_heading = $request->input("register_sub_heading");

        if($request->hasFile("register_background")){
            $this->validate($request,[
                "register_background" => "required|mimes:jpg,jpeg,png",
            ]);
            $image = $request->file('register_background');
            $image_name = $image->getClientOriginalName();
            $image_store_name = time().$image_name;
            $path = $image->storeAs('public/page background',$image_store_name);

            $page->background = $image_store_name;
        }
        $page->save();
    }

    public function saveGigsPage($request){
        $page = GigPage::find(1);
        $page->heading = $request->input("gig_heading");
        $page->sub_heading = $request->input("gig_sub_heading");

        if($request->hasFile("gigs_background")){
            $this->validate($request,[
                "gigs_background" => "required|mimes:jpg,jpeg,png",
            ]);
            $image = $request->file('gigs_background');
            $image_name = $image->getClientOriginalName();
            $image_store_name = time().$image_name;
            $path = $image->storeAs('public/page background',$image_store_name);

            $page->background = $image_store_name;
        }
        $page->save();
    }

    public function saveCategoryPage($request){
        $page = CategoryPage::find(1);
        $page->heading = $request->input("category_heading");
        $page->sub_heading = $request->input("category_sub_heading");

        if($request->hasFile("category_background")){
            $this->validate($request,[
                "category_background" => "required|mimes:jpg,jpeg,png",
            ]);
            $image = $request->file('category_background');
            $image_name = $image->getClientOriginalName();
            $image_store_name = time().$image_name;
            $path = $image->storeAs('public/page background',$image_store_name);

            $page->background = $image_store_name;
        }
        $page->save();
    }
}
