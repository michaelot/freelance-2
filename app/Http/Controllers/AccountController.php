<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Seller;
use App\SellerSkill;
use App\User;
use App\Gig;
use App\Order;
use App\Payment;
use App\Income;
use App\MailSetting;
use App\PaymentSetting;
use App\SiteSetting;
use App\Category;
use App\SubCategory;
use App\Skill;
use App\Notification;
use Illuminate\Support\Facades\View;
use App\Message;
use App\HomePage;
use App\LoginPage;
use App\RegisterPage;
use App\GigPage;
use App\CategoryPage;
use Mail;


class AccountController extends Controller
{
    public function __constructor(){
        // $chats = $this->getMessages();
        // view()->share('chats', $chats);
    }
    public $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
    public $languages =  array(
        'aa' => 'Afar',
        'ab' => 'Abkhaz',
        'ae' => 'Avestan',
        'af' => 'Afrikaans',
        'ak' => 'Akan',
        'am' => 'Amharic',
        'an' => 'Aragonese',
        'ar' => 'Arabic',
        'as' => 'Assamese',
        'av' => 'Avaric',
        'ay' => 'Aymara',
        'az' => 'Azerbaijani',
        'ba' => 'Bashkir',
        'be' => 'Belarusian',
        'bg' => 'Bulgarian',
        'bh' => 'Bihari',
        'bi' => 'Bislama',
        'bm' => 'Bambara',
        'bn' => 'Bengali',
        'bo' => 'Tibetan Standard, Tibetan, Central',
        'br' => 'Breton',
        'bs' => 'Bosnian',
        'ca' => 'Catalan; Valencian',
        'ce' => 'Chechen',
        'ch' => 'Chamorro',
        'co' => 'Corsican',
        'cr' => 'Cree',
        'cs' => 'Czech',
        'cu' => 'Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic',
        'cv' => 'Chuvash',
        'cy' => 'Welsh',
        'da' => 'Danish',
        'de' => 'German',
        'dv' => 'Divehi; Dhivehi; Maldivian;',
        'dz' => 'Dzongkha',
        'ee' => 'Ewe',
        'el' => 'Greek, Modern',
        'en' => 'English',
        'eo' => 'Esperanto',
        'es' => 'Spanish; Castilian',
        'et' => 'Estonian',
        'eu' => 'Basque',
        'fa' => 'Persian',
        'ff' => 'Fula; Fulah; Pulaar; Pular',
        'fi' => 'Finnish',
        'fj' => 'Fijian',
        'fo' => 'Faroese',
        'fr' => 'French',
        'fy' => 'Western Frisian',
        'ga' => 'Irish',
        'gd' => 'Scottish Gaelic; Gaelic',
        'gl' => 'Galician',
        'gn' => 'GuaranÃƒÂ­',
        'gu' => 'Gujarati',
        'gv' => 'Manx',
        'ha' => 'Hausa',
        'he' => 'Hebrew (modern)',
        'hi' => 'Hindi',
        'ho' => 'Hiri Motu',
        'hr' => 'Croatian',
        'ht' => 'Haitian; Haitian Creole',
        'hu' => 'Hungarian',
        'hy' => 'Armenian',
        'hz' => 'Herero',
        'ia' => 'Interlingua',
        'id' => 'Indonesian',
        'ie' => 'Interlingue',
        'ig' => 'Igbo',
        'ii' => 'Nuosu',
        'ik' => 'Inupiaq',
        'io' => 'Ido',
        'is' => 'Icelandic',
        'it' => 'Italian',
        'iu' => 'Inuktitut',
        'ja' => 'Japanese (ja)',
        'jv' => 'Javanese (jv)',
        'ka' => 'Georgian',
        'kg' => 'Kongo',
        'ki' => 'Kikuyu, Gikuyu',
        'kj' => 'Kwanyama, Kuanyama',
        'kk' => 'Kazakh',
        'kl' => 'Kalaallisut, Greenlandic',
        'km' => 'Khmer',
        'kn' => 'Kannada',
        'ko' => 'Korean',
        'kr' => 'Kanuri',
        'ks' => 'Kashmiri',
        'ku' => 'Kurdish',
        'kv' => 'Komi',
        'kw' => 'Cornish',
        'ky' => 'Kirghiz, Kyrgyz',
        'la' => 'Latin',
        'lb' => 'Luxembourgish, Letzeburgesch',
        'lg' => 'Luganda',
        'li' => 'Limburgish, Limburgan, Limburger',
        'ln' => 'Lingala',
        'lo' => 'Lao',
        'lt' => 'Lithuanian',
        'lu' => 'Luba-Katanga',
        'lv' => 'Latvian',
        'mg' => 'Malagasy',
        'mh' => 'Marshallese',
        'mi' => 'Maori',
        'mk' => 'Macedonian',
        'ml' => 'Malayalam',
        'mn' => 'Mongolian',
        'mr' => 'Marathi (Mara?hi)',
        'ms' => 'Malay',
        'mt' => 'Maltese',
        'my' => 'Burmese',
        'na' => 'Nauru',
        'nb' => 'Norwegian BokmÃƒÂ¥l',
        'nd' => 'North Ndebele',
        'ne' => 'Nepali',
        'ng' => 'Ndonga',
        'nl' => 'Dutch',
        'nn' => 'Norwegian Nynorsk',
        'no' => 'Norwegian',
        'nr' => 'South Ndebele',
        'nv' => 'Navajo, Navaho',
        'ny' => 'Chichewa; Chewa; Nyanja',
        'oc' => 'Occitan',
        'oj' => 'Ojibwe, Ojibwa',
        'om' => 'Oromo',
        'or' => 'Oriya',
        'os' => 'Ossetian, Ossetic',
        'pa' => 'Panjabi, Punjabi',
        'pi' => 'Pali',
        'pl' => 'Polish',
        'ps' => 'Pashto, Pushto',
        'pt' => 'Portuguese',
        'qu' => 'Quechua',
        'rm' => 'Romansh',
        'rn' => 'Kirundi',
        'ro' => 'Romanian, Moldavian, Moldovan',
        'ru' => 'Russian',
        'rw' => 'Kinyarwanda',
        'sa' => 'Sanskrit (Sa?sk?ta)',
        'sc' => 'Sardinian',
        'sd' => 'Sindhi',
        'se' => 'Northern Sami',
        'sg' => 'Sango',
        'si' => 'Sinhala, Sinhalese',
        'sk' => 'Slovak',
        'sl' => 'Slovene',
        'sm' => 'Samoan',
        'sn' => 'Shona',
        'so' => 'Somali',
        'sq' => 'Albanian',
        'sr' => 'Serbian',
        'ss' => 'Swati',
        'st' => 'Southern Sotho',
        'su' => 'Sundanese',
        'sv' => 'Swedish',
        'sw' => 'Swahili',
        'ta' => 'Tamil',
        'te' => 'Telugu',
        'tg' => 'Tajik',
        'th' => 'Thai',
        'ti' => 'Tigrinya',
        'tk' => 'Turkmen',
        'tl' => 'Tagalog',
        'tn' => 'Tswana',
        'to' => 'Tonga (Tonga Islands)',
        'tr' => 'Turkish',
        'ts' => 'Tsonga',
        'tt' => 'Tatar',
        'tw' => 'Twi',
        'ty' => 'Tahitian',
        'ug' => 'Uighur, Uyghur',
        'uk' => 'Ukrainian',
        'ur' => 'Urdu',
        'uz' => 'Uzbek',
        've' => 'Venda',
        'vi' => 'Vietnamese',
        'vo' => 'VolapÃƒÂ¼k',
        'wa' => 'Walloon',
        'wo' => 'Wolof',
        'xh' => 'Xhosa',
        'yi' => 'Yiddish',
        'yo' => 'Yoruba',
        'za' => 'Zhuang, Chuang',
        'zh' => 'Chinese',
        'zu' => 'Zulu',
    );
    public $currencies = array (
        'ALL' => 'Albania Lek',
        'AFN' => 'Afghanistan Afghani',
        'ARS' => 'Argentina Peso',
        'AWG' => 'Aruba Guilder',
        'AUD' => 'Australia Dollar',
        'AZN' => 'Azerbaijan New Manat',
        'BSD' => 'Bahamas Dollar',
        'BBD' => 'Barbados Dollar',
        'BDT' => 'Bangladeshi taka',
        'BYR' => 'Belarus Ruble',
        'BZD' => 'Belize Dollar',
        'BMD' => 'Bermuda Dollar',
        'BOB' => 'Bolivia Boliviano',
        'BAM' => 'Bosnia and Herzegovina Convertible Marka',
        'BWP' => 'Botswana Pula',
        'BGN' => 'Bulgaria Lev',
        'BRL' => 'Brazil Real',
        'BND' => 'Brunei Darussalam Dollar',
        'KHR' => 'Cambodia Riel',
        'CAD' => 'Canada Dollar',
        'KYD' => 'Cayman Islands Dollar',
        'CLP' => 'Chile Peso',
        'CNY' => 'China Yuan Renminbi',
        'COP' => 'Colombia Peso',
        'CRC' => 'Costa Rica Colon',
        'HRK' => 'Croatia Kuna',
        'CUP' => 'Cuba Peso',
        'CZK' => 'Czech Republic Koruna',
        'DKK' => 'Denmark Krone',
        'DOP' => 'Dominican Republic Peso',
        'XCD' => 'East Caribbean Dollar',
        'EGP' => 'Egypt Pound',
        'SVC' => 'El Salvador Colon',
        'EEK' => 'Estonia Kroon',
        'EUR' => 'Euro Member Countries',
        'FKP' => 'Falkland Islands (Malvinas) Pound',
        'FJD' => 'Fiji Dollar',
        'GHC' => 'Ghana Cedis',
        'GIP' => 'Gibraltar Pound',
        'GTQ' => 'Guatemala Quetzal',
        'GGP' => 'Guernsey Pound',
        'GYD' => 'Guyana Dollar',
        'HNL' => 'Honduras Lempira',
        'HKD' => 'Hong Kong Dollar',
        'HUF' => 'Hungary Forint',
        'ISK' => 'Iceland Krona',
        'INR' => 'India Rupee',
        'IDR' => 'Indonesia Rupiah',
        'IRR' => 'Iran Rial',
        'IMP' => 'Isle of Man Pound',
        'ILS' => 'Israel Shekel',
        'JMD' => 'Jamaica Dollar',
        'JPY' => 'Japan Yen',
        'JEP' => 'Jersey Pound',
        'KZT' => 'Kazakhstan Tenge',
        'KPW' => 'Korea (North) Won',
        'KRW' => 'Korea (South) Won',
        'KGS' => 'Kyrgyzstan Som',
        'LAK' => 'Laos Kip',
        'LVL' => 'Latvia Lat',
        'LBP' => 'Lebanon Pound',
        'LRD' => 'Liberia Dollar',
        'LTL' => 'Lithuania Litas',
        'MKD' => 'Macedonia Denar',
        'MYR' => 'Malaysia Ringgit',
        'MUR' => 'Mauritius Rupee',
        'MXN' => 'Mexico Peso',
        'MNT' => 'Mongolia Tughrik',
        'MZN' => 'Mozambique Metical',
        'NAD' => 'Namibia Dollar',
        'NPR' => 'Nepal Rupee',
        'ANG' => 'Netherlands Antilles Guilder',
        'NZD' => 'New Zealand Dollar',
        'NIO' => 'Nicaragua Cordoba',
        'NGN' => 'Nigeria Naira',
        'NOK' => 'Norway Krone',
        'OMR' => 'Oman Rial',
        'PKR' => 'Pakistan Rupee',
        'PAB' => 'Panama Balboa',
        'PYG' => 'Paraguay Guarani',
        'PEN' => 'Peru Nuevo Sol',
        'PHP' => 'Philippines Peso',
        'PLN' => 'Poland Zloty',
        'QAR' => 'Qatar Riyal',
        'RON' => 'Romania New Leu',
        'RUB' => 'Russia Ruble',
        'SHP' => 'Saint Helena Pound',
        'SAR' => 'Saudi Arabia Riyal',
        'RSD' => 'Serbia Dinar',
        'SCR' => 'Seychelles Rupee',
        'SGD' => 'Singapore Dollar',
        'SBD' => 'Solomon Islands Dollar',
        'SOS' => 'Somalia Shilling',
        'ZAR' => 'South Africa Rand',
        'LKR' => 'Sri Lanka Rupee',
        'SEK' => 'Sweden Krona',
        'CHF' => 'Switzerland Franc',
        'SRD' => 'Suriname Dollar',
        'SYP' => 'Syria Pound',
        'TWD' => 'Taiwan New Dollar',
        'THB' => 'Thailand Baht',
        'TTD' => 'Trinidad and Tobago Dollar',
        'TRY' => 'Turkey Lira',
        'TRL' => 'Turkey Lira',
        'TVD' => 'Tuvalu Dollar',
        'UAH' => 'Ukraine Hryvna',
        'GBP' => 'United Kingdom Pound',
        'USD' => 'United States Dollar',
        'UYU' => 'Uruguay Peso',
        'UZS' => 'Uzbekistan Som',
        'VEF' => 'Venezuela Bolivar',
        'VND' => 'Viet Nam Dong',
        'YER' => 'Yemen Rial',
        'ZWD' => 'Zimbabwe Dollar'
    );
    public function one()
    {
        return view("pages.account.one");
    }
    public function two(){
        return view("pages.account.two");
    }
    public function three(){
        return view("pages.account.three");
    }
    public function index(){

        // return $chats;
        $newGigs = Gig::orderBy("created_at",'desc')->take(12)->get();
        $random1 = Gig::inRandomOrder()->take(16)->get();
        $random2 = Gig::inRandomOrder()->take(16)->get();
        $categories = Category::all();


        foreach($newGigs as $gig){
            $gig->impressions = $gig->impressions+1;
            $gig->save();
        }
        foreach($random1 as $gig){
            $gig->impressions = $gig->impressions+1;
            $gig->save();
        }
        foreach($random2 as $gig){
            $gig->impressions = $gig->impressions+1;
            $gig->save();
        }
        return view("pages.account.index",[
            'newGigs' => $newGigs,
            'random1' => $random1,
            'random2' => $random2,
            "categories" => $categories,

        ]);
    }
    public function create(){
        $categories = Category::all();
        $skills = Skill::all();
        return view("pages.account.create",[
            'countries' => $this->countries,
            'languages' => $this->languages,
            'skills'=> $skills,
            'categories' => $categories
        ]);
    }
    public function store(Request $request){
        $this->validate($request,[
            "profile_image" => "required|mimes:jpg,jpeg,png,bitmap",
            "cover_image" => "required|mimes:jpg,jpeg,png,bitmap"
        ]);

        //store Profile Image
        $profile_image = $request->file('profile_image');
        $profile_image_file_name = $profile_image->getClientOriginalName();
        $profile_image_store_name = time().$profile_image_file_name;
        $path = $profile_image->storeAs('public/profile images',$profile_image_store_name);

        //store Cover Image
        $cover_image = $request->file('cover_image');
        $cover_image_file_name = $cover_image->getClientOriginalName();
        $cover_image_store_name = time().$cover_image_file_name;
        $path = $cover_image->storeAs('public/cover images',$cover_image_store_name);

        $seller = new Seller();
        $seller->profile_image = $profile_image_store_name;
        $seller->cover_image = $cover_image_store_name;
        $seller->user_id = auth()->id();
        $seller->full_name = $request->input('full_name') ;
        $seller->description = $request->input('description') ;
        $seller->tagline = $request->input('tagline');
        $seller->country = $request->input('country');
        $seller->language  = $request->input('language');
        $seller->website = $request->input('website') ;
        $seller->experience= $request->input('experience') ;
        $seller->occupation = $request->input('occupation') ;
        $seller->facebook = $request->input('facebook') ;
        $seller->google = $request->input('google');
        $seller->instagram = $request->input('instagram');
        $seller->twitter = $request->input('twitter');
        $seller->github = $request->input('github');
        $seller->dribble = $request->input('dribble');
        $seller->behance = $request->input('behance');
        $seller->stack_overflow = $request->input('stack_overflow');
        $seller->vimeo = $request->input('vimeo');
        foreach($request->input("skills") as $skill){
            $seller_skill = new SellerSkill();
            $seller_skill->user_id = auth()->id();
            $seller_skill->name = $skill;
            $seller_skill->save();
        }
        $seller->save();
        $user = User::findorfail(auth()->id());
        $user->type = "seller";
        $user->save();

        broadcast(new NewUserEvent($user));


        // Mail::raw('Welcome To Freelance App', function ($message) {
        //     $message->to(auth()->user()->email);
        // });
        Mail::send('pages.email.welcome',["name"=>auth()->user()->name],function ($message) {
            $message->to(auth()->user()->email);
            $message->subject('Welcome To Freelance App');
        });

        return redirect()->route("account.index");
    }
    public function show($id){
        $user = User::findorfail($id);
        $orders = Order::where('gig_id',$id)->get();
        return view("pages.account.show.index",[
            'orders' =>$orders,
            'user' => $user
        ]);
    }
    public function gigs($id){
        $user = User::findorfail($id);
        $orders = Order::where('gig_id',$id)->get();
        return view("pages.account.show.gigs",[
            'orders' =>$orders,
            'user' => $user
        ]);
    }
    public function reviews($id){
        $user = User::findorfail($id);
        $orders = Order::where('gig_id',$id)->get();
        return view("pages.account.show.reviews",[
            'orders' =>$orders,
            'user' => $user,
        ]);
    }
    public function edit(){
        if(auth()->user()->type == "seller" || auth()->user()->type == "admin"){
            $account = Seller::where("user_id",auth()->id())->first();
            $siteSetting = SiteSetting::find(1);
            $mailSetting = MailSetting::find(1);
            $paymentSetting = PaymentSetting::find(1);
            $categories = Category::all();
            $subCategories = SubCategory::all();
            $skills = skill::all();

            return view("pages.account.edit",[
                "account"=>$account,
                "countries" => $this->countries,
                'languages' => $this->languages,
                'currencies' => $this->currencies,
                'siteSetting' => $siteSetting,
                'mailSetting' => $mailSetting,
                'paymentSetting' => $paymentSetting,
                'categories' => $categories,
                'subCategories' => $subCategories,
                'skills' => $skills,
            ]);
        }else{
            return view("pages.account.edit");
        }
    }
    public function update(Request $request,$id){
        $user = User::findorfail($id);
        if($user->type != "admin"){
            $user->name = $request->input("name");
        }
        $user->email = $request->input("email");
        if(!empty($request->input("password"))){
            $this->validate($request,[
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            $user->password = Hash::make($request->input("password"));
        }
        $user->save();

        if($user->type == "admin"){
            $siteSetting = SiteSetting::findorfail(1);
            $siteSetting->name = $request->input("name");
            $siteSetting->tagline = $request->input("tagline");
            $siteSetting->country = $request->input("country");
            $siteSetting->language = $request->input("language");
            $siteSetting->currency = $request->input("currency");
            $siteSetting->save();

            $mailSetting = MailSetting::findorfail(1);
            $mailSetting->username = $request->input("mail_username");
            $mailSetting->from_username = $request->input("mail_from_username");
            $mailSetting->email = $request->input("from_mail_address");
            $mailSetting->encryption = $request->input("mail_encryption");
            $mailSetting->password = $request->input("mail_password");
            $mailSetting->port = $request->input("mail_port");
            $mailSetting->save();

            $paymentSetting = PaymentSetting::findorfail(1);
            $paymentSetting->secret_key = $request->input("secret_key");
            $paymentSetting->publishable_key = $request->input("client_id");
            $paymentSetting->tax = $request->input("tax");
            $paymentSetting->save();


            $categories = Category::all();
            // foreach($categories as $category){
            //     $category->delete();
            // }
            foreach($request->input("categories") as $key => $name){
                if(!empty($name)){
                    if($request->input("category_type")[$key] == "No"){
                        $category = Category::find($request->input("category_id")[$key]);
                        if($request->has("category_delete")){
                            foreach($request->input("category_delete") as $value){
                                if($value == $category->id){
                                    $category->delete();
                                    // return $value;
                                }
                            }
                        }else{
                            $category->name = $name;
                            if($request->hasFile("category_icon")){
                                foreach ($request->file("category_icon") as $t => $value) {
                                    if($t == $key){
                                    //    dd($value);
                                    $file_name = $value->getClientOriginalName();
                                        $store_name = time().$file_name;
                                        $path = $value->storeAs('public/category icon',$store_name);
                                        $category->icon = $store_name;
                                    }
                                }
                            }

                            if($request->hasFile("category_image")){
                                foreach ($request->file("category_image") as $t => $value) {
                                    if($t == $key){
                                    //    dd($value);
                                    $file_name = $value->getClientOriginalName();
                                        $store_name = time().$file_name;
                                        $path = $value->storeAs('public/category images',$store_name);
                                        $category->image = $store_name;
                                    }
                                }
                            }
                            $category->save();
                        }
                    }else{
                        $category = new Category();
                        $category->name = $name;
                        if($request->hasFile("category_icon")){
                            foreach ($request->file("category_icon") as $t => $value) {
                                if($t == $key){
                                //    dd($value);
                                   $file_name = $value->getClientOriginalName();
                                    $store_name = time().$file_name;
                                    $path = $value->storeAs('public/category icon',$store_name);
                                    $category->icon = $store_name;
                                }
                            }
                        }
                        if($request->hasFile("category_image")){
                            foreach ($request->file("category_image") as $t => $value) {
                                if($t == $key){
                                //    dd($value);
                                   $file_name = $value->getClientOriginalName();
                                    $store_name = time().$file_name;
                                    $path = $value->storeAs('public/category images',$store_name);
                                    $category->image = $store_name;
                                }
                            }
                        }
                        $category->save();
                    }
                }
            }

            $sub_categories = SubCategory::all();
            // foreach($sub_categories as $sub_category){
            //     $sub_category->delete();
            // }
            foreach($request->input("sub_categories") as $key => $name){
                if(!empty($name)){
                    if($request->input("sub_category_type")[$key] == "No"){
                        $sub_category = SubCategory::find($request->input("sub_category_id")[$key]);
                        $sub_category->name = $name;
                        $sub_category->save();
                    }else{
                        $sub_category = new SubCategory();
                        $sub_category->name = $name;
                        $sub_category->save();
                    }
                }
            }

            $skills = skill::all();
            // foreach($skills as $skill){
            //     $skill->delete();
            // }
            foreach($request->input("skills") as $key => $name){
                if(!empty($name)){
                    if($request->input("skill_type")[$key] == "No"){
                        $skill = Skill::find($request->input("skill_id")[$key]);
                        $skill->name = $name;
                        $skill->save();
                    }else{
                        $skill = new skill();
                        $skill->name = $name;
                        $skill->save();
                    }

                }
            }
        }

        if($user->type == "seller"){
            $seller = $user->seller;

            if($request->hasFile('profile_image')){
                $this->validate($request,[
                    "profile_image" => "required|mimes:jpg,jpeg,png,bitmap",
                ]);
                $profile_image = $request->file('profile_image');
                $profile_image_file_name = $profile_image->getClientOriginalName();
                $profile_image_store_name = time().$profile_image_file_name;
                $path = $profile_image->storeAs('public/profile images',$profile_image_store_name);
                $seller->profile_image = $profile_image_store_name;
            }
            if($request->hasFile('cover_image')){
                $this->validate($request,[
                    "cover_image" => "required|mimes:jpg,jpeg,png,bitmap"
                ]);
                $cover_image = $request->file('cover_image');
                $cover_image_file_name = $cover_image->getClientOriginalName();
                $cover_image_store_name = time().$cover_image_file_name;
                $path = $cover_image->storeAs('public/cover images',$cover_image_store_name);
                $seller->cover_image = $cover_image_store_name;
            }
            $seller->user_id = auth()->id();
            $seller->full_name = $request->input('full_name') ;
            $seller->description = $request->input('description') ;
            $seller->tagline = $request->input('tagline');
            $seller->country = $request->input('country');
            $seller->language  = $request->input('language');
            $seller->website = $request->input('website') ;
            $seller->experience= $request->input('experience') ;
            $seller->occupation = $request->input('occupation') ;
            $seller->facebook = $request->input('facebook') ;
            $seller->google = $request->input('google');
            $seller->instagram = $request->input('instagram');
            $seller->twitter = $request->input('twitter');
            $seller->github = $request->input('github');
            $seller->dribble = $request->input('dribble');
            $seller->behance = $request->input('behance');
            $seller->stack_overflow = $request->input('stack_overflow');
            $seller->vimeo = $request->input('vimeo');
            foreach($user->skills as $skill){
                $skill->delete();
            }
            foreach($request->input("skills") as $skill){
                $seller_skill = new SellerSkill();
                $seller_skill->user_id = auth()->id();
                $seller_skill->name = $skill;
                $seller_skill->save();
            }
            $seller->save();
        }

        return redirect()->back();
    }
    public function editPages(){
        $account = Seller::where("user_id",auth()->id())->first();
        $siteSetting = SiteSetting::findorfail(1);
        $mailSetting = MailSetting::findorfail(1);
        $paymentSetting = PaymentSetting::findorfail(1);
        $categories = Category::all();
        $subCategories = SubCategory::all();
        $skills = skill::all();

        $home = HomePage::find(1);
        $login = LoginPage::find(1);
        $register = RegisterPage::find(1);
        $gigs = GigPage::find(1);
        $category = CategoryPage::find(1);

        return view("pages.account.pageEdit",
        [
            "account"=>$account,
            // "countries" => $this->countries,
            // 'languages' => $this->languages,
            // 'currencies' => $this->currencies,
            // 'siteSetting' => $siteSetting,
            // 'mailSetting' => $mailSetting,
            // 'paymentSetting' => $paymentSetting,
            // 'categories' => $categories,
            // 'subCategories' => $subCategories,
            // 'skills' => $skills,
            'home' => $home,
            'login' => $login,
            'register' => $register,
            'gigs' => $gigs,
            'category' => $category
        ]);
    }

    public function getMessages(){
        $me = Message::select('reciever_id')->addSelect('created_at')->where('sender_id',auth()->id())->orderBy('created_at','desc')->distinct()->addSelect('message')->get();
        $them = Message::select('sender_id')->addSelect('created_at')->where('reciever_id',auth()->id())->orderBy('created_at','desc')->distinct()->addSelect('message')->get();
         $theArr = [];
         foreach($me as $m){
             $theArr[] = $m['reciever_id'];
         }
         foreach($them as $em){
            $theArr[] = $em['sender_id'];
        }
        $users = User::whereIn('id',$theArr)->get();
        foreach($users as $key => $user){
            $users[$key]->message = Message::where("sender_id",$user->id)->orWhere("reciever_id",$user->id)->orderBy("created_at","desc")->first()->message;
            $users[$key]->at = Message::where("sender_id",$user->id)->orWhere("reciever_id",$user->id)->orderBy("created_at","desc")->first()->created_at;
        }
        return $users;
    }
}
