<?php

                                            

class RaveController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // parent::__construct();

        /** setup PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
    public function form(){
        return view("pages.payment.testRave");
    }
    public function postPaymentWithpaypal(Request $request)
    {
        $setting = SiteSetting::find(1);
        $paymentSetting = PaymentSetting::find(1);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

    	$item_1 = new Item();

        $item_1->setName($request->input("gig_name")) /** item name **/
            ->setCurrency($setting->currency)
            ->setQuantity(1)
            ->setPrice($request->get('amount')); /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($setting->currency)
            ->setTotal($request->get('amount'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Purchase of "'.$request->input("gig_name").'"');

        Session::put('gig_id',$request->input("gig_id"));
        Session::put('plan_type',$request->input("plan_type"));
        Session::put('plan_id',$request->input("plan_id"));
        Session::put('amount',$request->input("amount"));

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('status')) /** Specify return URL **/
            ->setCancelUrl(URL::route('status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
            /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error','Connection timeout');
                return Redirect::route('paywithpaypal');
                /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                /** $err_data = json_decode($ex->getData(), true); **/
                /** exit; **/
            } else {
                \Session::put('error','Some error occur, sorry for inconvenient');
                return Redirect::route('paywithpaypal');
                /** die('Some error occur, sorry for inconvenient'); **/
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }

        \Session::put('error','Unknown error occurred');
    	return Redirect::route('paywithpaypal');
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error','Payment failed');
            return Redirect::route('paywithpaypal');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/
        if ($result->getState() == 'approved') {

            /** it's all right **/
            /** Here Write your database logic like that insert record or value in database if you want **/

            \Session::put('success','Payment success');
            // return Session::get('gig_id');

            $payment = new UserPayment();
            $payment->user_id = auth()->id();
            $payment->gig_id = Session::get('gig_id');
            $payment->plan_type = Session::get('plan_type');
            $payment->plan_id = Session::get('plan_id');
            $payment->amount = Session::get('amount');
            $payment->payment_status = "Success";
            $payment->save();

            $lastPayment = UserPayment::orderBy('created_at','desc')->first();

            $order = new Order();
            $order->payment_id = $lastPayment->id;
            $order->gig_id = Session::get('gig_id');
            $order->user_id = auth()->id();
            $order->save();

            $lastOrder = Order::orderBy('created_at','desc')->first();
            // return $lastPayment;

            $notify = new Notification;
            $notify->user_id = Gig::find(Session::get('gig_id'))->user->id;
            $notify->notification = auth()->user()->name." Purchased your Gig";
            $notify->link = "/orders/$lastOrder->id";
            $notify->save();

            $notify = new Notification;
            $notify->user_id = auth()->id();
            $notify->notification = "Your purchase of '".Gig::find(Session::get('gig_id'))->title."'"." was successful";
            $notify->link = "/orders/$lastOrder->id";
            $notify->save();

            Mail::raw(auth()->user()->name.' Purchased your Gig', function ($message) {
                $message->to(Gig::find(Session::get('gig_id'))->user->email);
            });

            return redirect()->route("payment.success",$lastOrder->id);

            // return Redirect::route('paywithpaypal');
        }
        \Session::put('error','Payment failed');

		return Redirect::route('paywithpaypal');
    }

}
