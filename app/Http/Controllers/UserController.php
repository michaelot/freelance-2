<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\SiteSetting;

class UserController extends Controller
{
    public function index(){
        $users = User::all();
        $setting = SiteSetting::find(1);
        return view("pages.user.index",[
            "users"=>$users,
            "setting"=>$setting,
        ]);
    }
}
