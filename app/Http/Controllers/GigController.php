<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Gig;
use App\GigSkill;
use App\BasicPlan;
use App\StandardPlan;
use App\PremiumPlan;
use App\BasicExtra;
use App\StandardExtra;
use App\PremiumExtra;
use App\GigFaq;
use App\GigImage;
use App\GigRequirement;
use App\SiteSetting;
use App\Category;
use App\SubCategory;
use App\Skill;
use App\Order;


class GigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "shift";
    }
    public function manage(){
        $gigs = Gig::where('user_id',auth()->id())->orderBy("created_at",'desc')->get();
        return view("pages.gig.manage",[
            "gigs" => $gigs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $subCategories = SubCategory::all();
        $skills = Skill::all();
        return view("pages.gig.create",[
            "categories" => $categories,
            "subCategories" => $subCategories,
            "skills" => $skills
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "image" => "required|mimes:jpg,jpeg,png,bitmap",
        ]);
        $gig_image = $request->file('image');
        $gig_image_file_name = $gig_image->getClientOriginalName();
        $gig_image_store_name = time().$gig_image_file_name;
        $path = $gig_image->storeAs('public/gig image',$gig_image_store_name);



        $gig = new Gig();
        $gig->user_id = auth()->id();
        $gig->title = $request->input("title");
        $gig->image = $gig_image_store_name;
        if($request->exists("video")){
            $this->validate($request,[
                "video" => "mimes:mp4",
            ]);
            $gig_video = $request->file('video');
            $gig_video_file_name = $gig_video->getClientOriginalName();
            $gig_video_store_name = time().$gig_video_file_name;
            $path = $gig_video->storeAs('public/gig video',$gig_video_store_name);
            $gig->video = $gig_video_store_name;
        }
        $gig->gig_category_id = $request->input("gig_category_id");
        $gig->gig_sub_category_id = $request->input("gig_sub_category_id");
        $gig->description = $request->input("description");
        if($request->exists("search_tags")){
            foreach($request->input("search_tags") as $tag){
                $gig->search_tags .= $tag.",";
            }
        }
        $gig->save();

        $currentGig = Gig::orderBy("created_at","desc")->first();

        // Save Skill
        foreach($request->input("skills") as $skill){
            $gig_skill = new GigSkill();
            $gig_skill->gig_id = $currentGig->id;
            $gig_skill->skill_id = $skill;
            $gig_skill->save();
        }


        //Save Basic Plan
        $basic_plan = new BasicPlan();
        $basic_plan->gig_id = $currentGig->id;
        $basic_plan->plan_title = $request->input("basic_plan_title");
        $basic_plan->description =  $request->input("basic_plan_description");
        $basic_plan->duration = $request->input("basic_plan_duration");
        $basic_plan->duration_type = $request->input("basic_plan_duration_type");
        $basic_plan->revisions = $request->input("basic_plan_revisions");
        $basic_plan->price = $request->input("basic_plan_price");
        $basic_plan->save();

        // Basic Plan Extras
        if($request->exists('basic_plan_extra')){
            if(sizeof($request->input("basic_plan_extra")) > 0){
                foreach($request->input("basic_plan_extra") as $extra){
                    if(!empty($extra)){
                        $basic_extra = new BasicExtra();
                        $basic_extra->basic_plan_id = BasicPlan::orderBy("created_at","desc")->first()->id;
                        $basic_extra->name = $extra;
                        $basic_extra->save();
                    }
                }
            }
        }


        //Standard Basic Plan
        $standard_plan = new StandardPlan();
        $standard_plan->gig_id = $currentGig->id;
        $standard_plan->plan_title = $request->input("standard_plan_title");
        $standard_plan->description =  $request->input("standard_plan_description");
        $standard_plan->duration = $request->input("standard_plan_duration");
        $standard_plan->duration_type = $request->input("standard_plan_duration_type");
        $standard_plan->revisions = $request->input("standard_plan_revisions");
        $standard_plan->price = $request->input("standard_plan_price");
        $standard_plan->save();

        //Standard Plan Extra
        // return $request->exists('standard_plan_extra');
        if($request->exists('standard_plan_extra')){
            if(sizeof($request->input("standard_plan_extra")) > 0){
                foreach($request->input("standard_plan_extra") as $extra){
                    if(!empty($extra)){
                        $standard_extra = new StandardExtra();
                        $standard_extra->standard_plan_id = StandardPlan::orderBy("created_at","desc")->first()->id;
                        $standard_extra->name = $extra;
                        $standard_extra->save();
                    }

                }
            }
        }

        //Premium Basic Plan
        $premium_plan = new PremiumPlan();
        $premium_plan->gig_id = $currentGig->id;
        $premium_plan->plan_title = $request->input("premium_plan_title");
        $premium_plan->description =  $request->input("premium_plan_description");
        $premium_plan->duration = $request->input("premium_plan_duration");
        $premium_plan->duration_type = $request->input("premium_plan_duration_type");
        $premium_plan->revisions = $request->input("premium_plan_revisions");
        $premium_plan->price = $request->input("premium_plan_price");
        $premium_plan->save();

        if($request->exists('premium_plan_extra')){
            if(sizeof($request->input("premium_plan_extra")) > 0){
                foreach($request->input("premium_plan_extra") as $extra){
                    if(!empty($extra)){
                        $premium_extra = new PremiumExtra();
                        $premium_extra->premium_plan_id = PremiumPlan::orderBy("created_at","desc")->first()->id;
                        $premium_extra->name = $extra;
                        $premium_extra->save();
                    }
                }
            }
        }
        if($request->exists('faq_title')){
            if(sizeof($request->input("faq_title")) > 0){
                for ($i=0; $i< count($request->input("faq_title")); $i++) {
                    $faq = new GigFaq();
                    $faq->gig_id = $currentGig->id;
                    $faq->title = $request->input("faq_title")[$i];
                    $faq->answer = $request->input("faq_answer")[$i];
                    $faq->save();
                }
            }
        }

        if($request->exists("requirement")){
            for($i=0; $i< count($request->input("requirement")); $i++){
                if(!empty($request->input('requirement')[$i])){
                    $requirement = new GigRequirement();
                    $requirement->gig_id = $currentGig->id;
                    $requirement->title = $request->input('requirement')[$i];
                    $requirement->answer_type = $request->input('answer_type')[$i];
                    $requirement->compulsory = $request->input('compulsory')[$i];
                    $requirement->save();
                }

            }
        }

        if($request->exists("gig_images")){
            $this->validate($request,[
                "gig_images.*" => "mimes:jpg,jpeg,png,bitmap",
            ]);
            foreach($request->file("gig_images") as $image){
                // $gig_image = $request->file('image');
                $image_file_name = $image->getClientOriginalName();
                $image_store_name = time().$image_file_name;
                $path = $image->storeAs('public/gig image',$image_store_name);
                $gigImage = new GigImage();
                $gigImage->gig_id = $currentGig->id;
                $gigImage->image = $image_store_name;
                $gigImage->save();
            }
        }

        return redirect()->route("gig.manage") ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $setting = SiteSetting::find(1);
        $gig = Gig::findorfail($id);
        if($gig->user_id != auth()->id()){
            $gig->visits = $gig->visits+1;
            $gig->save();
        }
        $gigs = Gig::inRandomOrder()->take(4)->get();
        foreach($gigs as $gig){
            $gig->impressions = $gig->impressions+1;
            $gig->save();
        }

        $orders = Order::where('gig_id',$id)->get();
        return view("pages.gig.show",[
            'gig' => $gig,
            "setting"=>$setting,
            'gigs' =>$gigs,
            'orders' =>$orders,
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gig = Gig::findorfail($id);
        $categories = Category::all();
        $subCategories = SubCategory::all();
        $skills = Skill::all();
        return view("pages.gig.edit",[
            "gig" => $gig,
            "categories" => $categories,
            "subCategories" => $subCategories,
            "skills" => $skills
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $gig = Gig::findorfail($id);
        $gig->user_id = auth()->id();
        $gig->title = $request->input("title");
        if($request->exists("image")){
            $this->validate($request,[
                "image" => "required|mimes:jpg,jpeg,png,bitmap",
            ]);
            $gig_image = $request->file('image');
            $gig_image_file_name = $gig_image->getClientOriginalName();
            $gig_image_store_name = time().$gig_image_file_name;
            $path = $gig_image->storeAs('public/gig image',$gig_image_store_name);

            $gig->image = $gig_image_store_name;
        }
        if($request->exists("video")){
            $this->validate($request,[
                "video" => "mimes:mp4",
            ]);
            $gig_video = $request->file('video');
            $gig_video_file_name = $gig_video->getClientOriginalName();
            $gig_video_store_name = time();
            $path = $gig_video->storeAs('public/gig video',$gig_video_store_name);
            Storage::delete('public/gig video/'+$gig->video);
            $gig->video = $gig_video_store_name;
        }
        $gig->gig_category_id = $request->input("gig_category_id");
        $gig->gig_sub_category_id = $request->input("gig_sub_category_id");
        $gig->description = $request->input("description");
        if($request->exists("search_tags")){
            foreach($request->input("search_tags") as $tag){
                $gig->search_tags .= $tag.",";
            }
        }
        $gig->save();

        // delete Skill
        $oldSkills = GigSkill::where("gig_id",$id)->get();
        foreach($oldSkills as $skill){
            $skill->delete();
        }
        // Save Skill
        foreach($request->input("skills") as $skill){
            $gig_skill = new GigSkill();
            $gig_skill->gig_id = $id;
            $gig_skill->skill_id = $skill;
            $gig_skill->save();
        }

        //Save Basic Plan
        $basic_plan = BasicPlan::where("gig_id",$id)->first();
        // $basic_plan->gig_id = $currentGig->id;
        $basic_plan->plan_title = $request->input("basic_plan_title");
        $basic_plan->description =  $request->input("basic_plan_description");
        $basic_plan->duration = $request->input("basic_plan_duration");
        $basic_plan->duration_type = $request->input("basic_plan_duration_type");
        $basic_plan->revisions = $request->input("basic_plan_revisions");
        $basic_plan->price = $request->input("basic_plan_price");
        $basic_plan->save();

        // Basic Plan Extras
        if($request->exists('basic_plan_extra')){
            if(sizeof($request->input("basic_plan_extra")) > 0){
                // delete Skill
                $oldExtras = BasicExtra::where("basic_plan_id",$basic_plan->id)->get();
                foreach($oldExtras as $extra){
                    $extra->delete();
                }

                foreach($request->input("basic_plan_extra") as $extra){
                    if(!empty($extra)){
                        $basic_extra = new BasicExtra();
                        $basic_extra->basic_plan_id = BasicPlan::orderBy("created_at","desc")->first()->id;
                        $basic_extra->name = $extra;
                        $basic_extra->save();
                    }
                }
            }
        }


        //Standard Basic Plan
        $standard_plan = StandardPlan::where("gig_id",$id)->first();
        // $standard_plan->gig_id = $currentGig->id;
        $standard_plan->plan_title = $request->input("standard_plan_title");
        $standard_plan->description =  $request->input("standard_plan_description");
        $standard_plan->duration = $request->input("standard_plan_duration");
        $standard_plan->duration_type = $request->input("standard_plan_duration_type");
        $standard_plan->revisions = $request->input("standard_plan_revisions");
        $standard_plan->price = $request->input("standard_plan_price");
        $standard_plan->save();

        //Standard Plan Extra
        if($request->exists('standard_plan_extra')){
            if(sizeof($request->input("standard_plan_extra")) > 0){

                $oldExtras = StandardExtra::where("standard_plan_id",$standard_plan->id)->get();
                foreach($oldExtras as $extra){
                    $extra->delete();
                }

                foreach($request->input("standard_plan_extra") as $extra){
                    if(!empty($extra)){
                        $standard_extra = new StandardExtra();
                        $standard_extra->standard_plan_id = StandardPlan::orderBy("created_at","desc")->first()->id;
                        $standard_extra->name = $extra;
                        $standard_extra->save();
                    }
                }
            }
        }

        //Premium Basic Plan
        $premium_plan = PremiumPlan::where("gig_id",$id)->first();
        // $premium_plan->gig_id = $currentGig->id;
        $premium_plan->plan_title = $request->input("premium_plan_title");
        $premium_plan->description =  $request->input("premium_plan_description");
        $premium_plan->duration = $request->input("premium_plan_duration");
        $premium_plan->duration_type = $request->input("premium_plan_duration_type");
        $premium_plan->revisions = $request->input("premium_plan_revisions");
        $premium_plan->price = $request->input("premium_plan_price");
        $premium_plan->save();

        if($request->exists('premium_plan_extra')){
            if(sizeof($request->input("premium_plan_extra")) > 0){

                $oldExtras = PremiumExtra::where("premium_plan_id",$premium_plan->id)->get();
                foreach($oldExtras as $extra){
                    $extra->delete();
                }

                foreach($request->input("premium_plan_extra") as $extra){
                    if(!empty($extra)){
                        $premium_extra = new PremiumExtra();
                        $premium_extra->premium_plan_id = PremiumPlan::orderBy("created_at","desc")->first()->id;
                        $premium_extra->name = $extra;
                        $premium_extra->save();
                    }
                }
            }
        }

        if($request->exists('faq_title')){
            foreach($gig->faqs as $faq){
                $faq->delete();
            }
            for ($i=0; $i< count($request->input("faq_title")); $i++) {
                if( $request->input("faq_title")[$i] =="" || $request->input("faq_answer")[$i] == ""){
                    continue;
                }else{
                    $faq = new GigFaq();
                    $faq->gig_id = $id;
                    $faq->title = $request->input("faq_title")[$i];
                    $faq->answer = $request->input("faq_answer")[$i];
                    $faq->save();
                }

            }
        }

        if($request->exists("requirement")){
            // foreach($gig->requirements as $requirement){
            //     $requirement->delete();
            // }
            for($i=0; $i< count($request->input("requirement")); $i++){
                if(!empty($request->input('requirement')[$i])){
                    if($request->input('new')[$i] == "Yes"){
                        $requirement = new GigRequirement();
                        $requirement->gig_id = $id;
                        $requirement->title = $request->input('requirement')[$i];
                        $requirement->answer_type = $request->input('answer_type')[$i];
                        $requirement->compulsory = $request->input('compulsory')[$i];
                        $requirement->save();
                    }elseif($request->input('new')[$i] == "No"){
                        $requirement = GigRequirement::findorfail($request->input('id')[$i]);
                        $requirement->gig_id = $id;
                        $requirement->title = $request->input('requirement')[$i];
                        $requirement->answer_type = $request->input('answer_type')[$i];
                        $requirement->compulsory = $request->input('compulsory')[$i];
                        $requirement->save();
                    }

                }
            }
        }


        if($request->exists("gig_images")){
            $this->validate($request,[
                "gig_images.*" => "mimes:jpg,jpeg,png,bitmap",
            ]);
            foreach($request->file("gig_images") as $image){
                // $gig_image = $request->file('image');
                $image_file_name = $image->getClientOriginalName();
                $image_store_name = time().$image_file_name;
                $path = $image->storeAs('public/gig image',$image_store_name);
                $gigImage = new GigImage();
                $gigImage->gig_id = $id;
                $gigImage->image = $image_store_name;
                $gigImage->save();
            }
        }

        if($request->exists("delete_images")){
            foreach($request->input("delete_images") as $image){
                // $gig_image = $request->file('image');
                $image = GigImage::findorfail($image);
                $image->delete();
            }
        }

        return redirect()->back()->withSuccess(" Updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gig = Gig::find($id);
        $gig->delete();
        return redirect()->back();
    }
}
