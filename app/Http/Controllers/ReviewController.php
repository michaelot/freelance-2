<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\Income;
use App\Order;
use App\Notification;


class ReviewController extends Controller
{
    public function store(Request $request,$id,$order_id,$amount){
        $review = new Review();
        $review->user_id = auth()->id();
        $review->seller_id = $id;
        $review->order_id = $order_id;
        $review->description = $request->input("description");
        $review->service = $request->input("service");
        $review->communication = $request->input("communication");
        $review->save();

        $review->order()->update([
            "status" => "Completed"
        ]);
        $order = Order::findorfail($order_id);
        $income = new Income();
        $income->user_id = $id;
        $income->order_id = $order_id;
        $income->amount = $amount;
        $income->save();

        $notify = new Notification;
        $notify->user_id = $review->order->gig->user->id;
        $notify->notification = auth()->user()->name." has Accept your delivery";
        $notify->link = "/orders/".$review->order->id;
        $notify->save();

        return redirect()->back();

    }

    public function update(Request $request,$id){
        $review = Review::find($id);
        $review->description = $request->input("description");
        $review->service = $request->input("service");
        $review->communication = $request->input("communication");
        $review->save();

        $review->order()->update([
            "status" => "Completed"
        ]);

        return redirect()->back();
    }
}
