<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\User;
use App\Events\MessageSent;
use App\Events\UpdateChatList;

class MessageController extends Controller
{
    public function index(){
        $users = $this->listing();
        //  return $users;
        return view("pages.message.index",[
            "users" => $users,

        ]);
    }
    public function listing(){
        $me = Message::select('reciever_id')->addSelect('created_at')->where('sender_id',auth()->id())->orderBy('created_at','desc')->distinct()->addSelect('message')->get();
        $them = Message::select('sender_id')->addSelect('created_at')->where('reciever_id',auth()->id())->orderBy('created_at','desc')->distinct()->addSelect('message')->get();
         $theArr = [];
         foreach($me as $m){
             $theArr[] = $m['reciever_id'];
         }
         foreach($them as $em){
            $theArr[] = $em['sender_id'];
        }
        $users = User::whereIn('id',$theArr)->get();
        foreach($users as $key => $user){
            $users[$key]->message = Message::where("sender_id",$user->id)->orWhere("reciever_id",$user->id)->orderBy("created_at","desc")->first()->message;
            $users[$key]->at = Message::where("sender_id",$user->id)->orWhere("reciever_id",$user->id)->orderBy("created_at","desc")->first()->created_at;
        }
        return $users;
    }

    public function receiverListing($id){
        $me = Message::select('reciever_id')->addSelect('created_at')->where('sender_id',$id)->orderBy('created_at','desc')->distinct()->addSelect('message')->get();
        $them = Message::select('sender_id')->addSelect('created_at')->where('reciever_id',$id)->orderBy('created_at','desc')->distinct()->addSelect('message')->get();
         $theArr = [];
         foreach($me as $m){
             $theArr[] = $m['reciever_id'];
         }
         foreach($them as $em){
            $theArr[] = $em['sender_id'];
        }
        $users = User::whereIn('id',$theArr)->get();
        $list = [];

        foreach($users as $key => $user){
            $users[$key]->message = Message::where("sender_id",$user->id)->orWhere("reciever_id",$user->id)->orderBy("created_at","desc")->first()->message;
            $users[$key]->at = Message::where("sender_id",$user->id)->orWhere("reciever_id",$user->id)->orderBy("created_at","desc")->first()->created_at;
            $list[] = $user;
        }
        return $list;
    }
    public function show($id){
        $user = User::find($id);
        $messages = Message::where(function ($query) use ($id){
            $query->where('sender_id',auth()->id())->where('reciever_id',$id);
        })->orWhere(function ($query) use ($id){
            $query->where('reciever_id',auth()->id())->where('sender_id',$id)->with("sender");
        })->with("sender")->get();
        // return $messages;
        return view("pages.message.show",[
            "messages" => $messages,
            "user" => $user
        ]);
    }
    public function messages($id){
        $messages = Message::where(function ($query) use ($id){
            $query->where('sender_id',auth()->id())->where('reciever_id',$id);
        })->orWhere(function ($query) use ($id){
            $query->where('reciever_id',auth()->id())->where('sender_id',$id)->with("sender");
        })->with("sender")->get();

        return $messages;
    }
    public function store(Request $request,$id){
        $sender = auth()->user();
        $message  = new Message();
        $message->sender_id = auth()->id();
        $message->reciever_id = $id;
        $message->message = $request->input("message");
        $message->save();

        broadcast(new MessageSent($sender, $message,$id))->toOthers();
        $list = $this->receiverListing($id);
        broadcast(new UpdateChatList($sender,$list,$id))->toOthers();

        return "Success";
    }

}
