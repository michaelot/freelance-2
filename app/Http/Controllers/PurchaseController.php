<?php

namespace App\Http\Controllers;

use App\Order;


class PurchaseController extends Controller
{
    public function index(){
        $orders = Order::where("user_id",auth()->id())->orderBy("created_at","desc")->get();
        return view("pages.purchase.index",[
            "orders" => $orders
        ]);
    }
}
