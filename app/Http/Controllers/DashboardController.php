<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gig;
use App\Order;
use App\Income;
use App\Payment;
use App\SiteSetting;
use App\PaymentSetting;
use App\Withdrawal;
use App\Review;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\DB;



class DashboardController extends Controller
{
    public function index(){
        $currency = SiteSetting::find(1)->currency;
        if(auth()->user()->type == "admin"){
            $totalGigs = Gig::count();
            $getGigs = Gig::orderBy("created_at","desc")->take(10)->get();
        }else{
            $totalGigs = Gig::where("user_id",auth()->id())->count();
            $getGigs = auth()->user()->gigs;
        }

        $gig_id = [];
        foreach($getGigs as $gig){
            $gig_id[] = $gig->id;
        }

        if(auth()->user()->type == "admin"){
            $totalOrders = Order::count();
        }else{
            $totalOrders = Order::whereIn("gig_id",$gig_id)->count();
        }
        $users = [];
        $totalpaymentsThisMonth = 0;
        $totalpayments=0;
        $paidWithdrawalsThisMonth=0;
        $paidWithdrawals=0;
        if(auth()->user()->type == "admin"){
            $users = User::all();
            $totalpayments = Payment::sum("amount");
            $totalpaymentsThisMonth = Payment::whereMonth("created_at",Carbon::now()->month)->sum("amount");

            $paidWithdrawals = Withdrawal::where("status","Paid")->sum("amount");
            $paidWithdrawalsThisMonth =Withdrawal::whereMonth("created_at",Carbon::now()->month)->where("status","Paid")->sum("amount");
        }
        $totalOrdersMonth = Order::whereMonth("created_at",Carbon::now()->month)->whereIn("gig_id",$gig_id)->count();
        $totalOrdersAmountMonth = Order::whereMonth("created_at",Carbon::now()->month)->whereIn("gig_id",$gig_id)->get();
        $paymentsThisMonth = 0;
        $viewsThisMonth= 0;
        foreach ($totalOrdersAmountMonth as $value) {
             $paymentsThisMonth += $value->payment->amount;
             $viewsThisMonth += $value->gig->visits;
        }
        // return $viewsThisMonth;
        $amountAvailable = Income::where("user_id",auth()->id())->sum("amount") - Withdrawal::where("user_id",auth()->id())->where("status","Paid")->sum("amount");
        $amountWithdrawed = Withdrawal::where("user_id",auth()->id())->where("status","Paid")->sum("amount");
        if(auth()->user()->type == "admin"){
            $completeOrders = Order::where("status","Completed")->count();
            $deliveredOrders = Order::where("status","Delivered")->count();
            $InProgressOrders = Order::where("status","In Progress")->count();
            $recentSales =  Order::orderBy("created_at","desc")->take(10)->get();
            $pendingOrders =  Order::where("status","In Progress")->take(10)->get();

        }else{
            $completeOrders = Order::whereIn("gig_id",$gig_id)->where("status","Completed")->count();
            $deliveredOrders = Order::whereIn("gig_id",$gig_id)->where("status","Delivered")->count();
            $InProgressOrders = Order::whereIn("gig_id",$gig_id)->where("status","In Progress")->count();
            $recentSales =  Order::whereIn("gig_id",$gig_id)->take(10)->get();
            $pendingOrders =  Order::whereIn("gig_id",$gig_id)->where("status","In Progress")->take(10)->get();
        }
        // $recentSales =  Order::whereIn("gig_id",$gig_id)->take(10)->get();
        $reviews = Review::where("seller_id",auth()->id())->get();
        $visits=[];
        $impressions = [];
        $sales = [];
        $names = [];
        foreach(auth()->user()->gigs as $gig){
            $visits[] = $gig->visits;
            $impressions[] = $gig->impressions;
            $sales[] = count($gig->orders);
            $names[] = $gig->title;
        }
        // return [$visits,$impressions,$sales];\
        // $theOrders = Order::select(DB::raw('count(id) as data'), DB::raw("DATE_FORMAT(created_at,'%m-%Y') new_date"), DB::raw('YEAR(created_At) year, MONTH(created_At) monnth') )->groupBy('year','month')->get();
        return view("pages.dashboard.index",[
            "totaGigs" => $totalGigs,
            "totalOrders" => $totalOrders,
            "users" => $users,
            "amountAvailable" => $amountAvailable,
            "amountWithdrawed" => $amountWithdrawed,
            "gigs" => $getGigs,
            "recentSales" => $recentSales,
            "currency" => $currency,
            "pendingOrders" => $pendingOrders,
            "completeOrders" => $completeOrders,
            "deliveredOrders" => $deliveredOrders,
            "InProgressOrders" => $InProgressOrders,
            "reviews" => $reviews,
            "visits" => $visits,
            "impressions" => $impressions,
            "sales" => $sales,
            "names" => $names,
            "totalOrdersMonth" => $totalOrdersMonth,
            "paymentsThisMonth" => $paymentsThisMonth,
            "viewsThisMonth" => $viewsThisMonth,
            "totalpayments" => $totalpayments,
            "totalpaymentsThisMonth" => $totalpaymentsThisMonth,
            "paidWithdrawals" => $paidWithdrawals,
            "paidWithdrawalsThisMonth" => $paidWithdrawalsThisMonth,
        ]);


    }
    public function columnChart(){
        $gigs = auth()->user->gigs;
        return  $gigs;
    }
}
