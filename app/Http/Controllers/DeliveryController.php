<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Delivery;
use App\Notification;

class DeliveryController extends Controller
{
    public function store(Request $request, $id){
        $this->validate($request,[
            'preview' =>"required|mimes:jpg,jpeg,png,gif,mp4"
        ]);

        $file = $request->file("preview");
        $file_name = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        $store_name = "preview-image-".time().".".$ext;
        $path = $file->storeAs('public/delivery preview',$store_name);

        $delivery = new Delivery();
        $delivery->order_id = $id;
        $delivery->description = $request->input("description");
        $delivery->preview = $store_name;
        $delivery->type = $ext;
        $delivery->preview_path = $store_name;
        $delivery->save();

        if($request->hasFile("files")){
            foreach($request->file("files") as $file){
                $file_name = $file->getClientOriginalName();
                $ext = $file->getClientOriginalExtension();
                $store_name =time()."-".$file_name;
                // return $store_name;
                $path = $file->storeAs('public/delivery files',$store_name);
                $delivery->files()->create([
                    "delivery_id" => $delivery->id,
                    "name" => $file_name,
                    "path" => $store_name
                ]);
            }
        }
        $delivery->order()->update([
            "status" => "Delivered"
        ]);
        $notify = new Notification;
        $notify->user_id = $delivery->order->user->id;
        $notify->notification = auth()->user()->name." has Delivered your order";
        $notify->link = "/orders/".$delivery->order->id;
        $notify->save();

        return redirect()->back();
    }
    public function update(Request $request, $id){
        $delivery = Delivery::find($id);
        if($request->has("description")){
            $delivery->description = $request->input("description");
        }
        if($request->hasFile("preview")){
            // return "L";
            $file = $request->file("preview");
            $file_name = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $store_name = "preview-image-".time().".".$ext;
            $path = $file->storeAs('public/delivery preview',$store_name);

            $delivery->preview = $store_name;
            $delivery->type = $ext;
            $delivery->preview_path = $store_name;
        }

        $delivery->save();

        if($request->hasFile("files")){
            foreach($request->file("files") as $file){
                $file_name = $file->getClientOriginalName();
                $ext = $file->getClientOriginalExtension();
                $store_name =time()."-".$file_name;
                // return $store_name;
                $path = $file->storeAs('public/delivery files',$store_name);
                $delivery->files()->create([
                    "delivery_id" => $delivery->id,
                    "name" => $file_name,
                    "path" => $store_name
                ]);
            }
        }
        $delivery->order()->update([
            "status" => "Delivered"
        ]);

        $notify = new Notification;
        $notify->user_id = $delivery->order->user->id;
        $notify->notification = auth()->user()->name." has Updated Delivered your order";
        $notify->link = "/orders/".$delivery->order->id;
        $notify->save();

        return redirect()->back();
    }
}
