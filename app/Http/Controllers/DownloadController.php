<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function download($at,$path,$name){
        return response()->download(storage_path('app/public/'.$at.'/'. $path), $name);
    }
}
