<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

// use PayPal\ Api\BillingInfo;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function skills(){
        return $this->hasMany(SellerSkill::class);
    }
    public function seller(){
        return $this->hasOne(Seller::class);
    }
    public function gigs(){
        return $this->hasMany(Gig::class);
    }
    public function payments(){
        return $this->hasMany(Payment::class);
    }
    public function orders(){
        return $this->hasMany(Order::class);
    }
    public function paymentInfo(){
        return $this->hasOne(BillingInfo::class);
    }
    public function reviews(){
       return $this->hasMany(Review::class,"seller_id");
    }
    public function notifications(){
        return $this->hasMany(Notification::class);
    }
}

