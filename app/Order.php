<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ["status"];
    public function payment(){
        return $this->belongsTo(Payment::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function gig(){
        return $this->belongsTo(Gig::class);
    }
    public function requirements(){
        return $this->hasMany(OrderRequirement::class);
    }
    public function delivery(){
        return $this->hasOne(Delivery::class);
    }
    public function review(){
        return $this->hasOne(Review::class);
    }
}
