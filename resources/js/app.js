/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// import Countdown from 'vuejs-countdown'
import moment from 'moment'

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('send-comment-component', require('./components/SendOrderComment.vue').default);
Vue.component('compose-message-component', require('./components/ComposeMessageComment.vue').default);
Vue.component('send-message-component', require('./components/SendMessageComponent.vue').default);
Vue.component('list-comment-component', require('./components/OrderComment.vue').default);
Vue.component('messages-component', require('./components/MessageComponent.vue').default);
Vue.component('chat-list', require('./components/MessageListComponent.vue').default);
Vue.component('gig-list', require('./components/GigListComponent.vue').default);
Vue.component('gig-page', require('./components/GigPageComponent.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data() {
        return {
            page: '',
            comments : [],
            orderId: '',
            receiverId:"",
            messages:[],
            listing:[],
            gigSearch:" ",
            gigs:[]
        }
    },
    created() {
        this.page = $("meta[name='page']").attr("content");
        this.orderId = $("#orderId").val();
        this.receiverId = $("#receiverId").val();


        if(this.page == "messageIndex"){
            this.fetchList();

            Echo.private("list."+$("#userId").val())
            .listen("UpdateChatList",(e) =>{
                console.log(e);
                this.listing = (e.users);
            })
        }else if(this.page == "messages"){
            this.fetchMessages(this.receiverId);

            Echo.private("message."+$("#userId").val())
            .listen("MessageSent",(e) =>{
                this.messages.push({
                    message : e.message.message,
                    sender : e.sender,
                    created_at: e.message.created_at
                })
            })
        }else if(this.page == "order"){
            this.fetchComments(this.orderId);

            Echo.private("comment."+this.orderId)
            .listen("CommentSent",(e) =>{
                this.comments.push({
                    comment : e.comment.comment,
                    user :e.user
                })
            })
        }

    },
    methods: {
        fetchComments(id){
            axios.get('/order/'+id+'/comments').then(response => {
                this.comments = response.data
            })
        },
        fetchMessages(id){
            axios.get('/message/'+id+'/listing').then(response => {
                this.messages = response.data
            })
        },
        fetchList(){
            axios.get('/message/listing').then(response => {
                this.listing = response.data
            })
        },
        addComment(message){
            this.comments.push(message)
            axios.post('/order/'+message.order+'/comments',message).then(response => {
            })
        },
        addMessage(message){
            console.log(message)
            this.messages.push(message)
            this.listing.push({
                id : message.id,
                name: message.name,
                message : message.message
            })
            axios.post('/message/'+message.receiver.id,message).then(response => {
            })
        },
        
    },
});
Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('l');
    }
})
//Form Page
