@extends('layouts.custom')
@section('content')
    <div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="text-white mb-3 text-left">Dashboard<br></h2>
                    <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Dashboard</span>&nbsp;&nbsp;</p>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.subNav',['page'=>"dashboard"])
    <section id="dashboard-main-section" class="section-padding bg-color">
        <div class="container">
            <div class="row mb-5">
                <div class="col col-lg-3 col-md-12 mb-3 col-sm-6">
                    <div class="shadow pt-4 pb-4 rounded text-center bg-brown shadow-sm">
                        <p class="text-white mb-3 text-20">Total Gigs</p>
                        <h1 class="text-white mb-2">{{ $totaGigs}}</h1>
                    </div>
                </div>
                <div class="col col-lg-3 col-md-12 mb-3 col-sm-6">
                    <div class="shadow pt-4 pb-4 rounded text-center bg-purple shadow-sm">
                        <p class="text-white mb-3 text-20">Orders</p>
                        <h1 class="text-white mb-2">{{ $totalOrders}}</h1>
                    </div>
                </div>
                @seller
                    <div class="col col-lg-3 col-md-12 mb-3 col-sm-6">
                        <div class="shadow pt-4 pb-4 rounded text-center bg-brown shadow-sm">
                            <p class="text-white mb-3 text-20">Amount Available</p>
                            <h1 class="text-white mb-2">{{ $amountAvailable}}</h1>
                        </div>
                    </div>
                    <div class="col col-lg-3 col-md-12 mb-3 col-sm-6">
                        <div class="shadow pt-4 pb-4 rounded text-center bg-purple shadow-sm">
                            <p class="text-white mb-3 text-20">Amount Withdrawed</p>
                            <h1 class="text-white mb-2">{{ $amountWithdrawed}}</h1>
                        </div>
                    </div>
                @endSeller
                @admin
                    <div class="col col-lg-3 col-md-12 mb-3 col-sm-6">
                        <div class="shadow pt-4 pb-4 rounded text-center shadow-sm bg-success">
                            <p class="text-white mb-3 text-20">Regular Users</p>
                            <h1 class="text-white mb-2">{{ $users->where("type","regular")->count()}}</h1>
                        </div>
                    </div>
                    <div class="col col-lg-3 col-12">
                        <div class="shadow pt-4 pb-4 rounded text-center shadow-sm main-bg-color">
                            <p class="text-white mb-3 text-20">Sellers</p>
                            <h1 class="text-white mb-2">{{ $users->where("type","seller")->count()}}</h1>
                        </div>
                    </div>
                @endAdmin
            </div>
            @admin
            <div class="row mb-5">
                <div class="col col-lg-3 col-md-12 mb-3 col-sm-6">
                    <div class="shadow pt-4 pb-4 rounded text-center bg-info shadow-sm">
                        <p class="text-white mb-3 text-20">Total Payments </p>
                        <h1 class="text-white mb-2">{{ $totalpayments}}</h1>
                    </div>
                </div>
                <div class="col col-lg-3 col-md-12 mb-3 col-sm-6">
                    <div class="shadow pt-4 pb-4 rounded text-center bg-info shadow-sm">
                        <p class="text-white mb-3 text-20">Payment This Month</p>
                        <h1 class="text-white mb-2">{{ $totalpaymentsThisMonth}}</h1>
                    </div>
                </div>
                <div class="col col-lg-3 col-md-12 mb-3 col-sm-6">
                    <div class="shadow pt-4 pb-4 rounded text-center shadow-sm bg-warning">
                        <p class="text-white mb-3 text-20">Total Paid Withdrawals</p>
                        <h1 class="text-white mb-2">{{ $paidWithdrawals }}</h1>
                    </div>
                </div>
                <div class="col col-lg-3 col-12">
                    <div class="shadow pt-4 pb-4 rounded text-center shadow-sm bg-warning">
                        <p class="text-white mb-3 text-20">Withdrawals This Month</p>
                        <h1 class="text-white mb-2">{{ $paidWithdrawalsThisMonth}}</h1>
                    </div>
                </div>
            </div>
            @endAdmin
            @seller
                <div class="row">
                    <div class="col bg-white rounded pt-5 pb-5 mb-5">
                        <div class="row">
                            <div class="col col-lg-10">
                                <h3 class="font-weight-normal ml-4 mb-4">Sales and Views Statistics</h3>
                            </div>
                            <div class="col col-lg-2">
                                <form>
                                    <div class="form-group"><select class="form-control main-input-form"><option value="12" selected="">Jan</option></select></div>
                                </form>
                            </div>
                        </div>
                        <div id="chart1"></div>
                        <div class="row mt-5">
                            <div class="col col-lg-4 text-center">
                                <h2 class="main-color">{{ $totalOrdersMonth }}</h2>
                                <p class="text-18 mt-1">Total orders For This Month&nbsp;</p>
                            </div>
                            <div class="col col-lg-4 text-center">
                                <h2 class="main-color">{{ $currency }} {{ $paymentsThisMonth }}</h2>
                                <p class="text-18 mt-1">Total Sales For This Month&nbsp;</p>
                            </div>
                            <div class="col col-lg-4 text-center">
                                <h2 class="main-color">{{ $viewsThisMonth }}</h2>
                                <p class="text-18 mt-1">Total View For This Month&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endSeller
            <div class="row">
                <div class="col col-lg-6">
                    <div class="bg-white pt-5 pb-5 pl-3 pr-3">
                        <div class="row">
                            <div class="col col-lg-12">
                                <h3 class="font-weight-normal mb-4">Orders</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div id="chart2"></div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col col-lg-6 text-center">
                                <h2 class="text-success">{{ $completeOrders }}</h2>
                                <p class="text-18 mt-1">Completed Orders&nbsp;</p>
                            </div>
                            <div class="col col-lg-6 text-center">
                                <h2 class="text-info">{{ $deliveredOrders }}</h2>
                                <p class="text-18 mt-1">Delivered Orders&nbsp;</p>
                            </div>
                            <div class="col col-lg-6 text-center">
                                <h2 class="text-warning">{{ $InProgressOrders }}</h2>
                                <p class="text-18 mt-1">Uncompleted Orders&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-lg-6">
                    <div class="bg-white pt-5 pb-5 pl-3 pr-3">
                        <div class="row">
                            <div class="col col-lg-12">
                                <h3 class="font-weight-normal mb-4">Gigs</h3>
                            </div>
                        </div>
                        @foreach ($gigs->take(6) as $gig)
                            <div class="row fr-cl-nds-container-row border-bottom-0 border-top">

                                <div class="col d-flex justify-content-center align-items-center col-md-3">
                                    <img class="img-fluid" src="{{ asset("storage/gig image/".$gig->image) }}">
                                </div>
                                <div class="col col-md-7 pl-0">
                                    <p>
                                        <span>{{ $gig->title }}</span>
                                        <br>
                                    </p>
                                    <span class="text-primary">{{ $gig->category->name }}</span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col col-lg-7">
                    <div class="bg-white pt-5 pb-5 pl-3 pr-3">
                        <div class="row">
                            <div class="col col-lg-12">
                                <h3 class="font-weight-normal mb-4">Recent Sales</h3>
                            </div>
                        </div>
                        @foreach ($recentSales as $order)
                            <div class="row fr-cl-nds-container-row border-top pt-3 pb-3 border-bottom-0">
                                <div class="col d-flex justify-content-center align-items-center col-md-2">
                                    <img class="img-fluid" src="{{ asset("storage/gig image/".$order->gig->image) }}" width="50">
                                </div>
                                <div class="col col-md-8 pl-0">
                                    <p class="text-20 font-weight-normal">{{ $order->user->name}}</p>
                                    <span class="text-purple text-14 d-block">{{ $order->gig->title }}</span>
                                    <span class="text-black-50 text-14 d-block">
                                        @php
                                            $current = Carbon\Carbon::now();
                                            $dt = new Carbon\Carbon($order->payment->created_at);
                                            // $dt->addDays(3);
                                            echo $dt->toFormattedDateString();
                                        @endphp
                                        </span>
                                </div>
                                <div class="col align-self-center col-md-2 pl-0 pr-0 text-center">
                                    <span class="badge badge-primary price-badge-light pl-3 pr-3">
                                        @if($order->payment->plan_type == "basic" )
                                           {{ $currency }} {{ $order->payment->basicPlan->price }}
                                        @endif
                                        @if($order->payment->plan_type == "standard" )
                                            {{ $currency }} {{ $order->payment->standardPlan->price }}
                                        @endif
                                        @if($order->payment->plan_type == "premium" )
                                            {{ $currency }} {{ $order->payment->premiumPlan->price }}
                                        @endif
                                    </span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col col-lg-5">
                    <div class="bg-white pt-5 pb-2 pl-3 pr-3">
                        <div class="row">
                            <div class="col col-lg-12">
                                <h3 class="font-weight-normal mb-4">Active Sales</h3>
                            </div>
                        </div>
                        @foreach ($pendingOrders as $order)
                            <div class="row fr-cl-nds-container-row border-top pt-3 pb-3 border-bottom-0">
                                <div class="col d-flex justify-content-center align-items-start col-md-3">
                                    <img class="img-fluid" src="{{ asset("storage/gig image/".$order->gig->image) }}" width="50">
                                </div>
                                <div class="col col-md-9 pl-0">
                                    <p class="text-16 font-weight-normal mb-0">{{ $order->gig->title }}</p>
                                    <p class="text-14 font-weight-normal mb-3 main-color">By {{ $order->user->name }}&nbsp;</p>
                                    <p><span class="badge badge-primary price-badge-thick mr-3 bg-info-light text-white mt-0">{{ $order->status }}</span></p>
                                </div>
                            </div>
                        @endforeach

                        {{-- <div class="row fr-cl-nds-container-row border-top pt-3 pb-3 border-bottom-0">
                            <div class="col d-flex justify-content-center align-items-start col-md-3"><img class="img-fluid" src="../assets/img/Editable-stationery-mockup.jpg" width="50"></div>
                            <div class="col col-md-9 pl-0">
                                <p class="text-16 font-weight-normal mb-0">Finance and Consulting Business Theme</p>
                                <p class="text-14 font-weight-normal mb-3 main-color">By John Whites&nbsp;</p>
                                <div>
                                    <div class="progress dashboard-recent-sales-bar">
                                        <div class="progress-bar bg-success progress-bar-striped" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"><span class="sr-only">60%</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row fr-cl-nds-container-row border-top pt-3 pb-3 border-bottom-0">
                            <div class="col d-flex justify-content-center align-items-start col-md-3"><img class="img-fluid" src="../assets/img/Editable-stationery-mockup.jpg" width="50"></div>
                            <div class="col col-md-9 pl-0">
                                <p class="text-16 font-weight-normal mb-0">Finance and Consulting Business Theme</p>
                                <p class="text-14 font-weight-normal mb-3 main-color">By John Whites&nbsp;</p>
                                <div>
                                    <div class="progress dashboard-recent-sales-bar">
                                        <div class="progress-bar bg-success progress-bar-striped" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"><span class="sr-only">60%</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row fr-cl-nds-container-row border-top pt-3 pb-3 border-bottom-0">
                            <div class="col d-flex justify-content-center align-items-start col-md-3"><img class="img-fluid" src="../assets/img/Editable-stationery-mockup.jpg" width="50"></div>
                            <div class="col col-md-9 pl-0">
                                <p class="text-16 font-weight-normal mb-0">Finance and Consulting Business Theme</p>
                                <p class="text-14 font-weight-normal mb-3 main-color">By John Whites&nbsp;</p>
                                <div>
                                    <div class="progress dashboard-recent-sales-bar">
                                        <div class="progress-bar bg-success progress-bar-striped" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;"><span class="sr-only">90%</span></div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col col-lg-6">
                    @seller
                        <div class="bg-white pt-5 pb-5 pl-3 pr-3">
                            <div class="row">
                                <div class="col col-lg-12">
                                    <h3 class="font-weight-normal mb-4">Recent Comments</h3>
                                </div>
                            </div>
                            @foreach ($reviews as $review)
                                <div class="row comment-card pt-4 pb-4 border-top">
                                    {{-- <div class="col text-center col-3 col-sm-2">
                                        <img class="rounded-circle" src="../assets/img/man%20(1).png" width="60" height="60">
                                    </div> --}}
                                    <div class="col">
                                        <h6 class="mb-1">{{ $review->user->name }}</h6>
                                        <p class="font-weight-light mt-1 font-weight-light text-black-50 text-16">
                                            @php
                                                $current = Carbon\Carbon::now();
                                                $dt = new Carbon\Carbon($review->created_at);
                                                // $dt->addDays(3);
                                                echo $dt->toFormattedDateString();
                                            @endphp
                                        </p>
                                        <p class="font-weight-normal mt-2">
                                            {{ $review->description }}
                                        </p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    @endSeller
                    @admin
                        <div class="bg-white pt-5 pb-5 pl-3 pr-3">
                            <div class="row">
                                <div class="col col-lg-12">
                                    <h3 class="font-weight-normal mb-4">Recent Sellers</h3>
                                </div>
                            </div>
                            @foreach ($users->where('type','seller')->take(10) as $user)
                                <div class="row comment-card pt-4 pb-4 border-top">
                                    <div class="col text-center col-3 col-sm-2">
                                        <img class="rounded-circle" src="{{ asset("storage/profile images/".$user->seller->profile_image) }}" width="60" height="60">
                                    </div>
                                    <div class="col">
                                        <h6 class="mb-1">{{ $user->name }}</h6>
                                        <p class="font-weight-light mt-1 font-weight-light text-black-50 text-16">
                                            {{ $user->seller->full_name }}
                                        </p>
                                        <p class="font-weight-normal mt-2">
                                            {{ $user->email }}
                                        </p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    @endAdmin
                </div>

                <div class="col col-lg-6">
                    <div class="bg-white pt-5 pb-2 pl-3 pr-3">
                        <div class="row">
                            <div class="col col-lg-8">
                                <h3 class="font-weight-normal mb-4">Recent Message</h3>
                            </div>
                            <div class="col col-lg-4 text-lg-right">
                                <h6 class="font-weight-normal mb-4 main-color">View All</h6>
                            </div>
                        </div>
                        @php
                            $cCount = 1;
                        @endphp
                        @foreach ($chats as $user)
                            @if ($cCount > 5)
                                @php
                                    break;
                                @endphp
                            @endif
                            <div class="row fr-cl-nds-container-row">
                                <div class="col d-flex justify-content-center align-items-center col-md-3">
                                    <img class="rounded-circle" src="{{ asset("img/boy%20(1).png") }}" height="45" width="45">
                                </div>
                                <div class="col col-md-9 pl-0">
                                    <p><span>{{ $user->name }}</span><br></p>
                                    <p>{{ $user->message }}</p>
                                    <span class="text-primary">
                                        @php
                                            $current = Carbon\Carbon::now();
                                            $dt = new Carbon\Carbon($user->at);
                                            echo $current->diffForHumans($dt);
                                        @endphp
                                    </span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </section>
    <script>
        var options = {
            chart: {
                type: 'bar',
                height: 550
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '30%',
                    endingShape: 'rounded'
                },
            },
            animations: {
                enabled: true,
                easing: 'easeinout',
                speed: 800,
                animateGradually: {
                    enabled: true,
                    delay: 150
                },
                dynamicAnimation: {
                    enabled: true,
                    speed: 350
                }
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            dataLabels: {
                enabled: false
            },
            series: [{
                name: 'Views',
                data: @php echo json_encode($visits); @endphp
            },
            {
                name: 'Impressions',
                data: @php echo json_encode($impressions); @endphp
            },
            {
                name: 'Sales',
                data: @php echo json_encode($sales); @endphp
            }

            ],
            xaxis: {
                categories: @php echo json_encode($names); @endphp,
            },
            yaxis: {
                title: {
                    text: 'numbers'
                }
            },
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val
                    }
                }
            }
        }

        var chart = new ApexCharts(document.querySelector("#chart1"), options);

        chart.render();



        var options2 = {
            chart: {
                type: 'donut',
            },
            colors:[
                "#28a745","#17a2b8","#ffc107"
            ],
            series: [{{ $completeOrders }}, {{ $deliveredOrders }}, {{ $InProgressOrders }}],
            total:{
                enable: true,
                label: "Total Orders",
                color:["#212121"]
            },
            labels: ['Completed Orders', 'Delivered Orders','Uncompleted Orders',],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 150
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        }

       var chart2 = new ApexCharts(
            document.querySelector("#chart2"),
            options2
        );

        chart2.render();

        // Chart #


    </script>
@endsection
