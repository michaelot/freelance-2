@extends('layouts.custom')
@section('content')
<div class="jumbotron d-flex justify-content-center align-items-center" id="home-main-jumbotron" style="background-image:url('{{ asset("storage/page background/".$data->background) }}')">
    <div>
        <h1 class="text-center">
            {{-- <span class="d-block">The Best</span>
            <span class="d-block bold">Freelance Market Place</span> --}}
            {{ $data->heading }}
        </h1>
        <p class="tagline">{{ $data->sub_heading }}</p>
        <div class="text-center mb-5 mb-sm-0">
            <a class="btn-lg mr-4 call-to-action" href="{{ route("gigs") }}">View All Products</a>
            <a class="btn-lg d-none d-sm-inline call-to-action" href="{{ route("categories") }}">Categories</a></div>
    </div>
</div>
<section id="home-main-searchbox">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="fr-cl-search-box">
                    <div class="row">
                        <div class="col col-md-12">
                            <form>
                                <div class="form-group row">
                                    <div class="col col-lg-7 col-sm-6 col-12 pr-sm-0"><input class="form-control form-control-lg" type="text" placeholder="Search your products..."></div>
                                    <div class="col col-lg-2 pl-sm-0 col-sm-6 col-12"><select class="form-control form-control-lg"><option value="12" selected="">All Categories</option><option value="13">This is item 2</option><option value="14">This is item 3</option></select></div>
                                    <div class="col col-lg-3 col-sm-6 offset-sm-3 offset-lg-0 col-12"><button class="btn btn-primary btn-block" type="button">Search Now</button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="home-main-features" class="section-padding">
    <div class="container">
        <div class="row">
            @foreach ($data->features as $item)
                <div class="col pt-5 pb-5 col-lg-4 col-sm-6 col-12">
                    <div class="text-center">
                        <img class="img-fluid" src="{{ asset("img/feature1.png") }}">
                    </div>
                    <div class="text-center">
                        <h3 class="pt-4 pb-4">{{ $item->title }}</h3>
                        <p>{{ $item->text }}.</p>
                    </div>
                </div>
            @endforeach
            {{-- <div class="col pt-5 pb-5 col-lg-4 col-sm-6 col-12">
                <div class="text-center">
                    <img class="img-fluid" src="{{ asset("img/feature1.png") }}">
                </div>
                <div class="text-center">
                    <h3 class="pt-4 pb-4">Best UX Research</h3>
                    <p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.</p>
                </div>
            </div>
            <div class="col pt-5 pb-5 col-lg-4 col-sm-6 col-12">
                <div class="text-center">
                    <img class="img-fluid" src="{{ asset("img/feature2.png") }}">
                </div>
                <div class="text-center">
                    <h3 class="pt-4 pb-4">Fully Responsive</h3>
                    <p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.</p>
                </div>
            </div>
            <div class="col pt-5 pb-5 col-lg-4 col-sm-6 offset-sm-3 offset-lg-0 col-12">
                <div class="text-center">
                    <img class="img-fluid" src="{{ asset("img/feature3.png") }}"></div>
                <div class="text-center">
                    <h3 class="pt-4 pb-4">Buy &amp; Sell Easily</h3>
                    <p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.</p>
                </div>
            </div> --}}
        </div>
    </div>
</section>
<section id="explore-the-market-place" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col">
                <h3 class="font-weight-bold mb-5">Explore our Marketplace</h3>
            </div>
        </div>
        <div class="row">
            @foreach ($categories as $category)
                <div class="col col-lg-3 mt-5" data-bs-hover-animate="pulse">
                    <div class="row">
                        <div class="col text-center">
                            <img class="img-fluid" src="{{ asset("storage/category icon/".$category->icon) }}" width="80">
                        </div>
                    </div>
                    <h5 class="text-center mt-4 font-weight-normal">{{ $category->name }}</h5>
                </div>
            @endforeach

            {{-- <div class="col col-lg-3 mt-5" data-bs-hover-animate="pulse">
                <div class="row">
                    <div class="col text-center">
                        <img class="img-fluid" src="assets/img/campaign.png" width="80">
                    </div>
                </div>
                <h5 class="text-center mt-4 font-weight-normal">Digital Marketing</h5>
            </div>
            <div class="col col-lg-3 mt-5" data-bs-hover-animate="pulse">
                <div class="row">
                    <div class="col text-center"><img class="img-fluid" src="assets/img/report.png" width="80"></div>
                </div>
                <h5 class="text-center mt-4 font-weight-normal">Writing &amp; Translation&nbsp;</h5>
            </div>
            <div class="col col-lg-3 mt-5" data-bs-hover-animate="pulse">
                <div class="row">
                    <div class="col text-center"><img class="img-fluid" src="assets/img/action-camera.png" width="80"></div>
                </div>
                <h5 class="text-center mt-4 font-weight-normal">Video &amp; Animation</h5>
            </div>
            <div class="col col-lg-3 mt-5" data-bs-hover-animate="pulse">
                <div class="row">
                    <div class="col text-center"><img class="img-fluid" src="assets/img/headphones.png" width="80"></div>
                </div>
                <h5 class="text-center mt-4 font-weight-normal">Music &amp; Audio</h5>
            </div>
            <div class="col col-lg-3 mt-5" data-bs-hover-animate="pulse">
                <div class="row">
                    <div class="col text-center"><img class="img-fluid" src="assets/img/coding.png" width="80"></div>
                </div>
                <h5 class="text-center mt-4 font-weight-normal">Programming &amp; Tech</h5>
            </div>
            <div class="col col-lg-3 mt-5" data-bs-hover-animate="pulse">
                <div class="row">
                    <div class="col text-center"><img class="img-fluid" src="assets/img/analytics.png" width="80"></div>
                </div>
                <h5 class="text-center mt-4 font-weight-normal">Business</h5>
            </div>
            <div class="col col-lg-3 mt-5" data-bs-hover-animate="pulse">
                <div class="row">
                    <div class="col text-center"><img class="img-fluid" src="assets/img/love.png" width="80"></div>
                </div>
                <h5 class="text-center mt-4 font-weight-normal">Lifestyle</h5>
            </div> --}}
        </div>
    </div>
</section>
<section id="home-main-featured-products" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row">
            <div class="col col-md-9 col-sm-8 col-6">
                <div>
                    <h2 class="mb-0">Weakly Featured Products</h2>
                </div>
            </div>
            <div class="col text-right col-md-3 col-sm-4 col-6">
                <div></div>
                <a id="prev" class="prev d-inline-block text-center" href="#">
                    <i class="icon-arrow-left"></i>
                </a>
                <a id="next" class="next d-inline-block text-center" href="#">
                    <i class="icon-arrow-right"></i>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col pl-0 pr-0">
                <div class="carousel slide" data-ride="carousel" data-interval="false" data-pause="false" data-keyboard="false" id="carousel-1">
                    <div class="carousel-inner" role="listbox">
                        @foreach ($gigs as $key => $gig)
                            <div class="carousel-item @if($key == 1) active @endif">
                                <div class="carousel-product row">
                                    <div class="col col-md-6 carousel-product-image col-12" style="background-image: url(&quot;{{ asset("storage/gig image/".$gig->image) }}&quot;);background-size: cover;background-repeat: no-repeat;"><span></span><img class="img-fluid d-block d-md-none" src="assets/img/image09.jpg"></div>
                                    <div class="col col-md-6 carousel-product-content col-12">
                                        <div class="row">
                                            <div class="col">
                                                <h4 class="pb-3"><a style="color:#212121" href="{{ route("gig.show",[$gig->id]) }}">{{ $gig->title }}</a></h4>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="d-flex align-items-center d-block float-left mr-3 user-avatar">
                                                    <div class="float-left">
                                                        <img src="{{ asset("storage/profile images/".$gig->user->seller->profile_image) }}" width="25" height="25"></div>
                                                    <div class="float-left pl-2">
                                                            <a style="color:#555555" href="{{ route("account.show",[$gig->user->id]) }}">{{ $gig->user->name }}</a>
                                                    </div>
                                                </div>
                                                <div class="float-left">
                                                    <p class="text-info"><i class="icon-layers"></i>&nbsp;{{ $gig->category_id }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <p class="mt-4 mb-4">{{ $gig->description }}</p>
                                            </div>
                                        </div>
                                        {{-- <div class="row">
                                            <div class="col tags mb-3"><a class="ml-2" href="#">plugin</a><a class="ml-2" href="#">plugin</a><a class="ml-2" href="#">plugin</a></div>
                                        </div> --}}
                                        <hr>
                                        <div class="row">
                                            <div class="col footer-section">
                                                <span class="badge badge-primary price-badge-thick mr-3">{{ $gig->basicPlan->price }} - {{ $gig->premiumPlan->price }}</span>
                                                <span class="badge badge-primary price-badge-light mr-3"><i class="icon-heart"></i>&nbsp; {{ count($gig->orders) }}</span>
                                                <span class="badge badge-primary price-badge-light mr-3"><i class="icon-basket"></i>&nbsp; 20</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="d-none">
                        <a class="carousel-control-prev text-truncate" href="#carousel-1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                            <span class="sr-only">Previous</span></a>
                            <a class="carousel-control-next" href="#carousel-1"
                            role="button" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="home-newest-release-products" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row">
            <div class="col col-md-9 col-sm-8 col-6">
                <div>
                    <h2 class="mb-0">Newest Release Products</h2>
                </div>
            </div>
            <div class="col d-flex align-items-center text-right col-md-3 col-sm-4 col-6">
                <div class="w-100">
                    <p class="float-left">Filter By</p>
                    <div class="dropdown float-right"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button"><i class="icon-options-vertical"></i></button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu"><a class="dropdown-item" role="presentation" href="#">First Item</a><a class="dropdown-item" role="presentation" href="#">Second Item</a><a class="dropdown-item" role="presentation" href="#">Third Item</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($gigs as $gig)
                @include('layouts.gig',['gig'=>$gig])
            @endforeach
            {{-- <div class="col col-lg-4 mb-3 col-md-6 col-12">
                <div class="row">
                    <div class="col"><img class="img-fluid d-block" src="assets/img/image09.jpg">
                        <div class="bg-white p-3 custom-box-shadow">
                            <h6 class="mb-1">One Page Wordpress Theme</h6>
                            <div>
                                <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                    <div class="float-left"><img src="assets/img/usr_avatar.png" width="25" height="25"></div>
                                    <div class="float-left pl-2">
                                        <p>John Timothy jakinson</p>
                                    </div>
                                </div>
                                <div>
                                    <p class="text-info"><i class="icon-layers"></i>&nbsp;Wordpress</p>
                                </div>
                            </div>
                            <p class="mt-2 mb-2">Nunc placerat mi id nisi interdum mollis.&nbsp;</p>
                            <hr>
                            <div class="footer-section"><span class="badge badge-primary price-badge-thick mr-3">$10 - $15</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-heart"></i>&nbsp; 90</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-basket"></i>&nbsp; 20</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-lg-4 mb-3 col-md-6 col-12">
                <div class="row">
                    <div class="col"><img class="img-fluid d-block" src="assets/img/business-card-letterhead-mockup-psd-elegant-free-stationery-branding-mock-up-on-behance-of-business-card-letterhead-mockup-psd.jpg">
                        <div class="bg-white p-3 custom-box-shadow">
                            <h5 class="mb-1">One Page Wordpress Theme</h5>
                            <div>
                                <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                    <div class="float-left"><img src="assets/img/usr_avatar.png" width="25" height="25"></div>
                                    <div class="float-left pl-2">
                                        <p>John Timothy jakinson</p>
                                    </div>
                                </div>
                                <div>
                                    <p class="text-info"><i class="icon-layers"></i>&nbsp;Wordpress</p>
                                </div>
                            </div>
                            <p class="mt-2 mb-2">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis.&nbsp;</p>
                            <hr>
                            <div class="footer-section"><span class="badge badge-primary price-badge-thick mr-3">$10 - $15</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-heart"></i>&nbsp; 90</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-basket"></i>&nbsp; 20</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-lg-4 mb-3 col-md-6 col-12">
                <div class="row">
                    <div class="col"><img class="img-fluid d-block" src="assets/img/Editable-stationery-mockup.jpg">
                        <div class="bg-white p-3 custom-box-shadow">
                            <h5 class="mb-1">One Page Wordpress Theme</h5>
                            <div>
                                <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                    <div class="float-left"><img src="assets/img/usr_avatar.png" width="25" height="25"></div>
                                    <div class="float-left pl-2">
                                        <p>John Timothy jakinson</p>
                                    </div>
                                </div>
                                <div>
                                    <p class="text-info"><i class="icon-layers"></i>&nbsp;Wordpress</p>
                                </div>
                            </div>
                            <p class="mt-2 mb-2">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis.&nbsp;</p>
                            <hr>
                            <div class="footer-section"><span class="badge badge-primary price-badge-thick mr-3">$10 - $15</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-heart"></i>&nbsp; 90</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-basket"></i>&nbsp; 20</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-lg-4 mb-3 col-md-6 col-12">
                <div class="row">
                    <div class="col"><img class="img-fluid d-block" src="assets/img/eye-for-ebony-354857-unsplash.jpg">
                        <div class="bg-white p-3 custom-box-shadow">
                            <h5 class="mb-1">One Page Wordpress Theme</h5>
                            <div>
                                <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                    <div class="float-left"><img src="assets/img/usr_avatar.png" width="25" height="25"></div>
                                    <div class="float-left pl-2">
                                        <p>John Timothy jakinson</p>
                                    </div>
                                </div>
                                <div>
                                    <p class="text-info"><i class="icon-layers"></i>&nbsp;Wordpress</p>
                                </div>
                            </div>
                            <p class="mt-2 mb-2">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis.&nbsp;</p>
                            <hr>
                            <div class="footer-section"><span class="badge badge-primary price-badge-thick mr-3">$10 - $15</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-heart"></i>&nbsp; 90</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-basket"></i>&nbsp; 20</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-lg-4 mb-3 col-md-6 col-12">
                <div class="row">
                    <div class="col"><img class="img-fluid d-block" src="assets/img/2Branding-Stationery-Mockup%20(1).jpg">
                        <div class="bg-white p-3 custom-box-shadow">
                            <h5 class="mb-1">One Page Wordpress Theme</h5>
                            <div>
                                <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                    <div class="float-left"><img src="assets/img/usr_avatar.png" width="25" height="25"></div>
                                    <div class="float-left pl-2">
                                        <p>John Timothy jakinson</p>
                                    </div>
                                </div>
                                <div>
                                    <p class="text-info"><i class="icon-layers"></i>&nbsp;Wordpress</p>
                                </div>
                            </div>
                            <p class="mt-2 mb-2">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis.&nbsp;</p>
                            <hr>
                            <div class="footer-section"><span class="badge badge-primary price-badge-thick mr-3">$10 - $15</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-heart"></i>&nbsp; 90</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-basket"></i>&nbsp; 20</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-lg-4 mb-3 col-md-6 col-12">
                <div class="row">
                    <div class="col"><img class="img-fluid d-block" src="assets/img/business-card-letterhead-mockup-psd-elegant-free-stationery-branding-mock-up-on-behance-of-business-card-letterhead-mockup-psd.jpg">
                        <div class="bg-white p-3 custom-box-shadow">
                            <h5 class="mb-1">One Page Wordpress Theme</h5>
                            <div>
                                <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                    <div class="float-left"><img src="assets/img/usr_avatar.png" width="25" height="25"></div>
                                    <div class="float-left pl-2">
                                        <p>John Timothy jakinson</p>
                                    </div>
                                </div>
                                <div>
                                    <p class="text-info"><i class="icon-layers"></i>&nbsp;Wordpress</p>
                                </div>
                            </div>
                            <p class="mt-2 mb-2">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis.&nbsp;</p>
                            <hr>
                            <div class="footer-section"><span class="badge badge-primary price-badge-thick mr-3">$10 - $15</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-heart"></i>&nbsp; 90</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-basket"></i>&nbsp; 20</span></div>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</section>
<section id="home-main-followers-feed" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row">
            <div class="col col-md-9 col-sm-8 col-6">
                <div>
                    <h2 class="mb-0">Your Followers Feed</h2>
                </div>
            </div>
            <div class="col text-right col-md-3 col-sm-4 col-6">
                <div></div><a id="prev2" class="prev d-inline-block text-center" href="#"><i class="icon-arrow-left"></i></a><a id="next2" class="next d-inline-block text-center" href="#"><i class="icon-arrow-right"></i></a></div>
        </div>
        <div class="row">
            <div class="col pl-0 pr-0">
                <div class="carousel slide" data-ride="carousel" data-interval="false" data-pause="false" data-keyboard="false" id="carousel-2">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="row">
                                @foreach ($gigs->take(6) as $gig)
                                    @include('layouts.gig',['gig'=>$gig])
                                @endforeach
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                @foreach ($gigs->take(6) as $gig)
                                    @include('layouts.gig',['gig'=>$gig])
                                @endforeach
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                @foreach ($gigs->take(6) as $gig)
                                    @include('layouts.gig',['gig'=>$gig])
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="d-none"><a class="carousel-control-prev text-truncate" href="#carousel-2" role="button" data-slide="prev"><span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carousel-2" role="button" data-slide="next"><span class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="home-main-why-choose-us" class="section-padding">
    <div class="container">
        <div class="row section-header-row">
            <div class="col">
                <h2 class="text-center">Why Choose us</h2>
            </div>
        </div>
        <div class="row">
            @foreach ($data->wcu as $item)
                <div class="col pt-5 pb-5 col-lg-4 col-sm-6 col-12">
                    <div class="text-center">
                        <img class="img-fluid" src="{{ asset("img/feature1.png") }}">
                    </div>
                    <div class="text-center">
                        <h3 class="pt-4 pb-4">{{ $item->title }}</h3>
                        <p>{{ $item->text }}.</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<section id="home-main-testimonials" class="section-padding" style="background-image: url(&quot;/img/test%20background.png&quot;);">
    <div class="container">
        <div class="row section-header-row">
            <div class="col">
                <h2 class="text-center mb-5">What they said about us</h2>
            </div>
        </div>
        <div class="row">
            <div class="carousel slide" data-ride="carousel" data-interval="false" data-pause="false" data-keyboard="false" id="carousel-3">
                <div class="carousel-inner" role="listbox">
                       
                    {{-- <div class="carousel-item active">
                        <div class="row">
                            <div class="col col-lg-5 mb-3 offset-lg-1 col-md-10 offset-md-1 offset-lg-0 col-8 offset-2">
                                <div class="row">
                                    <div class="col">
                                        <div class="border rounded shadow bg-white p-4 custom-box-shadow">
                                            <h1><i class="fa fa-quote-left"></i></h1>
                                            <p class="mt-2 mb-2 text-muted font-weight-light">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis.&nbsp;</p>
                                            <hr>
                                            <div>
                                                <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                                    <div class="float-left"><img src="assets/img/usr_avatar.png" width="45" height="45"></div>
                                                    <div class="float-left pl-2">
                                                        <p class="lead font-weight-normal">John Timothy jakinson</p>
                                                        <p>CEO Apple</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-lg-5 mb-3 offset-lg-0 col-md-10 offset-md-1 offset-lg-0 col-8 offset-2">
                                <div class="row">
                                    <div class="col">
                                        <div class="border rounded shadow bg-white p-4 custom-box-shadow">
                                            <h1><i class="fa fa-quote-left"></i></h1>
                                            <p class="mt-2 mb-2 text-muted font-weight-light">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis.&nbsp;</p>
                                            <hr>
                                            <div>
                                                <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                                    <div class="float-left"><img src="assets/img/usr_avatar.png" width="45" height="45"></div>
                                                    <div class="float-left pl-2">
                                                        <p class="lead font-weight-normal">John Timothy jakinson</p>
                                                        <p>CEO Apple</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col col-lg-5 mb-3 offset-lg-1 col-md-10 offset-md-1 offset-lg-0 col-8 offset-2">
                                <div class="row">
                                    <div class="col">
                                        <div class="border rounded shadow bg-white p-4 custom-box-shadow">
                                            <h1><i class="fa fa-quote-left"></i></h1>
                                            <p class="mt-2 mb-2 text-muted font-weight-light">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis.&nbsp;</p>
                                            <hr>
                                            <div>
                                                <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                                    <div class="float-left"><img src="assets/img/usr_avatar.png" width="45" height="45"></div>
                                                    <div class="float-left pl-2">
                                                        <p class="lead font-weight-normal">John Timothy jakinson</p>
                                                        <p>CEO Apple</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-lg-5 mb-3 offset-lg-0 col-md-10 offset-md-1 offset-lg-0 col-8 offset-2">
                                <div class="row">
                                    <div class="col">
                                        <div class="border rounded shadow bg-white p-4 custom-box-shadow">
                                            <h1><i class="fa fa-quote-left"></i></h1>
                                            <p class="mt-2 mb-2 text-muted font-weight-light">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis.&nbsp;</p>
                                            <hr>
                                            <div>
                                                <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                                    <div class="float-left"><img src="assets/img/usr_avatar.png" width="45" height="45"></div>
                                                    <div class="float-left pl-2">
                                                        <p class="lead font-weight-normal">John Timothy jakinson</p>
                                                        <p>CEO Apple</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div>
                    <a class="carousel-control-prev text-truncate" href="#carousel-3" role="button" data-slide="prev">
                        <div class="d-flex justify-content-center align-items-center bg-dark"><i class="fas fa-arrow-alt-circle-left"></i></div><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carousel-3" role="button" data-slide="next"><span class="sr-only">Next</span><div class="d-flex justify-content-center align-items-center bg-dark"><i class="fas fa-arrow-alt-circle-right"></i></div></a></div>
            </div>
        </div>
    </div>
</section>
<section id="home-main-call-to-action" class="section-padding" style="background-image:url('{{ asset("storage/page background/".$data->action_background) }}')">
    <div class="container">
        <div class="row">
            <div class="col">
                <div>
                    <h1 class="text-center text-white font-weight-normal mb-3">{{ $data->action_heading }}</h1>
                    <p class="text-center text-white">{{ $data->action_sub_heading }}</p>
                    <div class="text-center mb-5 mb-sm-0 mt-5">
                        <a class="btn-lg mr-4 call-to-action" href="{{ route("gigs") }}">Start Shopping</a>
                        <a class="btn-lg d-none d-sm-inline call-to-action" href="{{ route("account.one") }}">Become an Authur</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $("#home-main-featured-products #prev").on("click",function(e){
    e.preventDefault()
    $("#carousel-1").carousel("prev")
})

$("#home-main-featured-products #next").on("click",function(e){
    e.preventDefault()
    $("#carousel-1").carousel("next")
})

$("#prev2").on("click",function(e){
    e.preventDefault()
    $("#carousel-2").carousel("prev")
})

$("#next2").on("click",function(e){
    e.preventDefault()
    $("#carousel-2").carousel("next")
})
</script>
@endsection
