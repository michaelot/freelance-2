@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-4"><strong>{{ $data->heading }}</strong><br></h2>
                <h5 class="font-weight-normal text-white-50 mb-3">{{ $data->sub_heading }}</h5>
            </div>
        </div>
    </div>
</div>
<section id="main-category-listing" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="row">
                    @foreach ($categories as $category)
                        <div class="col col-lg-3 mb-3">
                            <a href="/gigs?a={{ $category->id }}">
                                <div class="category-card mb-3">
                                    <img height="200" class="rounded img-fluid" src="{{ asset("storage/category images/".$category->image) }}">
                                    <p class="font-weight-bold mt-1">{{ $category->name }}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach

                </div>

            </div>
        </div>
    </div>
</section>

@endsection
