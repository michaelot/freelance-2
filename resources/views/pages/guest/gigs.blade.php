@extends('layouts.custom')
@section('appjs')
    <meta name="page" content="gigListing">
    <script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
    <div id="app">
        <gig-page v-bind:pagedata="{{ $data }}" v-bind:categories="{{ $categories }}" v-bind:gigdata="{{ $gigs }}" v-bind:currency="'{{ $currency }}'"></gig-page>



        @regular
            <section id="home-main-call-to-action" class="section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div>
                                <h1 class="text-center text-white font-weight-normal mb-3">Ready To Join Our Market Place</h1>
                                <p class="text-center text-white">MartPlace is the most powerful, &amp; customizable template for Easy Digital Downloads Products</p>
                                <div class="text-center mb-5 mb-sm-0 mt-5"><a class="btn-lg mr-4 call-to-action" href="#">Start Shopping</a><a class="btn-lg d-none d-sm-inline call-to-action" href="#">Become an Authur</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endRegular
    </div>
@endsection
