@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Manage Gigs<br></h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Dashboard</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"users"])

<section id="statement" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row">
            <div class="col align-self-center col-lg-8">
                <h2>Users</h2>
            </div>
            <div class="col col-lg-4 text-right">
                <form>
                    <div class="form-group">
                        <div class="input-group"><input class="form-control main-input-form" type="text">
                            <div class="input-group-append"><button class="btn btn-primary fr-cl-bcs mr-0" type="button">Search</button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col col-lg-3">
                <div class="shadow d-flex align-items-center flex-wrap bg-success-light pt-4 pb-4 rounded-lg pl-3">
                    <div class="d-flex justify-content-center align-items-center account-statement-icon bg-white mr-4 mb-2">
                        <p class="text-center text-success-light"><i class="icon-tag"></i></p>
                    </div>
                    <div class="mb-2">
                        <h2 class="mb-3 text-white">{{ count($users->where("type","seller")) }}</h2>
                        <p class="text-white">Sellers </p>
                    </div>
                </div>
            </div>
            <div class="col col-lg-3">
                <div class="shadow d-flex align-items-center flex-wrap bg-warning-light pt-4 pb-4 rounded-lg pl-3">
                    <div class="d-flex justify-content-center align-items-center account-statement-icon bg-white mr-4 mb-2">
                        <p class="text-center text-warning-light"><i class="icon-basket"></i></p>
                    </div>
                    <div class="mb-2">
                        <h2 class="mb-3 text-white">{{ count($users->where("type","regular")) }}</h2>
                        <p class="text-white">Regular</p>
                    </div>
                </div>
            </div>
            <div class="col col-lg-3">
                <div class="shadow d-flex align-items-center flex-wrap bg-danger-light pt-4 pb-4 rounded-lg pl-3">
                    <div class="d-flex justify-content-center align-items-center account-statement-icon bg-white mr-4 mb-2 text-info-light">
                        <p class="text-danger-light"><i class="icon-briefcase"></i></p>
                    </div>
                    <div class="mb-2">
                        <h2 class="mb-3 text-white">{{ count($users->where("type","regular")) }}</h2>
                        <p class="text-white">Disabled</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row bg-white rounded pt-4 pb-4">
            <div class="col">
                <div id="orders-tabs">
                    <ul class="nav nav-pills justify-content-center" id="orders-tabs">
                        <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">Regulars</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">Sellers</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pt-5" role="tabpanel" id="tab-1">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="border-top-0">
                                        <tr>
                                            <th class="border-top-0">Joined</th>
                                            <th class="border-top-0">Name</th>
                                            <th class="border-top-0">Email</th>
                                            <th class="border-top-0">type</th>
                                            <th class="border-top-0">purchases</th>
                                            <th class="border-top-0">payments</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users->where("type","regular") as $user)
                                            <tr>
                                                <td class="text-14">
                                                    @php
                                                        $current = Carbon\Carbon::now();
                                                        $dt = new Carbon\Carbon($user->created_at);
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14">{{ $user->name }}</td>
                                                <td class="text-14 font-weight-bold">{{ $user->email }}</td>
                                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-light mr-3 bg-success-light text-white">{{ $user->type }}</span></td>
                                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-pink-light mr-3 text-16">{{ count($user->orders) }}</span></td>
                                                <td class="text-14 font-weight-bold"><a class="btn btn-primary fr-cl-bcs text-white font-weight-normal shadow-0" role="button">{{ $setting->currency }} {{ $user->payments->sum("amount") }}</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane pt-5" role="tabpanel" id="tab-2">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="border-top-0">
                                        <tr>
                                            <th class="border-top-0">Joined</th>
                                            <th class="border-top-0">Name</th>
                                            <th class="border-top-0">full name</th>
                                            <th class="border-top-0">Email</th>
                                            <th class="border-top-0">type</th>
                                            <th class="border-top-0">Gigs</th>
                                            {{-- <th class="border-top-0">Orders</th> --}}
                                            <th class="border-top-0">purchases</th>
                                            <th class="border-top-0">payments</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users->where("type","seller") as $user)
                                            <tr>
                                                <td class="text-14">
                                                    @php
                                                        $current = Carbon\Carbon::now();
                                                        $dt = new Carbon\Carbon($user->created_at);
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14"><a href="{{ route("account.show",[$user->id]) }}"> {{ $user->id }}</a></td>
                                                <td class="text-14">{{ $user->seller->full_name }}</td>
                                                <td class="text-14 font-weight-bold">{{ $user->email }}</td>
                                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-light mr-3 bg-success-light text-white">{{ $user->type }}</span></td>
                                                <td class="text-14">{{ count($user->gigs) }}</td>
                                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-pink-light mr-3 text-16">{{ count($user->orders) }}</span></td>
                                                <td class="text-14 font-weight-bold"><a class="btn btn-primary fr-cl-bcs text-white font-weight-normal shadow-0" role="button">{{ $setting->currency }} {{ $user->payments->sum("amount") }}</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
