@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron" style="background-image:url('{{ asset("storage/page background/".$data->background) }}')">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left"><strong>{{ $data->heading }}</strong><br></h2>
                <h5 class="font-weight-normal text-white-50 mb-3 text-left">{{ $data->sub_heading }}</h5>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Login</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
<section id="login-page-main" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-6 offset-lg-3 bg-white pt-5 pb-5">
                <div class="row border-bottom">
                    <div class="col">
                        <h2 class="font-weight-normal text-center mb-3">Welcome Back</h2>
                        <p class="text-center mb-3">You can sign in with your Email</p>
                    </div>
                </div>
                <form class="mt-4" action="{{ route('login') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col col-lg-10 offset-lg-1">
                                <label class="font-weight-bold mb-2">Email</label>
                                <input value="{{ old("email") }}" name="email" class="form-control main-input-form @error('email') is-invalid @enderror" style="@error('email') border:1px solid #dc3545 !important; color:#dc3545 !important; @enderror" type="email" placeholder="Enter your email..." required>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col col-lg-10 offset-lg-1">
                                <label class="font-weight-bold mb-2 mt-2">Password</label>
                                <input name="password" class="form-control main-input-form @error('password') is-invalid @enderror" style="@error('password') border:1px solid #dc3545 !important; color:#dc3545 !important; @enderror" type="password" placeholder="Enter your password..." required>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col col-lg-10 offset-lg-1">
                                <p class="text-black-50">Don't have an&nbsp;<a href="{{ route("register") }}">account</a>?&nbsp;</p>
                                <button class="btn btn-primary btn-block font-weight-bold pt-2 pb-2 mt-3" type="submit">Login Now</button>
                                <p class="mt-5">Forgot <a href="#">password</a>?</p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
