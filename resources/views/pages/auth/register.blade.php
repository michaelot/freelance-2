@extends('layouts.custom')
@section('content')
    <div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron" style="background-image:url('{{ asset("storage/page background/".$data->background) }}')">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="text-white mb-3 text-left"><strong>{{ $data->heading }}</strong><br></h2>
                    <h5 class="font-weight-normal text-white-50 mb-3 text-left">{{ $data->sub_heading }}</h5>
                    <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Login</span>&nbsp;&nbsp;</p>
                </div>
            </div>
        </div>
    </div>
    <section id="login-page-main" class="section-padding">
        <div class="container">
            {{-- @include('layouts.alert') --}}
            <div class="row">
                <div class="col col-lg-6 offset-lg-3 bg-white pt-5 pb-5">
                    <div class="row border-bottom">
                        <div class="col col-lg-10 offset-lg-1">
                            <h2 class="font-weight-normal text-center mb-3">Create Your Account</h2>
                            <p class="text-center mb-3">Please fill the following fields with appropriate information to register.</p>
                        </div>
                    </div>
                    <form action="{{ route('register') }}" id="register" method="POST" class="mt-4">
                        @csrf
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-10 offset-lg-1">
                                    <label class="font-weight-bold mb-2">Username/Company Name</label>
                                    <input name="name" class="form-control main-input-form @error('name') is-invalid @enderror" type="text" placeholder="Enter your username..." value="{{ old('name') }}" required autocomplete="name">

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-10 offset-lg-1">
                                    <label class="font-weight-bold mb-2">Full Name</label>
                                    <input v-model="fullName" value="{{ old("full_name") }}" name="full_name" class="form-control main-input-form @error('full_name') is-invalid @enderror" type="text" placeholder="Enter your full name..." required>
                                    @error('full_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-10 offset-lg-1">
                                    <label class="font-weight-bold mb-2">Email</label>
                                    <input value="{{ old("email") }}" name="email" class="form-control main-input-form @error('email') is-invalid @enderror" type="email" placeholder="Enter your email..." required>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-10 offset-lg-1">
                                    <label class="font-weight-bold mb-2 mt-2">Password</label>
                                    <input name="password" class="form-control main-input-form @error('password') is-invalid @enderror"  type="password" placeholder="Enter your password..." required>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-10 offset-lg-1">
                                    <label class="font-weight-bold mb-2 mt-2">Confirm Password</label>
                                    <input name="password_confirmation" class="form-control main-input-form @error('password_confirm') is-invalid @enderror" type="password" placeholder="Confirm password..." required>
                                    @error('password_confirm')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-10 offset-lg-1">
                                    <p class="text-black-50">Already have an&nbsp;<a href="{{ route('login') }}">account</a>?&nbsp;</p>
                                    <button class="btn btn-primary btn-block font-weight-bold pt-2 pb-2 mt-3" type="submit">Register Now</button></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
