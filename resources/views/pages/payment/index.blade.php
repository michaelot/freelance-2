@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Payments<br></h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Dashboard</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"payments"])

<section id="statement" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row">
            <div class="col align-self-center col-lg-8">
                <h2>Payment List</h2>
            </div>
            <div class="col col-lg-4 text-right">
                <form>
                    <div class="form-group">
                        <div class="input-group"><input class="form-control main-input-form" type="text">
                            <div class="input-group-append"><button class="btn btn-primary fr-cl-bcs mr-0" type="button">Search</button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row bg-white rounded pt-4 pb-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="border-top-0">
                            <tr>
                                <th class="border-top-0">Date</th>
                                <th class="border-top-0">Paid By</th>
                                <th class="border-top-0">Gig</th>
                                <th class="border-top-0">Paid to</th>
                                <th class="border-top-0">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($payments as $payment)
                                <tr>
                                    <td class="text-14">
                                        @php
                                            $current = Carbon\Carbon::now();
                                            $dt = new Carbon\Carbon($payment->created_at);
                                            // $dt->addDays(3);
                                            echo $dt->toFormattedDateString();
                                        @endphp
                                    </td>
                                    <td class="text-14 font-weight-bold">{{ $payment->user->name }}</td>
                                    <td class="text-14 font-weight-bold"><a href="{{ route("gig.show",[$payment->gig->id]) }}">{{ $payment->gig->title }}</a></td>
                                    <td class="text-14 font-weight-bold"><a href="{{ route("account.show",[$payment->gig->user->id]) }}">{{ $payment->gig->user->name }}</a></td>
                                    {{-- <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-light mr-3 bg-success-light text-white">Sale</span></td> --}}
                                    <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-pink-light mr-3 text-16">{{ $setting->currency }} {{ $payment->amount }}</span></td>
                                    {{-- <td class="text-14 font-weight-bold"><a class="btn btn-primary fr-cl-bcs text-white font-weight-normal shadow-0" role="button">View</a></td> --}}
                                </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
