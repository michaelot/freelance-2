@extends('layouts.custom')
@section('content')
{{-- <div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron" style="background-image:url('{{ asset("storage/page background/".$data->background) }}')">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left"><strong>{{ $data->heading }}</strong><br></h2>
                <h5 class="font-weight-normal text-white-50 mb-3 text-left">{{ $data->sub_heading }}</h5>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Login</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div> --}}
<section id="login-page-main" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-6 offset-lg-3 bg-white pt-5 pb-5">
                <div class="row border-bottom">
                    <div class="col">
                        <h2 class="font-weight-normal text-center mb-3">Welcome Back</h2>
                        <p class="text-center mb-3">You can sign in with your Email</p>
                    </div>
                </div>
                @include('layouts.alert')
                <form method="POST" action="{{ route('paypal') }}" id="paymentForm">
                    {{ csrf_field() }}
                    <input name="amount" value="500" /> <!-- Replace the value with your transaction amount -->
                    <input type="submit" value="Buy"  />
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
