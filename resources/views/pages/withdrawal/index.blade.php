@extends('layouts.custom')
@section('content')
<style>
    #stripeBtn{

        font-weight: 400!important;
        display: block !important;
        width: auto !important;
        padding: 0px 20px !important;
        border-radius: 21px !important;
        line-height: 34px !important;
        font-family: Quicksand, sans-serif;
        color: #fff !important;
        background: none !important;
        background-color: #7345BF !important;
        border-color: #7345BF !important;
        height:auto !important;
        font-size: 1rem !important;
        margin: 0px !important;
    }
</style>
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Manage Gigs<br></h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Dashboard</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"withdrawal"])

<section id="statement" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row">
            <div class="col align-self-center col-lg-8">
                <h2>Withdrawal</h2>
            </div>
            <div class="col col-lg-4 text-right">
                <form>
                    <div class="form-group">
                        <div class="input-group"><input class="form-control main-input-form" type="text">
                            <div class="input-group-append"><button class="btn btn-primary fr-cl-bcs mr-0" type="button">Search</button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @seller
            <div class="row mb-5 bg-white pt-5 pb-5">
                <div class="col col-lg-6 col-12">
                    <h3 class="mb-4" x="">Payment Setting</h3>
                    <hr class="mb-4">
                    <form action="{{ route("withdraw.setting") }}" method="POST">
                        @csrf
                        @method("put")
                        <div class="mt-4">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Paypal Secret Key&nbsp;<span class="span-required">*</span></label>
                                        <input name="secret_key" class="form-control main-input-form" type="text" placeholder="Enter Secret Key..." value="{{ auth()->user()->paymentInfo ? auth()->user()->paymentInfo->secret_key : ""   }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Paypal Client ID&nbsp;<span class="span-required">*</span></label>
                                        <input name="client_id" class="form-control main-input-form" type="text" placeholder="Enter Client ID..." value="{{ auth()->user()->paymentInfo ? auth()->user()->paymentInfo->publishable_key : ""  }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right">
                                <button class="btn btn-primary fr-cl-bcs font-weight-normal mt-4 mr-0" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col col-lg-6 col-12">
                    <h3 class="mb-4" x="">Withdraw Amount</h3>
                    <hr class="mb-4">
                    <form action="{{ route("withdraw.store") }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <p class="text-20 font-weight-normal mb-3">How much amount would you like to Withdraw?</p>
                            <div class="form-check pt-3 pb-3 bg-light rounded border-bottom shadow-sm mb-2">
                                <input name="withdrawal_type" value="available" class="form-check-input ml-0" type="radio" id="formCheck-1" checked="">
                                <label class="form-check-label ml-4" for="formCheck-1">Available Balance:&nbsp;<span class="font-weight-bold">&nbsp;{{ $currency }} {{ $available }}</span></label></div>
                            <div
                                class="form-check pt-3 pb-3 bg-light rounded border-bottom shadow-sm mb-2">
                                <input name="withdrawal_type" value="partial" class="form-check-input ml-0" type="radio" id="formCheck-partial-amount">
                                <label class="form-check-label ml-4" for="formCheck-partial-amount">A Partial Amount....</label>
                            </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col col-lg-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text border-0 form-control main-input-form">{{ $currency }}</span>
                                </div>
                                <input name="amount" class="form-control main-input-form" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button class="btn btn-primary fr-cl-bcs font-weight-normal mt-4 mr-0" type="submit">Submit Withdrawal</button>
                </div>
                </form>
                </div>
            </div>
        @endSeller

    @seller
        <div class="row bg-white rounded pt-4 pb-4">
            <div class="col">
                <h3 class="mb-4">Withdrawal History</h3>
                <hr class="mb-4">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="border-top-0">
                            <tr>
                                <th class="border-top-0">Date</th>
                                <th class="border-top-0">Amount</th>
                                <th class="border-top-0">Status</th>
                                <th class="border-top-0">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($withdrawals->where("user_id",auth()->id()) as $item)
                                <tr>
                                    <td class="text-14">
                                        @php
                                            $current = Carbon\Carbon::now();
                                            $dt = new Carbon\Carbon($item->created_at);
                                            echo $dt->toFormattedDateString();
                                        @endphp
                                    </td>
                                    <td class="text-14 font-weight-bold">
                                        <span class="badge badge-primary price-badge-pink-light mr-3 text-16">
                                            {{ $item->amount }}
                                        </span>
                                    </td>
                                    <td class="text-14 font-weight-bold">

                                        @if($item->status == "Paid")
                                            <span class="badge  price-badge-thick mr-3 bg-success-light text-white" style="">
                                                {{ $item->status }}
                                            </span>
                                        @elseif($item->status == "Pending")
                                        <span class="badge price-badge-thick mr-3 bg-info-light text-white">
                                                {{ $item->status }}
                                            </span>
                                        @elseif($item->status == "Canceled")
                                            <span class="badge price-badge-thick mr-3 bg-danger-light text-white" style="background-color:#FA6A6D  !important;">
                                                    {{ $item->status }}
                                            </span>
                                        @endif


                                    </td>
                                    <td class="text-14 font-weight-bold">
                                        @if($item->status != "Paid")
                                            <form action="{{ route("withdraw.cancel",[$item->id]) }}" method="POST">
                                                @csrf
                                                @method("put")
                                                <button class="btn btn-primary fr-cl-bcs bg-purple text-white font-weight-normal border-0" role="button">Cancel</button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endSeller
    @admin
        <div class="row bg-white rounded pt-5 pb-4">
            <div class="col">
                <div id="orders-tabs">
                    <ul class="nav nav-pills justify-content-center" id="orders-tabs">
                        <li class="nav-item"><a class="nav-link  active" role="tab" data-toggle="tab" href="#tab-1">Pending</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">Paid</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-3">Canceled</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pt-4" role="tabpanel" id="tab-1">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="border-top-0">
                                        <tr>
                                            <th class="border-top-0">Date</th>
                                            <th class="border-top-0">User</th>
                                            <th class="border-top-0">Amount</th>
                                            <th class="border-top-0">Status</th>
                                            <th class="border-top-0">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($withdrawals->where("status","Pending") as $item)
                                            <tr>
                                                <td class="text-14">
                                                    @php
                                                        $current = Carbon\Carbon::now();
                                                        $dt = new Carbon\Carbon($item->created_at);
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14 font-weight-bold">
                                                    {{ $item->user->name }}
                                                </td>
                                                <td class="text-14 font-weight-bold">
                                                    <span class="badge badge-primary price-badge-pink-light mr-3 text-16">
                                                        {{ $item->amount }}
                                                    </span>
                                                </td>
                                                <td class="text-14 font-weight-bold">

                                                    @if($item->status == "Paid")
                                                        <span class="badge  price-badge-thick mr-3 bg-success-lighttext-white" >
                                                            {{ $item->status }}
                                                        </span>
                                                    @elseif($item->status == "Pending")
                                                    <span class="badge price-badge-thick mr-3 bg-info-light text-white">
                                                            {{ $item->status }}
                                                        </span>
                                                    @elseif($item->status == "Canceled")
                                                        <span class="badge price-badge-thick mr-3 bg-danger-light text-white" style="background-color:#FA6A6D  !important;">
                                                                {{ $item->status }}
                                                        </span>
                                                    @endif


                                                </td>
                                                <td class="text-14 font-weight-bold">
                                                        <form method="POST" action="{{ route('withdraw.pay',[$item->id]) }}">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                                            <input type="hidden" name="user_id" value="{{ $item->user->id }}">
                                                            <input type="hidden" name="name" value="{{ $item->user->name }}">
                                                            <input type="hidden" name="amount" value="{{ $item->amount }}">
                                                            <input id="stripeBtn" class="btn-block border-0 p-0 mt-0" type="submit" value="Payoff"  />
                                                        </form>
                                                        {{-- <button class="btn btn-primary fr-cl-bcs bg-purple text-white font-weight-normal border-0" role="button">Cancel</button> --}}
                                                    {{-- <form method="POST" action="{{ route('withdraw.pay',[$item->id]) }}">
                                                        @csrf
                                                        @method("PUT")
                                                        <script
                                                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                                data-key="{{ $publishable_key }}"
                                                                data-amount="{{ $item->amount * 100 }}"
                                                                data-name="Freelance App"
                                                                data-description="Pay {{ $item->user->name }}"
                                                                data-locale="auto"
                                                                data-currency="{{ $currency }}">
                                                        </script>
                                                        <script>
                                                            $("button[type='submit'].stripe-button-el span").attr("id","stripeBtn");
                                                            $("button[type='submit'].stripe-button-el span").attr("class","btn btn-primary btn-block font-weight-bold pt-2 pb-2 stripeBtn");
                                                            $("button[type='submit'].stripe-button-el").attr("class","btn-block border-0 p-0");
                                                            $(".stripeBtn").text("Pay")
                                                        </script>
                                                    </form> --}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane pt-4" role="tabpanel" id="tab-2">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="border-top-0">
                                        <tr>
                                            <th class="border-top-0">Date</th>
                                            <th class="border-top-0">User</th>
                                            <th class="border-top-0">Amount</th>
                                            <th class="border-top-0">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($withdrawals->where("status","Paid") as $item)
                                            <tr>
                                                <td class="text-14">
                                                    @php
                                                        $current = Carbon\Carbon::now();
                                                        $dt = new Carbon\Carbon($item->created_at);
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14 font-weight-bold">
                                                    {{ $item->user->name }}
                                                </td>
                                                <td class="text-14 font-weight-bold">
                                                    <span class="badge badge-primary price-badge-pink-light mr-3 text-16">
                                                        {{ $item->amount }}
                                                    </span>
                                                </td>
                                                <td class="text-14 font-weight-bold">

                                                    @if($item->status == "Paid")
                                                        <span class="badge  price-badge-thick mr-3 bg-success-lighttext-white" >
                                                            {{ $item->status }}
                                                        </span>
                                                    @elseif($item->status == "Pending")
                                                    <span class="badge price-badge-thick mr-3 bg-info-light text-white">
                                                            {{ $item->status }}
                                                        </span>
                                                    @elseif($item->status == "Canceled")
                                                        <span class="badge price-badge-thick mr-3 bg-danger-light text-white" style="background-color:#FA6A6D  !important;">
                                                                {{ $item->status }}
                                                        </span>
                                                    @endif
                                                </td>

                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane pt-4" role="tabpanel" id="tab-3">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="border-top-0">
                                        <tr>
                                            <th class="border-top-0">Date</th>
                                            <th class="border-top-0">User</th>
                                            <th class="border-top-0">Amount</th>
                                            <th class="border-top-0">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($withdrawals->where("status","Canceled") as $item)
                                            <tr>
                                                <td class="text-14">
                                                    @php
                                                        $current = Carbon\Carbon::now();
                                                        $dt = new Carbon\Carbon($item->created_at);
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14 font-weight-bold">
                                                    {{ $item->user->name }}
                                                </td>
                                                <td class="text-14 font-weight-bold">
                                                    <span class="badge badge-primary price-badge-pink-light mr-3 text-16">
                                                        {{ $item->amount }}
                                                    </span>
                                                </td>
                                                <td class="text-14 font-weight-bold">

                                                    @if($item->status == "Paid")
                                                        <span class="badge  price-badge-thick mr-3 bg-success-lighttext-white" >
                                                            {{ $item->status }}
                                                        </span>
                                                    @elseif($item->status == "Pending")
                                                    <span class="badge price-badge-thick mr-3 bg-info-light text-white">
                                                            {{ $item->status }}
                                                        </span>
                                                    @elseif($item->status == "Canceled")
                                                        <span class="badge price-badge-thick mr-3 bg-danger-light text-white" style="background-color:#FA6A6D  !important;">
                                                                {{ $item->status }}
                                                        </span>
                                                    @endif


                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endAdmin
    </div>
</section>
@endsection
