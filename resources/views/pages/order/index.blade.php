@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Sales<br></h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Dashboard</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"orders"])
<section id="statement" class="section-padding bg-color">
    <div class="container">
        <div class="row bg-white rounded pt-4 pb-4">
            <div class="col">
                <div id="orders-tabs">
                    <ul class="nav nav-pills justify-content-center" id="orders-tabs">
                        <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">Active</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">Delivered</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-3">Completed</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pt-5" role="tabpanel" id="tab-1">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="border-top-0">
                                        <tr>
                                            <th class="border-top-0">Buyer</th>
                                            @admin
                                                <th class="border-top-0">Seller</th>
                                            @endAdmin
                                            <th class="border-top-0">Gig</th>
                                            <th class="border-top-0">Started On</th>
                                            <th class="border-top-0">Due On</th>
                                            <th class="border-top-0">Price</th>
                                            <th class="border-top-0">Progress</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($orders->where("status","In Progress") as $order)
                                            <tr>
                                                <td class="text-14">{{ $order->user->name }}</td>
                                                @admin
                                                    <td class="text-14">{{ $order->gig->user->name }}</td>
                                                @endAdmin
                                                {{ $order->payment->plan_type }}
                                                <td class="text-14">
                                                    @seller
                                                        <a href="{{ route("order.show",[$order->id]) }}">{{ $order->gig->title }}</a>
                                                    @endSeller
                                                    @admin
                                                        <a href="{{ route("gig.show",[$order->gig->id]) }}">{{ $order->gig->title }}</a>
                                                    @endAdmin
                                                    <p class="text-muted text-14">
                                                        @php
                                                            $price = 0
                                                        @endphp
                                                        @if($order->payment->plan_type == "basic" )
                                                            {{ $order->payment->basicPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->basicPlan->duration_type;
                                                                $duration =  $order->payment->basicPlan->duration;
                                                            @endphp
                                                        @endif
                                                        @if($order->payment->plan_type == "standard" )
                                                            {{ $order->payment->standardPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->standardPlan->duration_type;
                                                                $duration =  $order->payment->standardPlan->duration;
                                                            @endphp
                                                        @endif
                                                        @if($order->payment->plan_type == "premium" )
                                                            {{ $order->payment->premiumPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->premiumPlan->duration_type;
                                                                $duration =  $order->payment->premiumPlan->duration;
                                                            @endphp

                                                        @endif
                                                        &nbsp;
                                                    </p>
                                                </td>
                                                <td class="text-14">
                                                    @php
                                                        $current = Carbon\Carbon::now();
                                                        $dt = new Carbon\Carbon($order->payment->created_at);
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14">
                                                    @php
                                                        if($duration_type == "hour(s)"){
                                                            $dt->addHours($duration);
                                                            $diff = $dt->diffInMinutes($current);
                                                        }
                                                        if($duration_type == "week(s)"){
                                                            $dt->addWeeks($duration);
                                                            $diff = $dt->diffInDays($current);
                                                        }
                                                        if($duration_type == "day(s)"){
                                                            $dt->addDays($duration);
                                                            $diff = $dt->diffInHours($current);
                                                        }
                                                        if($duration_type == "month(s)"){
                                                            $dt->addDays($duration);
                                                            $diff = $dt->diffInWeeks($current);
                                                        }
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14 font-weight-bold">
                                                    <span class="badge badge-primary price-badge-pink-light mr-3 text-16">
                                                            {{-- {{ $setting->currency }} {{ $price }} --}}
                                                    </span>
                                                </td>
                                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-thick mr-3 bg-primary-light text-white">{{ $order->status }}</span></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane pt-5" role="tabpanel" id="tab-2">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="border-top-0">
                                        <tr>
                                            <th class="border-top-0">Buyer</th>
                                            @admin
                                                <th class="border-top-0">Seller</th>
                                            @endAdmin
                                            <th class="border-top-0">Gig</th>
                                            <th class="border-top-0">Started On</th>
                                            <th class="border-top-0">Due On</th>
                                            <th class="border-top-0">Price</th>
                                            <th class="border-top-0">Progress</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($orders->where("status","Delivered") as $order)
                                            <tr>
                                                <td class="text-14">{{ $order->user->name }}</td>
                                                @admin
                                                    <td class="text-14">{{ $order->gig->user->name }}</td>
                                                @endAdmin
                                                <td class="text-14">
                                                    @seller
                                                        <a href="{{ route("order.show",[$order->id]) }}">{{ $order->gig->title }}</a>
                                                    @endSeller
                                                    @admin
                                                        <a href="{{ route("gig.show",[$order->gig->id]) }}">{{ $order->gig->title }}</a>
                                                    @endAdmin
                                                    <p class="text-muted text-14">
                                                        @php
                                                            $price = 0
                                                        @endphp
                                                        @if($order->payment->plan_type == "basic" )
                                                            {{ $order->payment->basicPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->basicPlan->duration_type;
                                                                $duration =  $order->payment->basicPlan->duration;
                                                            @endphp
                                                        @endif
                                                        @if($order->payment->plan_type == "standard" )
                                                            {{ $order->payment->standardPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->standardPlan->duration_type;
                                                                $duration =  $order->payment->standardPlan->duration;
                                                            @endphp
                                                        @endif
                                                        @if($order->payment->plan_type == "premium" )
                                                            {{ $order->payment->premiumPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->premiumPlan->duration_type;
                                                                $duration =  $order->payment->premiumPlan->duration;
                                                            @endphp

                                                        @endif
                                                        &nbsp;
                                                    </p>
                                                </td>
                                                <td class="text-14">
                                                    @php
                                                        $current = Carbon\Carbon::now();
                                                        $dt = new Carbon\Carbon($order->payment->created_at);
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14">
                                                    @php
                                                        if($duration_type == "hour(s)"){
                                                            $dt->addHours($duration);
                                                            $diff = $dt->diffInMinutes($current);
                                                        }
                                                        if($duration_type == "week(s)"){
                                                            $dt->addWeeks($duration);
                                                            $diff = $dt->diffInDays($current);
                                                        }
                                                        if($duration_type == "day(s)"){
                                                            $dt->addDays($duration);
                                                            $diff = $dt->diffInHours($current);
                                                        }
                                                        if($duration_type == "month(s)"){
                                                            $dt->addDays($duration);
                                                            $diff = $dt->diffInWeeks($current);
                                                        }
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14 font-weight-bold">
                                                    <span class="badge badge-primary price-badge-pink-light mr-3 text-16">
                                                            {{ $setting->currency }} {{ $price }}
                                                    </span>
                                                </td>
                                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-thick mr-3 bg-info-light text-white">{{ $order->status }}</span></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane pt-5" role="tabpanel" id="tab-3">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="border-top-0">
                                        <tr>
                                            <th class="border-top-0">Buyer</th>
                                            @admin
                                                <th class="border-top-0">Seller</th>
                                            @endAdmin
                                            <th class="border-top-0">Gig</th>
                                            <th class="border-top-0">Started On</th>
                                            <th class="border-top-0">Due On</th>
                                            <th class="border-top-0">Price</th>
                                            <th class="border-top-0">Progress</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($orders->where("status","Completed") as $order)
                                            <tr>
                                                <td class="text-14">{{ $order->user->name }}</td>
                                                @admin
                                                    <td class="text-14">{{ $order->gig->user->name }}</td>
                                                @endAdmin
                                                <td class="text-14">
                                                    @seller
                                                        <a href="{{ route("order.show",[$order->id]) }}">{{ $order->gig->title }}</a>
                                                    @endSeller
                                                    @admin
                                                        <a href="{{ route("gig.show",[$order->gig->id]) }}">{{ $order->gig->title }}</a>
                                                    @endAdmin
                                                    <p class="text-muted text-14">
                                                        @php
                                                            $price = 0
                                                        @endphp
                                                        @if($order->payment->plan_type == "basic" )
                                                            {{ $order->payment->basicPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->basicPlan->duration_type;
                                                                $duration =  $order->payment->basicPlan->duration;
                                                            @endphp
                                                        @endif
                                                        @if($order->payment->plan_type == "standard" )
                                                            {{ $order->payment->standardPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->standardPlan->duration_type;
                                                                $duration =  $order->payment->standardPlan->duration;
                                                            @endphp
                                                        @endif
                                                        @if($order->payment->plan_type == "premium" )
                                                            {{ $order->payment->premiumPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->premiumPlan->duration_type;
                                                                $duration =  $order->payment->premiumPlan->duration;
                                                            @endphp

                                                        @endif
                                                        &nbsp;
                                                    </p>
                                                </td>
                                                <td class="text-14">
                                                    @php
                                                        $current = Carbon\Carbon::now();
                                                        $dt = new Carbon\Carbon($order->payment->created_at);
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14">
                                                    @php
                                                        if($duration_type == "hour(s)"){
                                                            $dt->addHours($duration);
                                                            $diff = $dt->diffInMinutes($current);
                                                        }
                                                        if($duration_type == "week(s)"){
                                                            $dt->addWeeks($duration);
                                                            $diff = $dt->diffInDays($current);
                                                        }
                                                        if($duration_type == "day(s)"){
                                                            $dt->addDays($duration);
                                                            $diff = $dt->diffInHours($current);
                                                        }
                                                        if($duration_type == "month(s)"){
                                                            $dt->addDays($duration);
                                                            $diff = $dt->diffInWeeks($current);
                                                        }
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14 font-weight-bold">
                                                    <span class="badge badge-primary price-badge-pink-light mr-3 text-16">
                                                       {{ $setting->currency }} {{ $price }}
                                                    </span>
                                                </td>
                                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-thick mr-3 bg-success-light text-white">{{ $order->status }}</span></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
