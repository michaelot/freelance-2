@extends('layouts.custom')
@section('appjs')
    <meta name="page" content="order">
    <script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Sale<br></h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Dashboard</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"orders"])
@php
    $delivered = false;
    $reviewed = false;

    if($order->delivery != null){
        $delivered = true;
    }
    if($order->review != null){
        $reviewed = true;
    }
@endphp
<section id="statement" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row">
            <div class="col align-self-center col-12   col-lg-10">
                <h2 class="mb-3">Sale of:&nbsp;<a class="text-muted" href="#">{{ $order->gig->title }}<br></a></h2>
                <div class="d-flex align-items-center mb-2">
                    <div class="d-flex align-items-center d-inline-block mb-2 user-avatar mr-3">
                        <div class="float-left">
                            @orderBuyer
                                <img src="{{ asset("storage/profile images/".$order->gig->user->seller->profile_image) }}" class="rounded-circle" width="30" height="30">
                            @endOrderBuyer
                        </div>
                        <div class="float-left pl-2">
                            @orderBuyer
                                <p class="text-16">{{ $order->gig->user->name }}&nbsp;</p>
                            @endOrderBuyer
                            @orderSeller
                                <p class="text-16">{{ $order->user->name }}&nbsp;</p>
                            @endOrderSeller
                        </div>
                    </div>
                    <div class="mb-2">
                        <p class="text-info text-16"><i class="icon-layers"></i>&nbsp;{{ $order->gig->gig_sub_category_id }}</p>
                    </div>
                </div>
                <p class="text-14">{{ $order->brief }}</p>
            </div>
            <div class="col  col-12 col-lg-2 pt-3">
                @if($order->status == "In Progress")
                    <span class="badge badge-primary price-badge-thick mr-3 bg-primary-light text-white">In Progress</span>
                @endif

                @if($order->status == "Completed")
                    <span class="badge badge-primary price-badge-thick mr-3 bg-success-light text-white">Completed</span>
                @endif

                @if($order->status == "Delivered")
                    <span class="badge badge-primary price-badge-thick mr-3 bg-info-light text-white">Delivered</span>
                @endif

                @if($order->status == "Canceled")
                    <span class="badge badge-primary price-badge-thick mr-3 text-white bg-danger-light">Canceled</span>
                @endif
            </div>
        </div>
        <div class="row bg-white rounded pt-5 pb-4 mb-5">
            <div class="col">
                <div class="table-responsive" id="order">
                    <table class="table">
                        <thead class="border-top-0">
                            <tr>
                                <th class="border-top-0">Plan</th>
                                <th class="border-top-0">Started On</th>
                                <th class="border-top-0">Due On</th>
                                <th class="border-top-0">Price</th>
                                <th class="border-top-0">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-14">{{ $order->gig->title }}
                                    @if($order->payment->plan_type == "basic" )
                                        <p class="text-muted text-14 mb-3">{{ $order->payment->basicPlan->plan_title }}&nbsp;</p>
                                        <ul class="list-unstyled">
                                            @orderBuyer
                                                @foreach ($order->payment->basicPlan->extras as $extra)
                                                    <li>
                                                        <i class="far fa-check-circle text-success"></i>&nbsp;
                                                        {{ $extra->name }}
                                                    </li>
                                                @endforeach
                                            @endOrderBuyer
                                        </ul>
                                        @php
                                            $price = $order->payment->basicPlan->price;
                                            $duration_type =  $order->payment->basicPlan->duration_type;
                                            $duration =  $order->payment->basicPlan->duration;
                                        @endphp
                                    @endif
                                    @if($order->payment->plan_type == "standard" )
                                        <p class="text-muted text-14 mb-3">{{ $order->payment->standardPlan->plan_title }}&nbsp;</p>
                                        <ul class="list-unstyled">
                                            @orderBuyer
                                                @foreach ($order->payment->standardPlan->extras as $extra)
                                                    <li>
                                                        <i class="far fa-check-circle text-success"></i>&nbsp;
                                                        {{ $extra->name }}
                                                    </li>
                                                @endforeach
                                            @endOrderBuyer
                                        </ul>
                                        @php
                                            $price = $order->payment->basicPlan->price;
                                            $duration_type =  $order->payment->standardPlan->duration_type;
                                            $duration =  $order->payment->standardPlan->duration;
                                        @endphp
                                    @endif
                                    @if($order->payment->plan_type == "premium" )
                                        <p class="text-muted text-14 mb-3">{{ $order->payment->premiumPlan->plan_title }}&nbsp;</p>
                                        <ul class="list-unstyled">
                                            @orderBuyer
                                                @foreach ($order->payment->premiumPlan->extras as $extra)
                                                    <li>
                                                        <i class="far fa-check-circle text-success"></i>&nbsp;
                                                        {{ $extra->name }}
                                                    </li>
                                                @endforeach
                                            @endOrderBuyer
                                        </ul>
                                        @php
                                            $price = $order->payment->basicPlan->price;
                                            $duration_type =  $order->payment->premiumPlan->duration_type;
                                            $duration =  $order->payment->premiumPlan->duration;
                                        @endphp
                                    @endif
                                </td>
                                <td class="text-14">
                                    @php
                                        $current = Carbon\Carbon::now();
                                        $dt = new Carbon\Carbon($order->payment->created_at);
                                        // $dt->addDays(3);
                                        echo $dt->toFormattedDateString();
                                    @endphp
                                </td>
                                <td class="text-14">
                                    {{-- {{ $duration."->" .$duration_type." " }} --}}
                                    @php
                                        // $dt = new Carbon\Carbon($order->payment->created_at);
                                        if($duration_type == "hour(s)"){
                                            $dt->addHours($duration);
                                            $diff = $dt->diffInMinutes($current);
                                        }
                                        if($duration_type == "week(s)"){
                                            $dt->addWeeks($duration);
                                            $diff = $dt->diffInDays($current);
                                        }
                                        if($duration_type == "day(s)"){
                                            $dt->addDays($duration);
                                            $diff = $dt->diffInDays($current);
                                        }
                                        if($duration_type == "month(s)"){
                                            $dt->addDays($duration);
                                            $diff = $dt->diffInWeeks($current);
                                        }
                                        echo $dt->toFormattedDateString();
                                    @endphp
                                </td>
                                <td class="text-14 font-weight-bold">
                                    <span class="badge badge-primary price-badge-pink-light mr-3 text-16">
                                            {{ $currency }} {{ $order->payment->amount }}
                                    </span>
                                </td>
                                <td class="text-14 font-weight-bold">
                                        @if($order->status == "In Progress")
                                            <span class="badge badge-primary price-badge-thick mr-3 bg-primary-light text-white">In Progress</span>
                                        @endif

                                        @if($order->status == "Completed")
                                            <span class="badge badge-primary price-badge-thick mr-3 bg-success-light text-white">Completed</span>
                                        @endif

                                        @if($order->status == "Delivered")
                                            <span class="badge badge-primary price-badge-thick mr-3 bg-info-light text-white">Delivered</span>
                                        @endif

                                        @if($order->status == "Canceled")
                                            <span class="badge badge-primary price-badge-thick mr-3 text-white bg-danger-light">Canceled</span>
                                        @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div class="row bg-white rounded pt-5 pb-4">
            <div class="col col-lg-12 text-center"><img class="img-fluid" src="{{ asset("img/start-up.svg") }}" width="70">
                <h3 class="mt-4 main-color mb-2">Order Started</h3>
                <p>Order Countdown is now tickling</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque&nbsp;</p>
                {{-- <h1 class="display-2  mt-4 main-color">00:40:00:11</h1> --}}
                <div class="row mt-4">
                    <div class="col pulse animated infinite col-lg-8 offset-lg-2">
                        <div class=" bg-color pl-2 pl-md-3  pr-2 pr-md-3 pt-3 pb-3 rounded-lg ">
                            @if ($order->status == "Completed")
                                <h1 class="display-4 text-success">Completed</h1>

                            @elseif ($order->status == "Delivered")
                                <h1 class="display-4 text-info">Delivered</h1>
                            @else
                                <h1 id="timer" class="display-4   main-color">
                                    <span id="timer-days" class="d-inline-flex flex-column"></span>:
                                    <span id="timer-hours" class="d-inline-flex flex-column"></span>:
                                    <span id="timer-mins" class="d-inline-flex flex-column"></span>:
                                    <span id="timer-secs" class="d-inline-flex flex-column"></span>
                                </h1>
                            @endif

                        </div>
                    </div>
                </div>
                <p class="text-20"></p>
            </div>
        </div>
        <div class="row bg-white rounded pt-5 pb-4 mt-5">
            <div class="col col-lg-12 text-center"><img class="img-fluid" src="{{ asset("img/shopping-list.svg") }}" width="70">
                <h3 class="mt-4 main-color mb-2">Order Requirements Submitted</h3>
                <p class="mb-4">Your buyer has filled his requirements</p>
                {{-- {{ $order->requirements }} --}}
                @orderSeller
                    @foreach ($order->requirements as $item)
                        {{-- {{ $item->requirement->id }} --}}
                        <div class="form-row text-left mb-3">
                            <div class="col col-lg-8 offset-lg-2">
                                <label class="font-weight-normal mb-2">{{ $item->requirement->title }}&nbsp;</label>
                                @if($item->requirement->answer_type == "Text")
                                    <p class="ml-3 text-muted">{{ $item->answer_text ? $item->answer_text : "Not Specified"  }}</p>
                                @endif
                                @if($item->requirement->answer_type == "File")
                                    <ul class="ml-2" class="list-unstyled">
                                        @foreach ($item->files as $file)
                                            <li>
                                                <i class="fas fa-download text-success"></i>&nbsp;
                                                <a href="{{ route("download",["requirement files",$file->path,$file->name])}}">{{ $file->name }}</a>
                                            </li>
                                        @endforeach
                                        @if (count($item->files) <= 0)
                                        <p class="text-muted">Not Specified</p>
                                        @endif
                                    </ul>
                                @endif
                            </div>
                        </div>
                    @endforeach
                @endOrderSeller

                @orderBuyer
                    <form class="text-left" enctype="multipart/form-data" method="POST" action="{{ route("order.requirement.update",[$order->id]) }}">
                        @csrf
                        @method("PUT")
                        @forelse ($order->requirements as $item)
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-8 offset-lg-2">
                                        <label class="font-weight-normal mb-2">{{ $item->requirement->title }}&nbsp;<span class="span-required">*</span></label>
                                        @if($item->requirement->answer_type == "Text")
                                            <textarea @if($order->status == "Completed") disabled @endif name="requirement[{{ $item->id }}][answer]" @if($item->requirement->compulsory == "Yes") required @endif class="form-control main-input-textarea" placeholder="Answer">{{ $item->answer_text ? $item->answer_text : "Not Specified"  }}</textarea>
                                        @endif
                                        @if($item->requirement->answer_type == "File")
                                            <ul class="ml-2" class="list-unstyled">
                                                @foreach ($item->files as $file)
                                                    <li><i class="fas fa-download text-success"></i>&nbsp; <a href="{{ route("download",["requirement files",$file->path,$file->name])}}">{{ $file->name }}</a><br></li>
                                                @endforeach
                                            </ul>
                                            <input @if($order->status == "Completed") disabled @endif name="file[{{ $item->id }}][]" @if($item->requirement->compulsory == "Yes") required @endif type="file" class="form-control border-0 bg-color" multiple>
                                        @endif
                                        <input type="hidden" value="{{ $item->requirement->answer_type }}" name="requirement[{{ $item->id }}][answer_type]">
                                        <input type="hidden" name="requirement[{{ $item->id }}][id]" value="{{ $item->id }}">
                                    </div>
                                </div>
                            </div>

                        @empty
                            <p id="no-requirement">No Requirement </p>
                        @endforelse
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-lg-8 offset-lg-2">
                                    <label class="font-weight-normal mb-2">Provide Additional Briefing&nbsp;<span class="span-required">*</span></label>
                                    <textarea @if($order->status == "Completed") disabled @endif name="brief" class="form-control main-input-textarea" placeholder="Additional Briefing">{{ $order->brief }}</textarea>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-6"><label class="font-weight-normal mb-2">Additional Video&nbsp;<span class="span-required">*</span></label><input type="file" class="form-control border-0 bg-color" multiple=""></div>
                            </div>
                        </div> --}}
                        <div class="row mb-5">
                            <div class="col col-lg-8 offset-lg-2 text-center">
                                <button @if($order->status == "Completed") disabled @endif type="submit" class="btn btn-primary text-white fr-cl-bcs btn-lg" >Submit</button>
                            </div>
                        </div>
                    </form>
                @endOrderBuyer
            </div>
        </div>
        <div id="app" class="row bg-white rounded pt-5 pb-4 mt-5">
            <div class="col col-lg-12 text-center"><img class="img-fluid" src="{{ asset('img/chat-comment-oval-speech-bubble-with-text-lines.svg') }}" width="70">
                <h3 class="mt-4 main-color mb-2">Comments</h3>
                <p class="mb-5">Type comments below</p>

                <input type="hidden" id="orderId" value="{{ $order->id }}">

                <list-comment-component v-bind:user="{{ auth()->user() }}" v-bind:comments="comments"></list-comment-component>
                <send-comment-component v-on:comment-Sent="addComment" v-bind:user="{{ auth()->user() }}" v-bind:type="'{{ auth()->user()->type }}'" v-bind:order="{{ $order->id }}"></send-comment-component>

            </div>
        </div>
        <div class="row bg-white rounded pt-5 pb-4 mt-5">
            <div class="col col-lg-12 text-center">
                <img class="img-fluid" src="{{ asset('img/open-box.svg') }}" width="70">

                @orderSeller
                    <h3 class="mt-4 main-color mb-2">Delivery Order</h3>
                    <p class="mb-3">Click the "Deliver Now" button to deliver order</p>
                    <a class="btn btn-primary text-white fr-cl-bcs bg-success border-0 font-weight-normal mb-5" role="button" id="delivery-btn" data-toggle="collapse" href="#delivery-form"> @if($delivered) Update @else Deliver @endif now</a>
                    <form enctype="multipart/form-data" method="POST" @if ($delivered) action="{{ route("order.delivery.update",[$order->delivery->id]) }}" @else action="{{ route("order.delivery.save",[$order->id]) }}" @endif  id="delivery-form" class="px-3 text-left mt-5 collapse">
                        @csrf
                        @if ($delivered)
                            @method("PUT")
                        @endif
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-8 offset-lg-2">
                                    <label class="font-weight-normal mb-2">Description&nbsp;<span class="span-required">*</span></label>
                                    <textarea required name="description" class="form-control main-input-textarea" placeholder="Description">@if ($delivered) {{ $order->delivery->description }} @endif</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-8 offset-lg-2">
                                    <label class="font-weight-normal mb-2">Preview (.jpg, .png, .gif, .jpeg, .mp4)&nbsp;<span class="span-required">*</span></label>
                                    @if ($delivered)
                                        <img class="img-fluid" src="{{ asset("storage/delivery preview/".$order->delivery->preview_path) }}">
                                     @endif
                                    <input @if (!$delivered) required @endif name="preview" type="file" class="form-control border-0 bg-color" multiple="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-8 offset-lg-2">
                                    <label class="font-weight-normal mb-2">Files&nbsp;<span class="span-required">*</span></label>
                                    @if($delivered)
                                        <ul class="ml-2" class="list-unstyled">
                                            @foreach ($order->delivery->files as $file)
                                                <li><i class="fas fa-download text-success"></i>&nbsp;
                                                    <a href="{{ route("download",["delivery files",$file->path,$file->name])}}">{{ $file->name }}</a><br></li>
                                            @endforeach
                                        </ul>
                                    @endif
                                    <input name="files[]" type="file" class="form-control border-0 bg-color" multiple="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center mt-4">
                            <button class="btn btn-primary text-white fr-cl-bcs bg-info-light border-0 font-weight-normal" type="submit">Upload work</button>
                        </div>
                    </form>
                    @if ($reviewed)
                        <div class="col col-12">
                            <div class="mt-5 text-left">
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-12 col-lg-6">
                                            <label class="font-weight-normal mb-2 mt-2">Rate Communication&nbsp;<span class="span-required">*</span></label>
                                            <select disabled  name="communication" class="form-control disabled main-input-form">
                                                <option @if($reviewed) @if($order->review->communication == "1.0") selected @endif @endif value="1.0">1.0</option>
                                                <option @if($reviewed) @if($order->review->communication == "2.0") selected @endif @endif value="2.0">2.0</option>
                                                <option @if($reviewed) @if($order->review->communication == "3.0") selected @endif @endif value="3.0">3.0</option>
                                                <option @if($reviewed) @if($order->review->communication == "4.0") selected @endif @endif value="4.0">4.0</option>
                                                <option @if($reviewed) @if($order->review->communication == "5.0") selected @endif @endif value="5.0">5.0</option>
                                            </select>
                                        </div>
                                        <div class="col col-12 col-lg-6">
                                            <label class="font-weight-normal mb-2 mt-2">Rate Service&nbsp;<span class="span-required">*</span></label>
                                            <select name="service" disabled class="form-control main-input-form">
                                                    <option @if($reviewed) @if($order->review->service == "1.0") selected @endif @endif value="1.0">1.0</option>
                                                    <option @if($reviewed) @if($order->review->service == "2.0") selected @endif @endif value="2.0">2.0</option>
                                                    <option @if($reviewed) @if($order->review->service == "3.0") selected @endif @endif value="3.0">3.0</option>
                                                    <option @if($reviewed) @if($order->review->service == "4.0") selected @endif @endif value="4.0">4.0</option>
                                                    <option @if($reviewed) @if($order->review->service == "5.0") selected @endif @endif value="5.0">5.0</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-12">
                                            <label class="font-weight-normal mb-2">Comment&nbsp;<span class="span-required">*</span></label>
                                            <textarea disabled name="description" class="form-control main-input-textarea" placeholder="Description">@if($reviewed) {{ $order->review->description }} @endif</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                @endOrderSeller

                @orderBuyer
                    <h3 class="mt-4 main-color mb-2">Here's your Delivery</h3>

                    @if (!$delivered)
                        <p>Order is being worked on</p>
                    @else
                        <p class="mb-3">Click the accept button to mark order as complete</p>
                        <div class="row">
                            <div class="col col-lg-10 offset-lg-1">
                                <div class="media text-left">
                                    <img class="img-fluid mr-3" src="{{ asset("storage/profile images/".$order->gig->user->seller->profile_image) }}" width="50">
                                    <div class="media-body">
                                        <h5>{{ $order->gig->user->name }}&nbsp;</h5>
                                        <p> {{ $order->delivery->description }} </p>
                                        <div class="row mt-5">
                                            <div class="col col-12 col-lg-7 text-left">
                                                <p class="text-muted mb-3">Preview</p>
                                                <img class="img-fluid" src="{{ asset("storage/delivery preview/".$order->delivery->preview_path) }}">
                                            </div>
                                            <div class="col col-12 col-lg-5 text-left">
                                                <p class="text-muted mb-3 mt-3">Delivered Files</p>
                                                <ul class="ml-2" class="list-unstyled">
                                                    @foreach ($order->delivery->files as $file)
                                                        <li><i class="fas fa-download text-success"></i>&nbsp; <a href="{{ route("download",["delivery files",$file->path,$file->name])}}">{{ $file->name }}</a><br></li>
                                                    @endforeach
                                                </ul>
                                            </div>

                                            @if ($order->status == "Delivered" or $order->status == "Completed")
                                                <div class="col col-12">
                                                    <form method="POST" action="@if($reviewed) {{ route("review.update",[$order->gig->user->seller->id,$order->id]) }}  @else {{ route("review.save",[$order->gig->user->seller->id,$order->id,$price]) }} @endif" class="mt-5">
                                                        @csrf
                                                        @if ($reviewed)
                                                            @method("put")
                                                        @endif
                                                        <div class="form-group">
                                                            <div class="form-row">
                                                                <div class="col col-12 col-md-6">
                                                                    <label class="font-weight-bold mb-2 mt-2">Rate Communication&nbsp;<span class="span-required">*</span></label>
                                                                    <select required @if($order->status == "Completed") disabled @endif name="communication" class="form-control main-input-form">
                                                                        <option  @if($reviewed) @if($order->review->communication == "1.0") selected @endif @endif value="1.0">1.0</option>
                                                                        <option  @if($reviewed) @if($order->review->communication == "2.0") selected @endif @endif value="2.0">2.0</option>
                                                                        <option  @if($reviewed) @if($order->review->communication == "3.0") selected @endif @endif value="3.0">3.0</option>
                                                                        <option  @if($reviewed) @if($order->review->communication == "4.0") selected @endif @endif value="4.0">4.0</option>
                                                                        <option  @if($reviewed) @if($order->review->communication == "5.0") selected @endif @endif value="5.0">5.0</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col col-12 col-md-6">
                                                                    <label class="font-weight-bold mb-2 mt-2">Rate Service&nbsp;<span class="span-required">*</span></label>
                                                                    <select required @if($order->status == "Completed") disabled @endif name="service" class="form-control main-input-form">
                                                                        <option  @if($reviewed) @if($order->review->service == "1.0") selected @endif @endif value="1.0">1.0</option>
                                                                        <option  @if($reviewed) @if($order->review->service == "2.0") selected @endif @endif value="2.0">2.0</option>
                                                                        <option  @if($reviewed) @if($order->review->service == "3.0") selected @endif @endif value="3.0">3.0</option>
                                                                        <option  @if($reviewed) @if($order->review->service == "4.0") selected @endif @endif value="4.0">4.0</option>
                                                                        <option  @if($reviewed) @if($order->review->service == "5.0") selected @endif @endif value="5.0">5.0</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-row">
                                                                <div class="col col-12">
                                                                    <label class="font-weight-normal mb-2">What was it like working with this seller? &nbsp;<span class="span-required">*</span></label>
                                                                    <textarea @if($order->status == "Completed") disabled @endif required name="description" class="form-control main-input-textarea" placeholder="Description">@if($reviewed) {{ $order->review->description }} @endif</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if($order->status == "Delivered")
                                                            <p class="text-muted text-14 mb-2 text-warning">Only Accept Order if you're satisfied</p>
                                                            <button class="btn btn-primary text-white fr-cl-bcs bg-success border-0 font-weight-normal mb-5" type="submit" >Accept Order</button>
                                                        @endif
                                                    </form>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endOrderBuyer
            </div>
        </div>
    </div>
</section>
@if (!($order->status == "Completed" or $order->status == "Delivered"))
<script>
    var end = @php  echo "'".$dt->toDateString()."T".$dt->toTimeString()."'" @endphp;
    var endDate = new Date(end).getTime();
    // alert(end)
    var timer = setInterval(function() {

        let now = new Date().getTime();
        let t = endDate - now;
        // alert(endDate)
        if (t >= 0) {

            let days = Math.floor(t / (1000 * 60 * 60 * 24));
            let hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let mins = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
            let secs = Math.floor((t % (1000 * 60)) / 1000);

            document.getElementById("timer-days").innerHTML =days +
            "<p class='small' style='font-size:20px'>DAY(S)</p>";

            document.getElementById("timer-hours").innerHTML = ("0"+hours).slice(-2) +
            "<p class='label' class='small' style='font-size:20px'>HR(S)</p>";

            document.getElementById("timer-mins").innerHTML = ("0"+mins).slice(-2) +
            "<p class='label' class='small' style='font-size:20px'>MIN(S)</p>";

            document.getElementById("timer-secs").innerHTML = ("0"+secs).slice(-2) +
            "<p class='label' class='small' style='font-size:20px'>SEC(S)</p>";

        } else {

            document.getElementById("timer").innerHTML = "Duration Expired!";
            $("#timer").css({
                color:"red"
            })
        }

    }, 1000);
</script>
@endif

@endsection
