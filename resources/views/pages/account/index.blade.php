@extends('layouts.custom')
@section('content')
<div class="carousel slide" data-ride="carousel" id="auth-index-carousel">
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <div class="d-flex align-items-center carousel-container" id="carousel-container-1">
                <div>
                    <h1 class="text-white">Tell Your Brand's Story</h1>
                    <p class="text-white font-weight-bold">Made of Exceptional Talent &amp; Service</p>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="d-flex align-items-center carousel-container" id="carousel-container-1">
                <div>
                    <h1 class="text-white">Tell Your Brand's Story</h1>
                    <p class="text-white font-weight-bold">Made of Exceptional Talent &amp; Service</p>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="d-flex align-items-center carousel-container" id="carousel-container-2">
                <div>
                    <h1 class="text-white">Bringing Out The Best In You!</h1>
                    <p class="text-white font-weight-bold">Try creating gig</p>
                </div>
            </div>
        </div>
    </div>
    <div class="d-none"><a class="carousel-control-prev" href="#auth-index-carousel" role="button" data-slide="prev"><span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#auth-index-carousel"
            role="button" data-slide="next"><span class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a></div>
    <ol class="carousel-indicators">
        <li data-target="#auth-index-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#auth-index-carousel" data-slide-to="1"></li>
        <li data-target="#auth-index-carousel" data-slide-to="2"></li>
    </ol>
</div>
<section id="auth-index-intro" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-10 offset-lg-1 text-center">
                <h2 class="text-center mb-2">Hi {{ auth()->user()->name }}!</h2>
                <p class="text-center"><strong>Join Our Growing Freelance Community</strong></p>
                <button class="btn btn-primary fr-cl-bcs font-weight-bold mt-4" type="button">What's Your Skill?</button>
            </div>
        </div>
    </div>
</section>
<section id="explore-the-market-place" class="section-padding pt-0">
    <div class="container">
        <div class="row">
            @foreach ($categories as $category)
                <div class="col col-lg-3 mt-5" data-bs-hover-animate="pulse">
                    <div class="row">
                        <div class="col text-center">
                            <img class="img-fluid" src="{{ asset("storage/category icon/".$category->icon) }}" width="80">
                        </div>
                    </div>
                    <h5 class="text-center mt-4 font-weight-normal">{{ $category->name }}</h5>
                </div>
            @endforeach
        </div>
    </div>
</section>
<section id="home-newest-release-products" class="section-padding bg-color product-container-section">
    <div class="container">
        <div class="row section-header-row pt-2 pb-2">
            <div class="col col-md-9 col-sm-8 col-6">
                <div>
                    <h4 class="mb-0">Newest Release Products</h4>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($newGigs as $gig)
                @include('layouts.gig',['gig' => $gig])
            @endforeach
        </div>
    </div>
</section>
<section id="home-newest-release-products" class="section-padding bg-color product-container-section">
    <div class="container">
        <div class="row section-header-row pt-2 pb-2">
            <div class="col col-md-9 col-sm-8 col-6">
                <div>
                    <h4 class="mb-0 font-weight-bold"><strong>Top Picks For You In Logo Design</strong></h4>
                </div>
            </div>
        </div>
        <div class="row">
                @foreach ($random1 as $gig)
                    @include('layouts.gig',['gig' => $gig])
                @endforeach
        </div>
    </div>
</section>
<section id="home-newest-release-products" class="section-padding bg-color product-container-section">
    <div class="container">
        <div class="row section-header-row pt-2 pb-2">
            <div class="col col-md-9 col-sm-8 col-6">
                <div>
                    <h4 class="mb-0 font-weight-bold"><strong>Similar To Services You've Viewed</strong><br></h4>
                </div>
            </div>
        </div>
        <div class="row">
                @foreach ($random2 as $gig)
                    @include('layouts.gig',['gig' => $gig])
                @endforeach
        </div>
    </div>
</section>
@endsection
