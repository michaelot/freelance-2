@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="main-become-a-seller-jumb">
    <h1 class="display-4 text-white mb-3 font-weight-normal">&nbsp;Let's Talk About Our Dont's</h1>
    <h3 class="text-white font-weight-light mb-5">Your Success Is Important To Us, Avoid The Following To Remain In Our Community</h3>
    <p></p>
</div>
<section class="section-padding">
    <div class="container">
        <div class="row mb-5">
            <div class="col col-lg-5 text-center"><img src="{{ asset('img/information%20(1).png') }}" width="120">
                <h3 class="font-weight-normal mt-4 mb-4 main-color">Providing False Identity</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam c&nbsp;</p>
            </div>
            <div class="col col-lg-5 text-center offset-lg-2"><img src="{{ asset('img/user.png') }}" width="120">
                <h3 class="font-weight-normal mt-4 mb-4 main-color">Opening Duplicate Account</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam c&nbsp;</p>
            </div>
        </div>
        <div class="row">
            <div class="col col-lg-5 text-center"><img src="{{ asset('img/portfolio.png') }}" width="120">
                <h3 class="font-weight-normal mt-4 mb-4 main-color">Soliciting Other Community Members For Work</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam c&nbsp;</p>
            </div>
            <div class="col col-lg-5 text-center offset-lg-2"><img src="{{ asset('img/guard.png') }}" width="120">
                <h3 class="font-weight-normal mt-4 mb-4 main-color">Requesting To Take Communication And Payment Outside</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam c&nbsp;</p>
            </div>
        </div>
        <div class="row pt-5 mb-5">
            <div class="col text-center"><a href="{{ route("account.three") }}" class="btn btn-primary text-white fr-cl-bcs btn-lg" role="button">Continue</a></div>
        </div>
    </div>
</section>
@endsection
