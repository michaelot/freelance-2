@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="main-become-a-seller-jumb">
    <h1 class="display-4 text-white mb-3 font-weight-normal">What Makes A Successful Seller</h1>
    <h3 class="text-white font-weight-light mb-5">Your First Impression Matters, Create A Profile That Will Stand Out</h3>
    <p></p>
</div>
<section id="become-a-seller-section-one" class="section-padding">
    <div class="container">
        <div class="row mb-5">
            <div class="col d-flex justify-content-center align-items-center col-lg-5 text-center">
                <div><img class="img-fluid" src="{{ asset('img/number.png') }}" width="200"></div>
            </div>
            <div class="col col-lg-6"><img src="{{ asset('img/resume.png') }}" width="90">
                <h3 class="font-weight-normal mt-3 mb-4">Take Your Time To Create Your Profile</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque
                    the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;</p>
            </div>
        </div>
        <div class="row pt-5 mb-5">
            <div class="col d-flex justify-content-center align-items-center col-lg-5 text-center">
                <div><img class="img-fluid" src="{{ asset('img/number%20(1).png') }}" width="200"></div>
            </div>
            <div class="col col-lg-6"><img src="{{ asset('img/link.png') }}" width="90">
                <h3 class="font-weight-normal mt-3 mb-4">Link Out To Your Professional Networks&nbsp;</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque
                    the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;</p>
            </div>
        </div>
        <div class="row pt-5 mb-5">
            <div class="col d-flex justify-content-center align-items-center col-lg-5 text-center">
                <div><img class="img-fluid" src="{{ asset('img/number%20(2).png') }}" width="200"></div>
            </div>
            <div class="col col-lg-6"><img src="{{ asset('img/notepad.png') }}" width="90">
                <h3 class="font-weight-normal mt-3 mb-4">Accurately Describe Your Professional Skills&nbsp;</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque
                    the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;</p>
            </div>
        </div>
        <div class="row pt-5 mb-5">
            <div class="col d-flex justify-content-center align-items-center col-lg-5 text-center">
                <div><img class="img-fluid" src="{{ asset('img/number%20(3).png') }}" width="200"></div>
            </div>
            <div class="col col-lg-6"><img src="{{ asset('img/add-user.png') }}" width="90">
                <h3 class="font-weight-normal mt-3 mb-4">Upload A Profile Picture That Clearly Shows Your Face&nbsp;</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque
                    the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;</p>
            </div>
        </div>
        <div class="row pt-5 mb-5">
            <div class="col d-flex justify-content-center align-items-center col-lg-5 text-center">
                <div><img class="img-fluid" src="{{ asset('img/number%20(4).png') }}" width="200"></div>
            </div>
            <div class="col col-lg-6"><img src="{{ asset('img/locked.png') }}" width="90">
                <h3 class="font-weight-normal mt-3 mb-4">Keep Our Market Save</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque
                    the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;</p>
            </div>
        </div>
        <div class="row pt-5 mb-5">
            <div class="col text-center">
                <a href="{{ route('account.create') }}" class="btn btn-primary text-white fr-cl-bcs btn-lg" role="button">Continue</a>
            </div>
        </div>
    </div>
</section>
@endsection
