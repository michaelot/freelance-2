@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Manage Gigs<br></h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Dashboard</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"manageGig"])
<section id="statement" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row">
            <div class="col align-self-center col-lg-8">
                <h2>Account Statement</h2>
            </div>
            <div class="col col-lg-4 text-right">
                <form>
                    <div class="form-group">
                        <div class="input-group"><input class="form-control main-input-form" type="text">
                            <div class="input-group-append"><button class="btn btn-primary fr-cl-bcs mr-0" type="button">Search</button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col col-lg-3">
                <div class="shadow d-flex align-items-center flex-wrap bg-success-light pt-4 pb-4 rounded-lg pl-3">
                    <div class="d-flex justify-content-center align-items-center account-statement-icon bg-white mr-4 mb-2">
                        <p class="text-center text-success-light"><i class="icon-tag"></i></p>
                    </div>
                    <div class="mb-2">
                        <h2 class="mb-3 text-white">{{ count($incomes) }}</h2>
                        <p class="text-white">Total Sales</p>
                    </div>
                </div>
            </div>
            <div class="col col-lg-3">
                <div class="shadow d-flex align-items-center flex-wrap bg-warning-light pt-4 pb-4 rounded-lg pl-3">
                    <div class="d-flex justify-content-center align-items-center account-statement-icon bg-white mr-4 mb-2">
                        <p class="text-center text-warning-light"><i class="icon-basket"></i></p>
                    </div>
                    <div class="mb-2">
                        <h2 class="mb-3 text-white">{{ $payments->sum('amount') }}</h2>
                        <p class="text-white">Total Purchases</p>
                    </div>
                </div>
            </div>
            <div class="col col-lg-3">
                <div class="shadow d-flex align-items-center flex-wrap bg-info-light pt-4 pb-4 rounded-lg pl-3">
                    <div class="d-flex justify-content-center align-items-center account-statement-icon bg-white mr-4 mb-2 text-info-light">
                        <p class="text-info-light"><i class="icon-film"></i></p>
                    </div>
                    <div class="mb-2">
                        <h2 class="mb-3 text-white">{{ $incomes->sum('amount') }}</h2>
                        <p class="text-white">Total Credited</p>
                    </div>
                </div>
            </div>
            <div class="col col-lg-3">
                <div class="shadow d-flex align-items-center flex-wrap bg-danger-light pt-4 pb-4 rounded-lg pl-3">
                    <div class="d-flex justify-content-center align-items-center account-statement-icon bg-white mr-4 mb-2 text-info-light">
                        <p class="text-danger-light"><i class="icon-briefcase"></i></p>
                    </div>
                    <div class="mb-2">
                        <h2 class="mb-3 text-white">$630</h2>
                        <p class="text-white">Total Withdraw</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row bg-white rounded pt-4 pb-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="border-top-0">
                            <tr>
                                <th class="border-top-0">Date</th>
                                <th class="border-top-0">Order ID</th>
                                <th class="border-top-0">Authur</th>
                                <th class="border-top-0">Details</th>
                                <th>Type</th>
                                <th class="border-top-0">Price</th>
                                <th class="border-top-0">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-14">09 Jul 2017</td>
                                <td class="text-14">MP810094</td>
                                <td class="text-14 font-weight-bold">AazzTech</td>
                                <td class="text-14 font-weight-bold"><a href="#">Visibility Manager Subscriptions</a></td>
                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-light mr-3 bg-success-light text-white">Sale</span></td>
                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-pink-light mr-3 text-16">$90</span></td>
                                <td class="text-14 font-weight-bold"><a class="btn btn-primary fr-cl-bcs text-white font-weight-normal shadow-0" role="button">View</a></td>
                            </tr>
                            <tr>
                                <td class="text-14">09 Jul 2017</td>
                                <td class="text-14">MP810094</td>
                                <td class="text-14 font-weight-bold">AazzTech</td>
                                <td class="text-14 font-weight-bold"><a href="#">Visibility Manager Subscriptions</a></td>
                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-light mr-3 bg-warning-light text-white">Purchase</span></td>
                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-pink-light mr-3 text-16">$90</span></td>
                                <td class="text-14 font-weight-bold"><a class="btn btn-primary fr-cl-bcs text-white font-weight-normal shadow-0" role="button">View</a></td>
                            </tr>
                            <tr>
                                <td class="text-14">09 Jul 2017</td>
                                <td class="text-14">MP810094</td>
                                <td class="text-14 font-weight-bold"></td>
                                <td class="text-14 font-weight-bold"><a href="#">Visibility Manager Subscriptions</a></td>
                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-light mr-3 bg-danger-light text-white">Withdraw</span></td>
                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-pink-light mr-3 text-16">$90</span></td>
                                <td class="text-14 font-weight-bold"><a class="btn btn-primary fr-cl-bcs text-white font-weight-normal shadow-0" role="button">View</a></td>
                            </tr>
                            <tr>
                                <td class="text-14">09 Jul 2017</td>
                                <td class="text-14">MP810094</td>
                                <td class="text-14 font-weight-bold"></td>
                                <td class="text-14 font-weight-bold"><a href="#">Visibility Manager Subscriptions</a></td>
                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-light mr-3 bg-info-light text-white">Credit</span></td>
                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-pink-light mr-3 text-16">$90</span></td>
                                <td class="text-14 font-weight-bold"><a class="btn btn-primary fr-cl-bcs text-white font-weight-normal shadow-0" role="button">View</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div id="orders-tabs">
                    <ul class="nav nav-tabs" id="orders-tabs">
                        <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">Incomes</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">Purchase</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-3">Withdrawal</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pt-5" role="tabpanel" id="tab-1">
                            <div class="table-responsive">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead class="border-top-0">
                                                <tr>
                                                    <th class="border-top-0">Date</th>
                                                    <th class="border-top-0">Order ID</th>
                                                    <th class="border-top-0">Authur</th>
                                                    <th class="border-top-0">Details</th>
                                                    <th>Type</th>
                                                    <th class="border-top-0">Price</th>
                                                    <th class="border-top-0">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-14">09 Jul 2017</td>
                                                    <td class="text-14">MP810094</td>
                                                    <td class="text-14 font-weight-bold">AazzTech</td>
                                                    <td class="text-14 font-weight-bold"><a href="#">Visibility Manager Subscriptions</a></td>
                                                    <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-light mr-3 bg-success-light text-white">Sale</span></td>
                                                    <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-pink-light mr-3 text-16">$90</span></td>
                                                    <td class="text-14 font-weight-bold"><a class="btn btn-primary fr-cl-bcs text-white font-weight-normal shadow-0" role="button">View</a></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-14">09 Jul 2017</td>
                                                    <td class="text-14">MP810094</td>
                                                    <td class="text-14 font-weight-bold">AazzTech</td>
                                                    <td class="text-14 font-weight-bold"><a href="#">Visibility Manager Subscriptions</a></td>
                                                    <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-light mr-3 bg-warning-light text-white">Purchase</span></td>
                                                    <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-pink-light mr-3 text-16">$90</span></td>
                                                    <td class="text-14 font-weight-bold"><a class="btn btn-primary fr-cl-bcs text-white font-weight-normal shadow-0" role="button">View</a></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-14">09 Jul 2017</td>
                                                    <td class="text-14">MP810094</td>
                                                    <td class="text-14 font-weight-bold"></td>
                                                    <td class="text-14 font-weight-bold"><a href="#">Visibility Manager Subscriptions</a></td>
                                                    <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-light mr-3 bg-danger-light text-white">Withdraw</span></td>
                                                    <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-pink-light mr-3 text-16">$90</span></td>
                                                    <td class="text-14 font-weight-bold"><a class="btn btn-primary fr-cl-bcs text-white font-weight-normal shadow-0" role="button">View</a></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-14">09 Jul 2017</td>
                                                    <td class="text-14">MP810094</td>
                                                    <td class="text-14 font-weight-bold"></td>
                                                    <td class="text-14 font-weight-bold"><a href="#">Visibility Manager Subscriptions</a></td>
                                                    <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-light mr-3 bg-info-light text-white">Credit</span></td>
                                                    <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-pink-light mr-3 text-16">$90</span></td>
                                                    <td class="text-14 font-weight-bold"><a class="btn btn-primary fr-cl-bcs text-white font-weight-normal shadow-0" role="button">View</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <table class="table">
                                    <thead class="border-top-0">
                                        <tr>
                                            <th class="border-top-0">Buyer</th>
                                            <th class="border-top-0">Gig</th>
                                            <th class="border-top-0">Started On</th>
                                            <th class="border-top-0">Due On</th>
                                            <th class="border-top-0">Price</th>
                                            <th class="border-top-0">Progress</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($orders->where("status","In Progress") as $order)
                                            <tr>
                                                <td class="text-14">{{ $order->user->name }}</td>
                                                <td class="text-14"><a href="{{ route("order.show",[$order->id]) }}">{{ $order->gig->title }}</a>
                                                    <p class="text-muted text-14">
                                                        @php
                                                            $price = 0
                                                        @endphp
                                                        @if($order->payment->plan_type == "basic" )
                                                            {{ $order->payment->basicPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->basicPlan->duration_type;
                                                                $duration =  $order->payment->basicPlan->duration;
                                                            @endphp
                                                        @endif
                                                        @if($order->payment->plan_type == "standard" )
                                                            {{ $order->payment->standardPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->standardPlan->duration_type;
                                                                $duration =  $order->payment->standardPlan->duration;
                                                            @endphp
                                                        @endif
                                                        @if($order->payment->plan_type == "premium" )
                                                            {{ $order->payment->premiumPlan->plan_title }}
                                                            @php
                                                                $price = $order->payment->basicPlan->price;
                                                                $duration_type =  $order->payment->premiumPlan->duration_type;
                                                                $duration =  $order->payment->premiumPlan->duration;
                                                            @endphp

                                                        @endif
                                                        &nbsp;
                                                    </p>
                                                </td>
                                                <td class="text-14">
                                                    @php
                                                        $current = Carbon\Carbon::now();
                                                        $dt = new Carbon\Carbon($order->payment->created_at);
                                                        // $dt->addDays(3);
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14">
                                                    @php
                                                        if($duration_type == "hour(s)"){
                                                            $dt->addHours($duration);
                                                            $diff = $dt->diffInMinutes($current);
                                                        }
                                                        if($duration_type == "week(s)"){
                                                            $dt->addWeeks($duration);
                                                            $diff = $dt->diffInDays($current);
                                                        }
                                                        if($duration_type == "day(s)"){
                                                            $dt->addDays($duration);
                                                            $diff = $dt->diffInHours($current);
                                                        }
                                                        if($duration_type == "month(s)"){
                                                            $dt->addDays($duration);
                                                            $diff = $dt->diffInWeeks($current);
                                                        }
                                                        echo $dt->toFormattedDateString();
                                                    @endphp
                                                </td>
                                                <td class="text-14 font-weight-bold">
                                                    <span class="badge badge-primary price-badge-pink-light mr-3 text-16">
                                                        {{ $price }}
                                                    </span>
                                                </td>
                                                <td class="text-14 font-weight-bold"><span class="badge badge-primary price-badge-thick mr-3 bg-primary-light text-white">{{ $order->status }}</span></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane pt-5" role="tabpanel" id="tab-2">
                            <div class="table-responsive">

                            </div>
                        </div>
                        <div class="tab-pane pt-5" role="tabpanel" id="tab-3">
                            <div class="table-responsive">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
