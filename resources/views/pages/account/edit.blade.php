@extends('layouts.custom')
@section('appjs')

@endsection
@section('content')
{{-- <script>
    $('select').select2();
</script> --}}
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Account Setting<br></h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Setting</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page' =>"setting"])
<section id="main-check-out" class="section-padding">
    <div class="container">
        <form enctype="multipart/form-data" action="{{ route('account.update',['id'=>auth()->id()]) }}" method="post">
            @csrf
            @include('layouts.alert')
            <div class="row">
                @seller
                    <div class="col col-lg-6 col-12">
                        <div class="row">
                            <div class="col">
                                <div class="shadow-sm bg-white pb-5 pt-5 mb-4 mt-2 mt-lg-0 pl-3 pr-3">
                                    <h3 class="mb-5">Profile image &amp; Cover Image</h3>
                                    <div class="row mb-5">
                                        <div class="col col-lg-3 text-center">
                                            <img class="rounded-circle img-fluid mb-3 mb-lg-0" id="profile-picture-avatar" src="{{ asset('storage/profile images/'.$account['profile_image']) }}" width="100">
                                        </div>
                                        <div class="col align-self-center col-lg-9">
                                            <p class="text-18 font-weight-bold">Profile Image</p>
                                            <p class="mb-3">JPG, PNG or GIF</p>
                                            <div class="form-group">
                                                <input name="profile_image" class="@error('profile_image') is-invalid @enderror" type="file" >
                                                @error('profile_image')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <p class="text-18 font-weight-bold mb-3 text-left">Cover Image</p>
                                        <img class="img-fluid mb-3" src="{{ asset('storage/cover images/'.$account['cover_image']) }}">
                                        <div class="form-group text-left">
                                            <input name="cover_image" class="@error('cover_image') is-invalid @enderror" type="file">
                                            @error('cover_image')
                                                {{-- @php
                                                    dd($errors->all()[1])
                                                @endphp --}}
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->all()[1] }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-4 mt-2 mt-lg-0">
                                    <div class="form-group row">
                                        <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                            <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #3b5998;">
                                                <p class="text-white"><i class="icon-social-facebook"></i></p>
                                            </div>
                                        </div>
                                        <div class="col col-lg-10">
                                            <input value="{{ old("facebook") }}" name="facebook" class="form-control main-input-form" type="text" placeholder="Facebook">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                            <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #db4a39;">
                                                <p class="text-white"><i class="icon-social-google"></i></p>
                                            </div>
                                        </div>
                                        <div class="col col-lg-10">
                                            <input value="{{ old("google") }}" name="google" class="form-control main-input-form" type="text" placeholder="Google"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                            <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #bc2a8d;">
                                                <p class="text-white"><i class="icon-social-instagram"></i></p>
                                            </div>
                                        </div>
                                        <div class="col col-lg-10"><input value="{{ old("instagram") }}" name="instagram" class="form-control main-input-form" type="text" placeholder="Instagram"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                            <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #38A1F3;">
                                                <p class="text-white"><i class="icon-social-twitter"></i></p>
                                            </div>
                                        </div>
                                        <div class="col col-lg-10"><input value="{{ old("twitter") }}" name="twitter" class="form-control main-input-form" type="text" placeholder="Twitter"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                            <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #c9510c;">
                                                <p class="text-white"><i class="icon-social-github"></i></p>
                                            </div>
                                        </div>
                                        <div class="col col-lg-10"><input value="{{ old("github") }}" name="github" class="form-control main-input-form" type="text" placeholder="Github"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                            <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #ea4c89;">
                                                <p class="text-white"><i class="icon-social-dribbble"></i></p>
                                            </div>
                                        </div>
                                        <div class="col col-lg-10"><input value="{{ old("dribbble") }}" name="dribbble" class="form-control main-input-form" type="text" placeholder="Dribble"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                            <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #053eff;">
                                                <p class="text-white"><i class="icon-social-behance"></i></p>
                                            </div>
                                        </div>
                                        <div class="col col-lg-10"><input value="{{ old("behance") }}" name="behance" class="form-control main-input-form" type="text" placeholder="Behance"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                            <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #ef8236;">
                                                <p class="text-white"><i class="fa fa-stack-overflow"></i></p>
                                            </div>
                                        </div>
                                        <div class="col col-lg-10"><input value="{{ old("stack_overflow") }}" name="stack_overflow" class="form-control main-input-form" type="text" placeholder="Stackover Flow"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                            <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #19b7ea;">
                                                <p class="text-white"><i class="fab fa-vimeo-v"></i></p>
                                            </div>
                                        </div>
                                        <div class="col col-lg-10"><input value="{{ old("vimeo") }}" name="vimeo" class="form-control main-input-form" type="text" placeholder="Vimeo"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endSeller
                <div class="col col-lg-6 col-12">
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Personal Information</h3>

                        <div class="mt-4">
                            @public
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2 mt-2">Username/Company Name&nbsp;<span class="span-required">*</span></label>
                                            <input name="name" class="form-control main-input-form" type="text" placeholder="Enter your username..." value="{{ auth()->user()->name }}" required>
                                        </div>
                                    </div>
                                </div>
                            @endPublic
                            @admin
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2 mt-2">Enter Site Name&nbsp;<span class="span-required">*</span></label>
                                            <input name="name" class="form-control main-input-form" type="text" placeholder="Enter site name..." value="{{ $siteSetting->name }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2 mt-2">Enter Site Tagline&nbsp;<span class="span-required">*</span></label>
                                            <input name="tagline" class="form-control main-input-form" type="text" placeholder="Enter site tagline..." value="{{ $siteSetting->tagline }}" required>
                                        </div>
                                    </div>
                                </div>
                            @endAdmin
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Email&nbsp;<span class="span-required">*</span></label>
                                        <input name="email" class="form-control main-input-form" type="email" placeholder="Enter your email..." value="{{ auth()->user()->email }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-6 col-12">
                                        <label class="font-weight-bold mb-2 mt-2">New Password&nbsp;<span class="span-required">*</span></label>
                                        <input name="password" class="form-control main-input-form" type="password" placeholder="">
                                    </div>
                                    <div class="col col-lg-6 col-12">
                                        <label class="font-weight-bold mb-2 mt-2">Confirm Password&nbsp;<span class="span-required">*</span></label>
                                        <input name="password_confirmation" class="form-control main-input-form" type="password" placeholder="">
                                    </div>
                                </div>
                            </div>
                            @seller
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2">Full Name&nbsp;<span class="span-required">*</span></label>
                                            <input value="{{ $account["full_name"] }}" name="full_name" class="form-control main-input-form" type="text" placeholder="Jane Doe" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2 mt-2">Description&nbsp;<span class="span-required">*</span></label>
                                            <textarea name="description" class="form-control main-input-textarea" placeholder="Share a bit about your work experience, cool projects you've completed, and your area of expertise" required>{{ $account["description"] }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2 mt-2">Country&nbsp;<span class="span-required">*</span></label>
                                            <select name="country" class="form-control main-input-form" required>
                                                @foreach ($countries as $country)
                                                    <option @if($account['country'] == $country) selected="selected" @endif value="{{ $country }}">{{ $country }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2 mt-2">Language&nbsp;<span class="span-required">*</span></label>
                                            <select name="language" class="form-control main-input-form" required>
                                                    @foreach ($languages as $language => $value)
                                                    <option @if($account['language'] == $value) selected @endif value="{{ $value }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2 mt-2">Website &nbsp;</label>
                                            <input value="{{ $account["website"] }}" name="website" class="form-control main-input-form" type="text" placeholder="http://">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2 mt-2">Tagline&nbsp;<span class="span-required">*</span></label>
                                            <input value="{{ $account['tagline'] }}" name="tagline" class="form-control main-input-form" type="text" placeholder="Bread Co." required>
                                        </div>
                                    </div>
                                </div>
                            @endSeller
                            @admin
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2 mt-2">Country&nbsp;<span class="span-required">*</span></label>
                                            <select name="country" class="form-control main-input-form" required>
                                                @foreach ($countries as $country)
                                                    <option @if($siteSetting->country == $country) selected="selected" @endif value="{{ $country }}">{{ $country }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2 mt-2">Language&nbsp;<span class="span-required">*</span></label>
                                            <select name="language" class="form-control main-input-form" required>
                                                    @foreach ($languages as $language => $value)
                                                    <option @if($siteSetting->language == $value) selected @endif value="{{ $value }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2 mt-2">Currency&nbsp;<span class="span-required">*</span></label>
                                            <select name="currency" class="form-control main-input-form" required>
                                                @foreach ($currencies as $key => $value)
                                                    <option @if($siteSetting->currency == $key) selected @endif value="{{ $key }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            @endAdmin
                        </div>
                    </div>
                    @admin
                        <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                            <h3>Categories</h3>
                            <div id="category" class="mt-4">
                                @forelse ($categories as $category)
                                    <div class="form-group pb-3 mt-5 ">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Category Name&nbsp;<span class="span-required">*</span></label>
                                                <input type="hidden" value="{{ $category->id }}" name="category_id[]">
                                                <input type="hidden" value="No" name="category_type[]">
                                                <input name="categories[]" class="form-control main-input-form" type="text" placeholder="Enter Category Name..." value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Category Icon&nbsp;<span class="span-required">*</span></label>
                                                @if($category->icon !='')
                                                    <img width="200" class="rounded img-fluid mb-3 d-block mx-auto" src="{{ asset("storage/category icon/".$category->icon) }}">
                                                @endif
                                                <input name="category_icon[]" class="d-block" type="file" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group pb-5">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Category Image&nbsp;<span class="span-required">*</span></label>
                                                <img class="rounded img-fluid mb-3 d-block" src="{{ asset("storage/category images/".$category->image) }}">
                                                <input name="category_image[]" class="d-block" type="file" >
                                            </div>
                                            <div class="col col-12">
                                                <div class="btn text-left btn-danger  mt-3 font-weight-normal">
                                                    <div class="d-flex align-items-center">
                                                        <input type="checkbox" value="{{ $category->id }}" name="category_delete[]">
                                                        <label for="category_delete" class="m-0 ml-2">Delete Category</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <div class="form-group pb-3">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Category Name&nbsp;<span class="span-required">*</span></label>
                                                <input type="hidden" value="Yes" name="category_type[]">
                                                <input name="categories[]" class="form-control main-input-form" type="text" placeholder="Enter Category Name..." value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Category Icon&nbsp;<span class="span-required">*</span></label>
                                                <input name="category_icon[]" class="d-block" type="file" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Category Image&nbsp;<span class="span-required">*</span></label>
                                                <input name="category_image[]" class="d-block" type="file" >
                                            </div>
                                        </div>
                                    </div>

                                @endforelse
                                {{-- <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2">Category Name&nbsp;<span class="span-required">*</span></label>
                                            <input name="categories[]" class="form-control main-input-form" type="text" placeholder="Enter your username..." value="{{ auth()->user()->name }}" required>
                                        </div>
                                    </div>
                                </div> --}}

                            </div>
                            <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-category" type="button">Add Category</button>
                        </div>

                        <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                            <h3>Sub Categories</h3>
                            <div id="sub-category" class="mt-4">
                                @forelse ($subCategories as $subCategory)
                                    <div class="form-group pb-3">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <input type="hidden" name="sub_category_type[]" value="No">
                                                <input type="hidden" name="sub_category_id[]" value="{{ $subCategory->id }}">
                                                <label class="font-weight-bold mb-2">Sub Category Name&nbsp;<span class="span-required">*</span></label>
                                                <input name="sub_categories[]" class="form-control main-input-form" type="text" placeholder="Enter your username..." value="{{ $subCategory->name }}" required>
                                            </div>
                                        </div>
                                    </div>
                                 @empty
                                    <div class="form-group pb-3">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Sub Category Name&nbsp;<span class="span-required">*</span></label>
                                                <input type="hidden" name="sub_category_type[]" value="Yes">
                                                <input name="sub_categories[]" class="form-control main-input-form" type="text" placeholder="Enter Sub Category Name..." value="">
                                            </div>
                                        </div>
                                    </div>
                                @endforelse

                            </div>
                            <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-sub-category" type="button">Add Sub Category</button>
                        </div>

                        <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                            <h3>Skills</h3>
                            <div id="skill" class="mt-4">
                                @forelse ($skills as $skill)
                                    <div class="form-group pb-3">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Skill Name&nbsp;<span class="span-required">*</span></label>
                                                <input type="hidden" name="skill_type[]" value="No">
                                                <input type="hidden" name="skill_id[]" value="{{ $skill->id }}">
                                                <input name="skills[]" class="form-control main-input-form" type="text" placeholder="Enter Skill Name..." value="{{ $skill->name }}" >
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <div class="form-group pb-3">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Skill Name&nbsp;<span class="span-required">*</span></label>
                                                <input type="hidden" name="skill_type[]" value="Yes">
                                                <input name="skills[]" class="form-control main-input-form" type="text" placeholder="Enter your username..." value="{{ auth()->user()->name }}" required>
                                            </div>
                                        </div>
                                    </div>
\                               @endforelse

                            </div>
                            <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-skill" type="button">Add Skill</button>
                        </div>
                    @endAdmin
                    @seller
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Professionl Information</h3>
                        <div class="mt-4">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Your Occupation &nbsp;<span class="span-required">*</span></label>
                                        <select name="occupation" class="form-control main-input-form" required>
                                            <option value="Programming & Tech" @if($account['occupation'] == "Programming & Tech") selected @endif>Programming &amp; Tech</option>
                                            @foreach ($categories as $item)
                                                <option value="{{ $item->id }}" @if($account['occupation'] == $item->id) selected @endif>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Years Of Experience &nbsp;<span class="span-required">*</span></label>
                                        <input value="{{ $account['experience'] }}" name="experience" class="form-control main-input-form" type="text" placeholder="1" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Skills &nbsp;<span class="span-required">*</span></label>
                                        <select class="form-control main-input-form d-block" id="skills" multiple="multiple" name="skills[]" required>
                                            @foreach ($skills as $item)

                                                <option @foreach(auth()->user()->skills as $skill) @if($skill->name == $item->id) selected @endif @endforeach value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endSeller
                </div>

                @admin
                    <div class="col col-lg-6 col-12">
                        <div class="row">
                            <div class="col">
                                <div class="shadow-sm bg-white pb-5 pt-5 mb-4 mt-2 mt-lg-0 pl-3 pr-3">
                                    <h3>Mail Settings</h3>
                                    <div class="mt-4">
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2 mt-2">Username&nbsp;<span class="span-required">*</span></label>
                                                    <input name="mail_username" class="form-control main-input-form" type="text" placeholder="Enter your username..." value="{{ $mailSetting->username }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2 mt-2">From Mail Username&nbsp;<span class="span-required">*</span></label>
                                                    <input name="mail_from_username" class="form-control main-input-form" type="text" placeholder="Enter From Mail Username..." value="{{ $mailSetting->from_username }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2 mt-2">From Mail Address&nbsp;<span class="span-required">*</span></label>
                                                    <input name="from_mail_address" class="form-control main-input-form" type="email" placeholder="Enter From Mail Address..." value="{{ $mailSetting->email }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-6 col-12">
                                                    <label class="font-weight-bold mb-2 mt-2">Encryption&nbsp;<span class="span-required">*</span></label>
                                                    <input name="mail_encryption" value="{{ $mailSetting->encryption }}" class="form-control main-input-form" type="text" placeholder="" required>
                                                </div>
                                                <div class="col col-lg-6 col-12">
                                                    <label class="font-weight-bold mb-2 mt-2">Port&nbsp;<span class="span-required">*</span></label>
                                                    <input name="mail_port" value="{{ $mailSetting->port }}" class="form-control main-input-form" type="text" placeholder="" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-6 col-12">
                                                    <label class="font-weight-bold mb-2 mt-2">Mail Password&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ $mailSetting->password }}" name="mail_password" class="form-control main-input-form" type="password" placeholder="" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col">
                                    <div class="shadow-sm bg-white pb-5 pt-5 mb-4 mt-2 mt-lg-0 pl-3 pr-3">
                                        <h3>Payment Settings</h3>
                                        <div class="mt-4">
                                            <div class="form-group">
                                                <div class="form-row">
                                                    <div class="col col-lg-12">
                                                        <label class="font-weight-bold mb-2 mt-2">Paypal Secret Key&nbsp;<span class="span-required">*</span></label>
                                                        <input name="secret_key" class="form-control main-input-form" type="text" placeholder="Paypal Secret Key..." value="{{ $paymentSetting->secret_key }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-row">
                                                    <div class="col col-lg-12">
                                                        <label class="font-weight-bold mb-2 mt-2">Paypal Client ID&nbsp;<span class="span-required">*</span></label>
                                                        <input name="client_id" class="form-control main-input-form" type="text" placeholder="Enter Paypal Client ID..." value="{{ $paymentSetting->publishable_key }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-row">
                                                    <div class="col col-lg-12">
                                                        <label class="font-weight-bold mb-2 mt-2">Estimated Texes & Fees&nbsp;<span class="span-required">*</span></label>
                                                        <input name="tax" class="form-control main-input-form" type="text" placeholder="Estimated Texes & Fees..." value="{{ $paymentSetting->percentage }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                @endAdmin
            </div>

            <div class="row pt-5 mb-5">
                <div class="col text-center">
                    <button type="submit" class="btn btn-primary text-white fr-cl-bcs btn-lg" role="button">Save</button>
                </div>
            </div>
        </form>
    </div>
</section>
<script>
    $('select').select2();

    //Add Category
    $("#add-category").on("click",function(){
        var formCode = `
        <div class="form-group pb-3">
            <div class="form-row">
                <div class="col col-lg-12">
                    <label class="font-weight-bold mb-2">Category Name&nbsp;<span class="span-required">*</span></label>
                    <input type="hidden" value="Yes" name="category_type[]">
                    <input name="categories[]" class="form-control main-input-form" type="text" placeholder="" value="" >
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
                <div class="col col-lg-12">
                    <label class="font-weight-bold mb-2">Category Icon&nbsp;<span class="span-required">*</span></label>
                    <input name="category_icon[]" class="d-block" type="file" >
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
                <div class="col col-lg-12">
                    <label class="font-weight-bold mb-2">Category Image&nbsp;<span class="span-required">*</span></label>
                    <input name="category_image[]" class="d-block" type="file" >
                </div>
            </div>
        </div>
        `

        $("#category").append($(formCode).fadeIn(5000)) ;
    })

    //Add Sub Category
    $("#add-sub-category").on("click",function(){
        var formCode = `
            <div class="form-group  pb-3">
                <div class="form-row">
                    <div class="col col-lg-12">
                        <label class="font-weight-bold mb-2">Sub Category Name&nbsp;<span class="span-required">*</span></label>
                        <input type="hidden" name="sub_category_type[]" value="Yes">
                        <input name="sub_categories[]" class="form-control main-input-form" type="text" placeholder="Enter Sub Category Name..." value="">
                    </div>
                </div>
            </div>
            `

        $("#sub-category").append($(formCode).fadeIn(5000)) ;
    })

    //Skill
    $("#add-skill").on("click",function(){
        var formCode = `
        <div class="form-group pb-3">
            <div class="form-row">
                <div class="col col-lg-12">
                    <label class="font-weight-bold mb-2">Skill Name&nbsp;<span class="span-required">*</span></label>
                    <input type="hidden" name="skill_type[]" value="Yes">
                    <input name="skills[]" class="form-control main-input-form" type="text" placeholder="Enter your username..." value="" >
                </div>
            </div>
        </div>
        `

        $("#skill").append($(formCode).fadeIn(5000)) ;
    })
    $('.demo').iconpicker();

</script>
@endsection
