@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="main-become-a-seller-jumb">
    <h1 class="display-4 text-white mb-3 font-weight-normal">Work Your Way</h1>
    <h2 class="text-white font-weight-light mb-5"><strong>You bring the skill. We'll make earning easy</strong></h2>
    <p></p>
</div>
<section id="main-check-out" class="section-padding">
    <div class="container">
        <form enctype="multipart/form-data" action="{{ route('account.store') }}" method="post">
            @csrf
            @include('layouts.alert')
            <div class="row">
                <div class="col col-lg-6 col-12">
                    <div class="row">
                        <div class="col">
                            <div class="shadow-sm bg-white pb-5 pt-5 mb-4 mt-2 mt-lg-0 pl-3 pr-3">
                                <h3 class="mb-5">Profile image &amp; Cover Image</h3>
                                <div class="row mb-5">
                                    {{-- <div class="col col-lg-3 text-center">
                                        <img class="rounded-circle img-fluid mb-3 mb-lg-0" id="profile-picture-avatar" src="../assets/img/man.png" width="100">
                                    </div> --}}
                                    <div class="col align-self-center col-lg-9">
                                        <p class="text-18 font-weight-bold">Profile Image</p>
                                        <p class="mb-3">JPG, PNG or GIF</p>
                                        <div class="form-group">
                                            <input name="profile_image" class="@error('profile_image') is-invalid @enderror" type="file" required>
                                            @error('profile_image')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <p class="text-18 font-weight-bold mb-3 text-left">Cover Image</p>
                                    {{-- <img class="img-fluid mb-3" src="../assets/img/nischal-masand-270164-unsplash.jpg"> --}}
                                    <div class="form-group text-left">
                                        <input name="cover_image" class="@error('cover_image') is-invalid @enderror" type="file" required>
                                        @error('cover_image')
                                            {{-- @php
                                                dd($errors->all()[1])
                                            @endphp --}}
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->all()[1] }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-4 mt-2 mt-lg-0">
                                <div class="form-group row">
                                    <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                        <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #3b5998;">
                                            <p class="text-white"><i class="icon-social-facebook"></i></p>
                                        </div>
                                    </div>
                                    <div class="col col-lg-10">
                                        <input value="{{ old("email") }}" name="facebook" class="form-control main-input-form" type="text" placeholder="Facebook">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                        <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #db4a39;">
                                            <p class="text-white"><i class="icon-social-google"></i></p>
                                        </div>
                                    </div>
                                    <div class="col col-lg-10">
                                        <input value="{{ old("google") }}" name="google" class="form-control main-input-form" type="text" placeholder="Google"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                        <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #bc2a8d;">
                                            <p class="text-white"><i class="icon-social-instagram"></i></p>
                                        </div>
                                    </div>
                                    <div class="col col-lg-10"><input value="{{ old("instagram") }}" name="instagram" class="form-control main-input-form" type="text" placeholder="Instagram"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                        <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #38A1F3;">
                                            <p class="text-white"><i class="icon-social-twitter"></i></p>
                                        </div>
                                    </div>
                                    <div class="col col-lg-10"><input value="{{ old("twitter") }}" name="twitter" class="form-control main-input-form" type="text" placeholder="Twitter"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                        <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #c9510c;">
                                            <p class="text-white"><i class="icon-social-github"></i></p>
                                        </div>
                                    </div>
                                    <div class="col col-lg-10"><input value="{{ old("github") }}" name="github" class="form-control main-input-form" type="text" placeholder="Github"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                        <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #ea4c89;">
                                            <p class="text-white"><i class="icon-social-dribbble"></i></p>
                                        </div>
                                    </div>
                                    <div class="col col-lg-10"><input value="{{ old("dribbble") }}" name="dribbble" class="form-control main-input-form" type="text" placeholder="Dribble"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                        <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #053eff;">
                                            <p class="text-white"><i class="icon-social-behance"></i></p>
                                        </div>
                                    </div>
                                    <div class="col col-lg-10"><input value="{{ old("behance") }}" name="behance" class="form-control main-input-form" type="text" placeholder="Behance"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                        <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #ef8236;">
                                            <p class="text-white"><i class="fa fa-stack-overflow"></i></p>
                                        </div>
                                    </div>
                                    <div class="col col-lg-10"><input value="{{ old("stack_overflow") }}" name="stack_overflow" class="form-control main-input-form" type="text" placeholder="Stackover Flow"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col align-self-center col-lg-2 col-3 col-sm-2">
                                        <div class="d-flex justify-content-center align-items-center account-setting-icon-label text-white" style="background: #19b7ea;">
                                            <p class="text-white"><i class="fab fa-vimeo-v"></i></p>
                                        </div>
                                    </div>
                                    <div class="col col-lg-10"><input value="{{ old("vimeo") }}" name="vimeo" class="form-control main-input-form" type="text" placeholder="Vimeo"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-lg-6 col-12">
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Personal Information</h3>
                        <div class="mt-4">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2">Full Name&nbsp;<span class="span-required">*</span></label>
                                        <input value="{{ old("full_name") }}" name="full_name" class="form-control main-input-form" type="text" placeholder="Jane Doe" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Description&nbsp;<span class="span-required">*</span></label>
                                        <textarea name="description" class="form-control main-input-textarea" placeholder="Share a bit about your work experience, cool projects you've completed, and your area of expertise" required>{{ old("description") }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Country&nbsp;<span class="span-required">*</span></label>
                                        <select name="country" class="form-control main-input-form" required>
                                            @foreach ($countries as $country)
                                                <option value="{{ $country }}">{{ $country }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Language&nbsp;<span class="span-required">*</span></label>
                                        <select name="language" class="form-control main-input-form" required>
                                            @foreach ($languages as $language => $value)
                                                <option value="{{ $value }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Website &nbsp;</label>
                                        <input value="{{ old("website") }}" name="website" class="form-control main-input-form" type="text" placeholder="http://">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Tagline&nbsp;<span class="span-required">*</span></label>
                                        <input value="{{ old("tagline") }}" name="tagline" class="form-control main-input-form" type="text" placeholder="Enter your tagline here.." required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Professionl Information</h3>
                        <div class="mt-4">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Your Occupation &nbsp;<span class="span-required">*</span></label>
                                        <select name="occupation" class="form-control main-input-form" required>
                                            @foreach ($categories as $item)
                                                <option value="{{ $item->id }}" selected="">{{ $item->name }}</option>
                                            @endforeach

                                        </select>
                                        </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Years Of Experience &nbsp;<span class="span-required">*</span></label>
                                        <input value="{{ old("experience") }}" name="experience" class="form-control main-input-form" type="text" placeholder="1" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Skills &nbsp;<span class="span-required">*</span></label>
                                        <select class="form-control main-input-form d-block" id="skills" multiple="multiple" name="skills[]" required>
                                            <option value="HTML">HTML</option>
                                            @foreach ($skills as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                            {{-- <option value="Php">Php</option>
                                            <option value="CSS">CSS</option>
                                            <option value="Photoshop">Photoshop</option>
                                            <option value="illustrator">illustrator</option>
                                            <option value="Corel Draw">Corel Draw</option> --}}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-5 mb-5">
                <div class="col text-center">
                    <button type="submit" class="btn btn-primary text-white fr-cl-bcs btn-lg" role="button">Save</button>
                </div>
            </div>
        </form>
    </div>
</section>
<script>
    $('select').select2();
</script>
@endsection
