@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="main-become-a-seller-jumb">
    <h1 class="display-4 text-white mb-3 font-weight-normal">Work Your Way</h1>
    <h2 class="text-white font-weight-light mb-5"><strong>You bring the skill. We'll make earning easy</strong></h2>
    <p></p>
</div>
<section id="become-a-seller-section-one" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-7 text-center"><img class="img-fluid" src="{{ asset('img/form.png') }}" width="400"></div>
            <div class="col col-lg-5">
                <h3 class="font-weight-normal mt-5 mb-4">Create Your Account</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque
                    the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;</p>
                    <a class="btn btn-primary mt-4 fr-cl-bcs @auth disabled @endauth  btn-lg" role="button" href="{{ route('register') }}">Register</a></div>
        </div>
    </div>
</section>
<section id="become-a-seller-section-two" class="section-padding bg-color">
    <div class="container">
        <div class="row">
            <div class="col col-lg-5">
                <h3 class="font-weight-normal mt-5 mb-4">Upload Your Product</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque
                    the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;</p><button class="btn btn-primary mt-4 fr-cl-bcs btn-lg" type="button" style="background: #7345bf;border: none;">Upload Items</button></div>
            <div class="col col-lg-7 text-center"><img class="img-fluid" src="{{ asset('img/receipt.png') }}" width="400"></div>
        </div>
    </div>
</section>
<section id="become-a-seller-section-three" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-7 text-center"><img class="img-fluid" src="{{ asset('img/envelope.png') }}" width="400"></div>
            <div class="col col-lg-5">
                <h3 class="font-weight-normal mt-5 mb-4">Start Making Money</h3>
                <p class="mb-4">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque
                    the mattis, leo quam aliquet diam congue is laoreet elit metus.&nbsp;</p>
                    <a href="{{ route('account.two') }}" class="btn btn-primary mt-4 fr-cl-bcs btn-lg btn-white" role="button" style="background: #ffffff;border: none;" href="#">Continue</a></div>
        </div>
    </div>
</section>
{{-- <section id="home-main-featured-products" class="section-padding bg-color">
    <div class="container">
        <div class="row shadow-0 mb-5">
            <div class="col col-md-9 col-sm-8 col-6">
                <div>
                    <h2 class="mb-2 font-weight-normal text-left">Seller's Stories</h2>
                </div>
            </div>
            <div class="col text-right col-md-3 col-sm-4 col-6">
                <div></div><a id="prev2" class="prev d-inline-block text-center" href="#"><i class="icon-arrow-left"></i></a><a id="next2" class="next d-inline-block text-center" href="#"><i class="icon-arrow-right"></i></a></div>
        </div>
        <div class="row">
            <div class="col pl-0 pr-0">
                <div class="carousel slide" data-ride="carousel" data-interval="false" data-pause="false" data-keyboard="false" id="carousel-2">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col col-lg-4 mb-3 col-md-6 col-12">
                                    <div class="row">
                                        <div class="col"><img class="img-fluid d-block" src="{{ asset('img/austin-distel-1539000-unsplash.jpg') }}">
                                            <div class="bg-white p-3 custom-box-shadow">
                                                <h5 class="mb-1 font-weight-normal">Umolu Jones</h5>
                                                <div>
                                                    <div>
                                                        <p class="text-purple"><i class="icon-direction"></i>&nbsp;Bahama Store</p>
                                                    </div>
                                                </div>
                                                <p class="mt-2 mb-2 text-center">"Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis. "</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-lg-4 mb-3 col-md-6 col-12">
                                    <div class="row">
                                        <div class="col"><img class="img-fluid d-block" src="{{ asset('img/austin-distel-1539000-unsplash.jpg') }}">
                                            <div class="bg-white p-3 custom-box-shadow">
                                                <h5 class="mb-1 font-weight-normal">Umolu Jones</h5>
                                                <div>
                                                    <div>
                                                        <p class="text-purple"><i class="icon-direction"></i>&nbsp;Bahama Store</p>
                                                    </div>
                                                </div>
                                                <p class="mt-2 mb-2 text-center">"Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis. "</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-lg-4 mb-3 col-md-6 col-12">
                                    <div class="row">
                                        <div class="col"><img class="img-fluid d-block" src="{{ asset('img/austin-distel-1539000-unsplash.jpg') }}">
                                            <div class="bg-white p-3 custom-box-shadow">
                                                <h5 class="mb-1 font-weight-normal">Umolu Jones</h5>
                                                <div>
                                                    <div>
                                                        <p class="text-purple"><i class="icon-direction"></i>&nbsp;Bahama Store</p>
                                                    </div>
                                                </div>
                                                <p class="mt-2 mb-2 text-center">"Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis. "</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col col-lg-4 mb-3 col-md-6 col-12">
                                    <div class="row">
                                        <div class="col"><img class="img-fluid d-block" src="{{ asset('img/austin-distel-1539000-unsplash.jpg') }}">
                                            <div class="bg-white p-3 custom-box-shadow">
                                                <h5 class="mb-1 font-weight-normal">Umolu Jones</h5>
                                                <div>
                                                    <div>
                                                        <p class="text-purple"><i class="icon-direction"></i>&nbsp;Bahama Store</p>
                                                    </div>
                                                </div>
                                                <p class="mt-2 mb-2 text-center">"Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis. "</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-lg-4 mb-3 col-md-6 col-12">
                                    <div class="row">
                                        <div class="col"><img class="img-fluid d-block" src="{{ asset('img/austin-distel-1539000-unsplash.jpg') }}">
                                            <div class="bg-white p-3 custom-box-shadow">
                                                <h5 class="mb-1 font-weight-normal">Umolu Jones</h5>
                                                <div>
                                                    <div>
                                                        <p class="text-purple"><i class="icon-direction"></i>&nbsp;Bahama Store</p>
                                                    </div>
                                                </div>
                                                <p class="mt-2 mb-2 text-center">"Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis. "</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-lg-4 mb-3 col-md-6 col-12">
                                    <div class="row">
                                        <div class="col"><img class="img-fluid d-block" src="{{ asset('img/austin-distel-1539000-unsplash.jpg') }}">
                                            <div class="bg-white p-3 custom-box-shadow">
                                                <h5 class="mb-1 font-weight-normal">Umolu Jones</h5>
                                                <div>
                                                    <div>
                                                        <p class="text-purple"><i class="icon-direction"></i>&nbsp;Bahama Store</p>
                                                    </div>
                                                </div>
                                                <p class="mt-2 mb-2 text-center">"Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis. "</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col col-lg-4 mb-3 col-md-6 col-12">
                                    <div class="row">
                                        <div class="col"><img class="img-fluid d-block" src="{{ asset('img/austin-distel-1539000-unsplash.jpg') }}">
                                            <div class="bg-white p-3 custom-box-shadow">
                                                <h5 class="mb-1 font-weight-normal">Umolu Jones</h5>
                                                <div>
                                                    <div>
                                                        <p class="text-purple"><i class="icon-direction"></i>&nbsp;Bahama Store</p>
                                                    </div>
                                                </div>
                                                <p class="mt-2 mb-2 text-center">"Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis. "</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-lg-4 mb-3 col-md-6 col-12">
                                    <div class="row">
                                        <div class="col"><img class="img-fluid d-block" src="{{ asset('img/austin-distel-1539000-unsplash.jpg') }}">
                                            <div class="bg-white p-3 custom-box-shadow">
                                                <h5 class="mb-1 font-weight-normal">Umolu Jones</h5>
                                                <div>
                                                    <div>
                                                        <p class="text-purple"><i class="icon-direction"></i>&nbsp;Bahama Store</p>
                                                    </div>
                                                </div>
                                                <p class="mt-2 mb-2 text-center">"Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis. "</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-lg-4 mb-3 col-md-6 col-12">
                                    <div class="row">
                                        <div class="col"><img class="img-fluid d-block" src="{{ asset('img/austin-distel-1539000-unsplash.jpg') }}">
                                            <div class="bg-white p-3 custom-box-shadow">
                                                <h5 class="mb-1 font-weight-normal">Umolu Jones</h5>
                                                <div>
                                                    <div>
                                                        <p class="text-purple"><i class="icon-direction"></i>&nbsp;Bahama Store</p>
                                                    </div>
                                                </div>
                                                <p class="mt-2 mb-2 text-center">"Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis. "</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-none"><a class="carousel-control-prev text-truncate" href="#carousel-2" role="button" data-slide="prev"><span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carousel-2"
                            role="button" data-slide="next"><span class="carousel-control-next-icon"></span><span class="sr-only">Next</span></div></div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<section id="become-a-seller-call-to-action" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-12">
                <div>
                    <h1 class="text-center text-white font-weight-normal mb-3">Get The Best Out OF &nbsp;Our Market Place</h1>
                    <p class="text-center text-white">MartPlace is the most powerful, &amp; customizable template for Easy Digital Downloads Products</p>
                    <div class="text-center mb-5 mb-sm-0 mt-5"><a class="btn-lg d-none d-sm-inline call-to-action" href="#">Get Started</a></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
