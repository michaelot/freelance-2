@extends('layouts.custom')
@section('appjs')

@endsection
@section('content')
{{-- <script>
    $('select').select2();
</script> --}}
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Account Setting<br></h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Setting</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page' =>"managePages"])
<section id="main-check-out" class="section-padding">
    <div class="container">
        <form enctype="multipart/form-data" action="{{ route('account.update.pages') }}" method="post">
            @csrf
            @method("PUT")
            @include('layouts.alert')
            <div class="row">
                <div class="col col-lg-6 col-12">
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Guest Home Page</h3>

                        <div class="mt-4">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Heading&nbsp;<span class="span-required">*</span></label>
                                        <input value="{{ $home->heading }}" name="heading" class="form-control main-input-form" type="text" placeholder="" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold h mb-2 mt-2">Sub Heading&nbsp;<span class="span-required">*</span></label>
                                        <input value="{{ $home->sub_heading }}" name="sub_heading" class="form-control main-input-form" type="text" placeholder="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mb-5">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Header Background Image&nbsp;<span class="span-required">*</span></label>
                                        <div class="mt-3 mb-3">
                                            <img src="{{ asset("storage/page background/".$home->background) }}" class="img-fluid" alt="">
                                        </div>
                                        <input name="background" class="form-control main-input-form" type="file">
                                    </div>
                                </div>
                            </div>

                            <h3 class="mt-5 mb-5">Features</h3>
                            <div id="features">
                                @forelse ($home->features as $item)
                                    <input type="hidden" name="feature_type[]" value="old">
                                    <input type="hidden" name="feature_id[]" value="{{ $item->id }}">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Feature  Title&nbsp;<span class="span-required">*</span></label>
                                                <input value="{{ $item->title }}" name="feature_title[]" class="form-control main-input-form" type="text" placeholder="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Feature text&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="feature_text[]" class="form-control main-input-form" type="text" placeholder="">{{ $item->text }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group mb-5">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Feature Icon(svg/png)&nbsp;<span class="span-required">*</span></label>
                                                <div class="mt-3 mb-3">
                                                    <img width="200" src="{{ asset("storage/page icon/".$item->icon) }}" class="img-fluid mb-3 d-block mx-auto" alt="">
                                                </div>
                                                <input value="" name="feature_icon[]" class="form-control main-input-form" type="file" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <input type="hidden" name="feature_type[]" value="new">

                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Feature  Title&nbsp;<span class="span-required">*</span></label>
                                                <input value="" name="feature_title[]" class="form-control main-input-form" type="text" placeholder="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Feature text&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="feature_text[]" class="form-control main-input-form" type="text" placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group mb-5">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Feature Icon(svg/png)&nbsp;<span class="span-required">*</span></label>
                                                <input value="" name="feature_icon[]" class="form-control main-input-form" type="file" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                @endforelse

                            </div>
                            <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-feature" type="button">Add Feature</button>


                            <h3 class="mt-5 mb-5">Why Choose us</h3>

                            <div id="wcu">
                                @forelse ($home->wcu as $wcu)
                                    <input type="hidden" name="wcu_type[]" value="old">
                                    <input type="hidden" name="wcu_id[]" value="{{ $wcu->id }}">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Why Choose Us Title &nbsp;<span class="span-required">*</span></label>
                                                <input value="{{ $wcu->title }}" name="wcu_title[]" class="form-control main-input-form" type="text" placeholder="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Why Choose Us Text&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="wcu_text[]" class="form-control main-input-form" type="text" placeholder="">{{ $wcu->text }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group mb-5">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Why Choose Us Icon(svg/png)&nbsp;<span class="span-required">*</span></label>
                                                <div class="mt-3 mb-3">
                                                    <img src="{{ asset("storage/page icon/".$wcu->icon) }}" width="200" class="rounded img-fluid mb-3 d-block mx-auto" alt="">
                                                </div>
                                                <input value="" name="wcu_icon[]" class="form-control main-input-form" type="file" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                @empty

                                    <input type="hidden" name="wcu_type[]" value="new">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Why Choose Us Title &nbsp;<span class="span-required">*</span></label>
                                                <input value="" name="wcu_title[]" class="form-control main-input-form" type="text" placeholder="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Why Choose Us Text&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="wcu_text[]" class="form-control main-input-form" type="text" placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group mb-5">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Why Choose Us Icon(svg/png)&nbsp;<span class="span-required">*</span></label>
                                                <input name="wcu_icon[]" class="form-control main-input-form" type="file" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                @endforelse

                            </div>

                            <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-wcu" type="button">Add WCU</button>

                            <h3 class="mt-5 mb-5">Testimonials</h3>
                            <div id="testimonial" class="mt-4">
                                @forelse ($home->testimonials as $item)
                                    <input type="hidden" name="comment_type[]" value="old">
                                    <input type="hidden" name="comment_id[]" value="{{ $item->id }}">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Comment&nbsp;<span class="span-required">*</span></label>
                                                <textarea name="comment[]" class="form-control main-input-textarea" placeholder="">{{ $item->text }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-6">
                                                <label class="font-weight-bold mb-2">Full Name&nbsp;<span class="span-required">*</span></label>
                                                <input value="{{ $item->name }}" name="comment_name[]" class="form-control main-input-form" type="text" placeholder="">
                                            </div>
                                            <div class="col col-lg-6">
                                                <label class="font-weight-bold mb-2">Title&nbsp;<span class="span-required">*</span></label>
                                                <input value="{{ $item->title }}" name="comment_title[]" class="form-control main-input-form" type="text" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mb-5">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Image&nbsp;<span class="span-required">*</span></label>
                                                <div class="mt-3 mb-3">
                                                    <img src="{{ asset("storage/page icon/".$item->image) }}" width="200" class="rounded img-fluid mb-3 d-block mx-auto" alt="">
                                                </div>
                                                <input type="file" name="comment_image[]" class="form-control main-input-form" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <input type="hidden" name="comment_type[]" value="new">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Comment&nbsp;<span class="span-required">*</span></label>
                                                <textarea name="comment[]" class="form-control main-input-textarea" placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-6">
                                                <label class="font-weight-bold mb-2">Full Name&nbsp;<span class="span-required">*</span></label>
                                                <input value="" name="comment_name[]" class="form-control main-input-form" type="text" placeholder="">
                                            </div>
                                            <div class="col col-lg-6">
                                                <label class="font-weight-bold mb-2">Title&nbsp;<span class="span-required">*</span></label>
                                                <input value="" name="comment_title[]" class="form-control main-input-form" type="text" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mb-5">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Image&nbsp;<span class="span-required">*</span></label>
                                                <input type="file" name="comment_image[]" class="form-control main-input-form" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                @endforelse
                                {{-- <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2">Comment&nbsp;<span class="span-required">*</span></label>
                                            <textarea name="comment[]" class="form-control main-input-textarea" placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-6">
                                            <label class="font-weight-bold mb-2">Full Name&nbsp;<span class="span-required">*</span></label>
                                            <input value="" name="comment_name[]" class="form-control main-input-form" type="text" placeholder="">
                                        </div>
                                        <div class="col col-lg-6">
                                            <label class="font-weight-bold mb-2">Title&nbsp;<span class="span-required">*</span></label>
                                            <input value="" name="comment_title[]" class="form-control main-input-form" type="text" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-5">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2">Image&nbsp;<span class="span-required">*</span></label>
                                            <input name="comment_image[]" class="form-control main-input-textarea" placeholder="">
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                            <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-testimonial" type="button">Add Testimonials</button>

                            <h3 class="mt-5 mb-5">Call To Action</h3>

                            <div class="form-group mb-5">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2"> Background Image&nbsp;<span class="span-required">*</span></label>
                                        <div class="mt-3 mb-3">
                                            <img src="{{ asset("storage/page background/".$home->action_background) }}" class="img-fluid" alt="">
                                        </div>
                                        <input name="call_to_action_background" class="form-control main-input-form" type="file">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2">Heading&nbsp;<span class="span-required">*</span></label>
                                        <textarea name="call_to_action_heading" class="form-control main-input-textarea" >{{ $home->action_heading }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2">SubHeading&nbsp;<span class="span-required">*</span></label>
                                        <textarea name="call_to_action_sub_heading" class="form-control main-input-form" type="text" placeholder="">{{ $home->action_sub_heading }}</textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col col-lg-6 col-12">
                    <div class="row">
                        <div class="col">
                            <div class="shadow-sm bg-white pb-5 pt-5 mb-4 mt-2 mt-lg-0 pl-3 pr-3">
                                <h3>Login Page</h3>
                                <div class="mt-4">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Background Image&nbsp;<span class="span-required">*</span></label>
                                                <div class="mt-3 mb-3">
                                                    <img src="{{ asset("storage/page background/".$login->background) }}" class="img-fluid" alt="">
                                                </div>
                                                <input value="{{ $login->heading }}" name="login_background" class="form-control main-input-form" type="file" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Heading&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="login_header" class="form-control main-input-form" type="text" placeholder="">{{ $login->heading }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">SubHeading&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="login_sub_header" class="form-control main-input-form" type="text" placeholder="">{{ $login->sub_heading }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="shadow-sm bg-white pb-5 pt-5 mb-4 mt-2 mt-lg-0 pl-3 pr-3">
                                <h3>Register Page</h3>
                                <div class="mt-4">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Background Image&nbsp;<span class="span-required">*</span></label>
                                                <div class="mt-3 mb-3">
                                                    <img src="{{ asset("storage/page background/".$register->background) }}" class="img-fluid" alt="">
                                                </div>
                                                <input value="" name="register_background" class="form-control main-input-form" type="file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Heading&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="register_heading" class="form-control main-input-form" type="text" placeholder="">{{ $register->heading }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">SubHeading&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="register_sub_heading" class="form-control main-input-form" type="text" placeholder="">{{ $register->sub_heading }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="shadow-sm bg-white pb-5 pt-5 mb-4 mt-2 mt-lg-0 pl-3 pr-3">
                                <h3>Gigs Page</h3>
                                <div class="mt-4">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Background Image&nbsp;<span class="span-required">*</span></label>
                                                <div class="mt-3 mb-3">
                                                    <img src="{{ asset("storage/page background/".$gigs->background) }}" class="img-fluid" alt="">
                                                </div>
                                                <input value="" name="gigs_background" class="form-control main-input-form" type="file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Heading&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="gig_heading" class="form-control main-input-form" type="text" placeholder="">{{ $gigs->heading }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">SubHeading&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="gig_sub_heading" class="form-control main-input-form" type="text" placeholder="" >{{ $gigs->sub_heading }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="shadow-sm bg-white pb-5 pt-5 mb-4 mt-2 mt-lg-0 pl-3 pr-3">
                                <h3>Category Page</h3>
                                <div class="mt-4">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Background Image&nbsp;<span class="span-required">*</span></label>
                                                <div class="mt-3 mb-3">
                                                    <img src="{{ asset("storage/page background/".$category->background) }}" class="img-fluid" alt="">
                                                </div>
                                                <input value="" name="category_background" class="form-control main-input-form" type="file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">Heading&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="category_heading" class="form-control main-input-form" type="text" placeholder="">{{ $category->heading }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-lg-12">
                                                <label class="font-weight-bold mb-2">SubHeading&nbsp;<span class="span-required">*</span></label>
                                                <textarea value="" name="category_sub_heading" class="form-control main-input-form" type="text" placeholder="">{{ $category->sub_heading }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pt-5 mb-5">
                <div class="col text-center">
                    <button type="submit" class="btn btn-primary text-white fr-cl-bcs btn-lg" role="button">Save</button>
                </div>
            </div>
        </form>
    </div>
</section>
<script>
    $('select').select2();

    //Add Category
    $("#add-feature").on("click",function(){
        var formCode = `
        <input type="hidden" name="feature_type[]" value="new">

        <div id="features">
            <div class="form-group">
                <div class="form-row">
                    <div class="col col-lg-12">
                        <label class="font-weight-bold mb-2">Feature Title&nbsp;<span class="span-required">*</span></label>
                        <input value="" name="feature_title[]" class="form-control main-input-form" type="text" placeholder="">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-row">
                    <div class="col col-lg-12">
                        <label class="font-weight-bold mb-2">Feature text&nbsp;<span class="span-required">*</span></label>
                        <textarea value="" name="feature_text[]" class="form-control main-input-form" type="text" placeholder=""></textarea>
                    </div>
                </div>
            </div>

            <div class="form-group mb-5">
                <div class="form-row">
                    <div class="col col-lg-12">
                        <label class="font-weight-bold mb-2">Feature Icon(svg/png)&nbsp;<span class="span-required">*</span></label>
                        <input value="" name="feature_icon[]" class="form-control main-input-form" type="file" placeholder="">
                    </div>
                </div>
            </div>
        </div>`

        $("#features").append($(formCode).fadeIn(5000)) ;
    })

    //wcu
    $("#add-wcu").on("click",function(){
        var formCode = `
        <input type="hidden" name="wcu_type[]" value="new">

            <div class="form-group">
                <div class="form-row">
                    <div class="col col-lg-12">
                        <label class="font-weight-bold mb-2">Why Choose Us Title &nbsp;<span class="span-required">*</span></label>
                        <input value="" name="wcu_title[]" class="form-control main-input-form" type="text" placeholder="">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-row">
                    <div class="col col-lg-12">
                        <label class="font-weight-bold mb-2">Why Choose Us Text&nbsp;<span class="span-required">*</span></label>
                        <textarea value="" name="wcu_text[]" class="form-control main-input-form" type="text" placeholder=""></textarea>
                    </div>
                </div>
            </div>

            <div class="form-group mb-5">
                <div class="form-row">
                    <div class="col col-lg-12">
                        <label class="font-weight-bold mb-2">Why Choose Us Icon(svg/png)&nbsp;<span class="span-required">*</span></label>
                        <input name="wcu_icon[]" class="form-control main-input-form" type="file" placeholder="">
                    </div>
                </div>
            </div>
        </div>
        `
        $("#wcu").append($(formCode).fadeIn(5000)) ;
    })

    $("#add-testimonial").on("click",function(){
        var formCode = `
        <input type="hidden" name="comment_type[]" value="new">

        <div class="form-group">
            <div class="form-row">
                <div class="col col-lg-12">
                    <label class="font-weight-bold mb-2">Comment&nbsp;<span class="span-required">*</span></label>
                    <textarea name="comment[]" class="form-control main-input-textarea" placeholder=""></textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
                <div class="col col-lg-6">
                    <label class="font-weight-bold mb-2">Full Name&nbsp;<span class="span-required">*</span></label>
                    <input value="" name="comment_name[]" class="form-control main-input-form" type="text" placeholder="">
                </div>
                <div class="col col-lg-6">
                    <label class="font-weight-bold mb-2">Title&nbsp;<span class="span-required">*</span></label>
                    <input value="" name="comment_title[]" class="form-control main-input-form" type="text" placeholder="">
                </div>
            </div>
        </div>
        <div class="form-group mb-5">
            <div class="form-row">
                <div class="col col-lg-12">
                    <label class="font-weight-bold mb-2">Image&nbsp;<span class="span-required">*</span></label>
                    <input type="file" name="comment_image[]" class="form-control main-input-form" placeholder="">
                </div>
            </div>
        </div>
        `
        $("#testimonial").append($(formCode).fadeIn(5000)) ;
    })

</script>
@endsection
