@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="profile-jumbotron" style="background:url('{{ asset("storage/cover images/".$user->seller->cover_image) }}');background-position: left;background-size: cover;">
    <div id="profile-jumbotron-overlay"></div>
    <div class="container h-100">
        <div class="row h-100">
            <div class="col d-flex align-items-end h-100 pb-5 col-lg-8 offset-lg-4">
                <div>
                    <h2 class="text-white mb-2 text-left"><strong>{{ $user->seller->full_name }}</strong><br></h2>
                    <p class="text-left text-white text-20">{{ $user->seller->category->name }}, {{ $user->seller->country }} </p>
                </div>
            </div>
        </div>
    </div>
</div>
<section id="profile-page-main-content" class="section-padding bg-color">
    <div class="container">
        <div class="row">
            <div class="col col-lg-4 col-12" id="profile-page-side-nav">
                <div class="shadow bg-white pt-5 pb-5 pl-3 pr-3 mb-5 rounded">
                    <div class="text-center">
                        <div id="profile-picture-avatar-container" style="width: 150px;margin: auto;">
                            <img id="profile-picture-avatar" src="{{ asset("storage/profile images/".$user->seller->profile_image) }}" width="150">
                            {{-- <div class="pulse animated" id="profile-picture-avatar-edit">
                                <p class="d-flex justify-content-center align-items-center font-weight-bold text-white"><i class="icon ion-ios-camera"></i></p>
                            </div> --}}
                        </div>
                        <h5 class="mt-3 mb-0">{{ $user->name }}</h5>
                        <p class="mb-0" style="font-size: 13px;">{{ $user->seller->tagline }}<br></p>
                        <p class="font-weight-light mt-1 rating font-weight-bold mt-0"><i class="fa fa-star"></i>&nbsp;{{ count($user->reviews) }}</p>
                        <p class="text-left font-weight-bold main-color" style="font-size: 14px;">From<span class="float-right text-black-50">{{ $user->seller->country }}</span></p>
                        <p class="text-left font-weight-bold main-color" style="font-size: 14px;">Joined On<span class="float-right text-black-50">
                        @php
                            $dt = new Carbon\Carbon($user->seller->created_at);
                            echo $dt->toFormattedDateString();
                        @endphp
                        </span></p>
                        <p class="text-left font-weight-bold main-color" style="font-size: 14px;">Languages<span class="float-right text-black-50">{{ $user->seller->language }}</span></p>
                        <p class="text-left font-weight-bold main-color" style="font-size: 14px;">Website<span class="float-right text-black-50">{{ $user->seller->website? $user->seller->website: "-" }}</span></p>
                        <div role="tablist" id="product-sizes-accordian" class="text-left mt-3">
                            <div class="card">
                                <div class="card-header border-0 pl-0" role="tab">
                                    <h6 class="mb-0"><a data-toggle="collapse" aria-expanded="false" aria-controls="product-sizes-accordian .item-1" href="div#product-sizes-accordian .item-1">Skills</a></h6>
                                </div>
                                <div class="collapse item-1" role="tabpanel" data-parent="#product-sizes-accordian">
                                    <div class="card-body pt-0">
                                        @foreach ($user->skills as $skill)
                                            <p>{{ $skill->name }}</p>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="text-20 mt-3 text-left">
                            <a class="text-success text-decoration-none" href="{{ $user->seller->facebook }}"><i class="fab fa-facebook-f"></i></a>
                            <a class="text-success pl-2" href="{{ $user->seller->google }}"><i class="fab fa-google-plus"></i></a>
                            <a class="text-success pl-2" href="{{ $user->seller->dribble }}"><i class="fab fa-dribbble"></i></a>
                            <a class="text-success pl-2" href="{{ $user->seller->twitter }}"><i class="fab fa-twitter"></i></a>
                            <a class="text-success pl-2" href="{{ $user->seller->instagram }}"><i class="fab fa-instagram"></i></a>
                            <a class="text-success pl-2" href="{{ $user->seller->facebook }}"><i class="fab fa-linkedin-in"></i></a>
                            <a class="text-success pl-2" href="{{ $user->seller->stack_overflow }}"><i class="fab fa-stack-overflow"></i></a>
                            <a class="text-success pl-2" href="{{ $user->seller->behance }}"><i class="fab fa-behance"></i></a>
                            <a class="text-success pl-2" href="{{ $user->seller->github }}"><i class="fab fa-github"></i></a>
                            <a class="text-success pl-2" href="{{ $user->seller->vimeo }}"><i class="fab fa-vimeo-v"></i></a>&nbsp;&nbsp;
                        </p>
                            @if(auth()->id()!= $user->id )
                                <a class="btn btn-outline-success btn-block btn-sm text-success mt-4" href="{{ route("message",[$user->id]) }}" role="button"><i class="icon ion-chatbox"></i>&nbsp; Chat</a>
                            @endif
                    </div>
                </div>
                <div class="bg-white pt-5 pb-5 pl-3 pr-3 mb-5 rounded shadow">
                    <ul class="nav nav-pills flex-column">
                        <li class="nav-item"><a class="nav-link text-dark" href="{{ route("account.show",[$user->id]) }}">Profile</a></li>
                        <li class="nav-item"><a class="nav-link active " href="{{ route("account.gigs",[$user->id]) }}">Gigs</a></li>
                        <li class="nav-item"><a class="nav-link text-dark" href="{{ route("account.reviews",[$user->id]) }}">Reviews</a></li>
                        {{-- <li class="nav-item"><a class="nav-link text-dark" href="#">Setting</a></li> --}}
                    </ul>
                </div>
            </div>
            <div class="col col-lg-8">
                <div class="row mb-5">
                    <div class="col col-lg-4 col-md-12 mb-3 col-sm-6">
                        <div class="shadow pt-3 pb-3 rounded text-center bg-brown shadow-sm">
                            <p class="text-white mb-2">Total Items</p>
                            <h1 class="text-white">{{ count($user->gigs) }}</h1>
                        </div>
                    </div>
                    <div class="col col-lg-4 col-md-12 mb-3 col-sm-6">
                        <div class="shadow pt-3 pb-3 rounded text-center shadow-sm bg-success">
                            <p class="text-white mb-2">Total Sales</p>
                            <h1 class="text-white">{{ count($orders) }}</h1>
                        </div>
                    </div>
                    <div class="col col-lg-4 col-12">
                        <div class="shadow pt-3 pb-3 rounded text-center shadow-sm main-bg-color">
                            <p class="text-white mb-2">Author Rating</p>
                            <h1 class="text-white"><i class="fa fa-star rating"></i>&nbsp;{{ count($orders->where("status","Completed")) }}</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-12 col-12">
                        <div class="section-header-row pt-3 pb-3 mt-5">
                            <h4 class="mb-0 font-weight-normal">{{ count($user->gigs) }} Gigs From&nbsp;<span class="mb-0 font-weight-bold">{{ $user->name }}</span></h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @for ($i = 0; $i < 3; $i++)
                        @php
                            if(count($user->gigs) == ($i) ){
                                break;
                            }
                        @endphp
                         <div class="col col-lg-6 mb-3 col-md-6 col-12">
                            <div class="row">
                                <a href="{{ route("gig.show",[$user->gigs[$i]->id]) }}" class="text-black">
                                    <div class="col">
                                        <img class="img-fluid d-block" src="{{ asset("storage/gig image/".$user->gigs[$i]->image) }}">
                                        <div class="bg-white p-3 custom-box-shadow">
                                            <h6 class="mb-2 text-black">{{ $user->gigs[$i]->title }}</h6>
                                            <div class="d-flex align-items-center">
                                                <div class="d-flex align-items-center d-inline-block mb-2 user-avatar" style="flex: 1;">
                                                    <div class="float-left">
                                                        <img src="{{ asset("storage/profile images/".$user->seller->profile_image) }}" width="25" height="25">
                                                    </div>
                                                    <div class="float-left pl-2">
                                                        <p class="text-14">{{ auth()->user()->name }}&nbsp;</p>
                                                    </div>
                                                </div>
                                                <div class="mb-2" style="flex: 1;">
                                                    <p class="text-info text-14"><i class="icon-layers"></i>&nbsp;Web Design</p>
                                                </div>
                                            </div>
                                            <p class="mt-0 mb-2 text-14">{{ substr($user->gigs[$i]->description,0,60) }}..</p>
                                            <hr class="mt-0 mb-2">
                                            <div class="d-flex justify-content-between footer-section">
                                                <span class="badge badge-primary price-badge-light mr-3">{{ $user->gigs[$i]->basicPlan->price }} - {{ $user->gigs[$i]->premiumPlan->price }}</span>
                                                <span class="badge badge-primary price-badge-pink-light mr-3"><i class="icon-heart"></i>&nbsp; 90</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endfor
                    {{-- <div class="col col-lg-6 mb-3 col-md-6 col-12">
                        <div class="row">
                            <div class="col"><img class="img-fluid d-block" src="assets/img/image09.jpg">
                                <div class="bg-white p-3 custom-box-shadow">
                                    <h6 class="mb-1">One Page Wordpress Theme</h6>
                                    <div>
                                        <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                            <div class="float-left"><img src="assets/img/man.png" width="25" height="25"></div>
                                            <div class="float-left pl-2">
                                                <p class="font-weight-bold">John Timothy</p>
                                            </div>
                                        </div>
                                        <div>
                                            <p class="text-info"><i class="icon-layers"></i>&nbsp;Wordpress</p>
                                        </div>
                                    </div>
                                    <p class="mt-0 mb-0">Nunc placerat mi id nisi interdum mollis.&nbsp;</p>
                                    <hr class="mt-0 mb-1">
                                    <div class="footer-section"><span class="badge badge-primary price-badge-thick mr-3">$10 - $15</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-heart"></i>&nbsp; 90</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-lg-6 mb-3 col-md-6 col-12">
                        <div class="row">
                            <div class="col"><img class="img-fluid d-block" src="assets/img/image09.jpg">
                                <div class="bg-white p-3 custom-box-shadow">
                                    <h6 class="mb-1">One Page Wordpress Theme</h6>
                                    <div>
                                        <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                            <div class="float-left"><img src="assets/img/man.png" width="25" height="25"></div>
                                            <div class="float-left pl-2">
                                                <p class="font-weight-bold">John Timothy</p>
                                            </div>
                                        </div>
                                        <div>
                                            <p class="text-info"><i class="icon-layers"></i>&nbsp;Wordpress</p>
                                        </div>
                                    </div>
                                    <p class="mt-0 mb-0">Nunc placerat mi id nisi interdum mollis.&nbsp;</p>
                                    <hr class="mt-0 mb-1">
                                    <div class="footer-section"><span class="badge badge-primary price-badge-thick mr-3">$10 - $15</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-heart"></i>&nbsp; 90</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-lg-6 mb-3 col-md-6 col-12">
                        <div class="row">
                            <div class="col"><img class="img-fluid d-block" src="assets/img/image09.jpg">
                                <div class="bg-white p-3 custom-box-shadow">
                                    <h6 class="mb-1">One Page Wordpress Theme</h6>
                                    <div>
                                        <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                            <div class="float-left"><img src="assets/img/man.png" width="25" height="25"></div>
                                            <div class="float-left pl-2">
                                                <p class="font-weight-bold">John Timothy</p>
                                            </div>
                                        </div>
                                        <div>
                                            <p class="text-info"><i class="icon-layers"></i>&nbsp;Wordpress</p>
                                        </div>
                                    </div>
                                    <p class="mt-0 mb-0">Nunc placerat mi id nisi interdum mollis.&nbsp;</p>
                                    <hr class="mt-0 mb-1">
                                    <div class="footer-section"><span class="badge badge-primary price-badge-thick mr-3">$10 - $15</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-heart"></i>&nbsp; 90</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-lg-6 mb-3 col-md-6 col-12">
                        <div class="row">
                            <div class="col"><img class="img-fluid d-block" src="assets/img/image09.jpg">
                                <div class="bg-white p-3 custom-box-shadow">
                                    <h6 class="mb-1">One Page Wordpress Theme</h6>
                                    <div>
                                        <div class="d-flex align-items-center d-inline-block mb-2 user-avatar">
                                            <div class="float-left"><img src="assets/img/man.png" width="25" height="25"></div>
                                            <div class="float-left pl-2">
                                                <p class="font-weight-bold">John Timothy</p>
                                            </div>
                                        </div>
                                        <div>
                                            <p class="text-info"><i class="icon-layers"></i>&nbsp;Wordpress</p>
                                        </div>
                                    </div>
                                    <p class="mt-0 mb-0">Nunc placerat mi id nisi interdum mollis.&nbsp;</p>
                                    <hr class="mt-0 mb-1">
                                    <div class="footer-section"><span class="badge badge-primary price-badge-thick mr-3">$10 - $15</span><span class="badge badge-primary price-badge-light mr-3"><i class="icon-heart"></i>&nbsp; 90</span></div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
