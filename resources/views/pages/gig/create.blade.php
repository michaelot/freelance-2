@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Create&nbsp;</h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Setting</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"createGig"])
<section id="main-check-out" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 col-12">
                @include('layouts/alert')
                <form enctype="multipart/form-data" action="{{ route("gig.store") }}" method="post">
                    @csrf
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Overview</h3>
                        <div class="mt-4">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2">Gig Title&nbsp;<span class="span-required">*</span></label>
                                        <input value="{{ old("title") }}" required name="title" class="form-control main-input-form" type="text" placeholder="e.g &quot;I make Beautiful UI/UX &quot;">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-6">
                                        <label class="font-weight-bold mb-2 mt-2">Gig Category&nbsp;<span class="span-required">*</span></label>
                                        <select required name="gig_category_id" class="form-control main-input-form">
                                            @foreach ($categories as $item)
                                                <option  value="{{ $item->id }}" >{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col align-self-end col-lg-6">
                                        <select required name="gig_sub_category_id" class="form-control main-input-form">
                                            @foreach ($subCategories as $item)
                                                <option  value="{{ $item->id }}" >{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Skills &nbsp;<span class="span-required">*</span></label>
                                        <select required class="form-control main-input-form d-block" multiple="multiple" name="skills[]">
                                            @foreach ($skills as $item)
                                                <option  value="{{ $item->id }}" >{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Search Tags &nbsp;<span class="span-required">*</span></label>

                                        <select class="form-control main-input-form d-block" multiple="multiple" name="search_tags[]">
                                            @foreach ($skills as $item)
                                                <option  value="{{ $item->id }}" >{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3 class="mb-4">Pricing</h3>
                        <div role="tablist" id="product-sizes-accordian" class="mb-4">

                            {{-- Basic Plan  --}}
                            <div class="card">
                                <div class="card-header" role="tab">
                                    <h6 class="mb-0">
                                        <a data-toggle="collapse" aria-expanded="false" aria-controls="product-sizes-accordian .item-1" href="div#product-sizes-accordian .item-1"><i class="fa fa-dot-circle-o"></i>&nbsp; Basic Plan&nbsp;<span class="span-required">*</span></a>
                                    </h6>
                                </div>
                                <div class="collapse item-1" role="tabpanel" data-parent="#product-sizes-accordian">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Plan Title&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ old("basic_plan_title") }}" required name="basic_plan_title" class="form-control main-input-form" type="text" placeholder="Plan Title">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Description&nbsp;<span class="span-required">*</span></label>
                                                    <textarea required name="basic_plan_description" class="form-control main-input-textarea" placeholder="Describe Plan">{{ old("basic_plan_description") }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-6">
                                                    <label class="font-weight-bold mb-2 mt-2">Duration&nbsp;<span class="span-required">*</span></label>
                                                    <input required value="{{ old("basic_plan_duration") }}" required name="basic_plan_duration" class="form-control main-input-form" type="text" placeholder="">
                                                </div>
                                                <div class="col align-self-end col-lg-6">
                                                    <select required name="basic_plan_duration_type" class="form-control main-input-form">
                                                        <option value="hour(s)">Hour(s)</option>
                                                        <option value="day(s)" selected="">Day(s)</option>
                                                        <option value="week(s)">week(s)</option>
                                                        <option value="month(s)">month(s)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Revisions&nbsp;<span class="span-required">*</span></label>
                                                    <input required value="{{ old("basic_plan_revisions") }}" required name="basic_plan_revisions" class="form-control main-input-form" type="text" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Price&nbsp;<span class="span-required">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text border-0 form-control main-input-form">$</span>
                                                        </div>
                                                        <input value="{{ old("basic_plan_price") }}" required name="basic_plan_price" class="form-control main-input-form" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-3">Extras&nbsp;<span class="span-required">*</span></label>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Fast Delivery" name="basic_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-1">
                                                        <label class="form-check-label ml-4" for="formCheck-1">Fast Delivery</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Additional Revision" name="basic_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-2">
                                                        <label class="form-check-label ml-4" for="formCheck-2">Additional Revision</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Stock Photos" name="basic_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-3">
                                                        <label class="form-check-label ml-4" for="formCheck-3">Stock Photos</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Mockup" name="basic_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-4">
                                                        <label class="form-check-label ml-4" for="formCheck-4">Mockup</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="basic-plan-Extra"></div>
                                        <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-basic-plan-extra" type="button">Add Gig Extra</button>
                                    </div>
                                </div>
                            </div>
                            {{--END OF Basic Plan  --}}

                            {{-- Standard Plan  --}}
                            <div class="card">
                                <div class="card-header" role="tab">
                                    <h6 class="mb-0"><a data-toggle="collapse" aria-expanded="false" aria-controls="product-sizes-accordian .item-2" href="div#product-sizes-accordian .item-2"><i class="fa fa-dot-circle-o"></i>&nbsp; Standard Plan&nbsp;<span class="span-required">*</span></a></h6>
                                </div>
                                <div class="collapse item-2" role="tabpanel" data-parent="#product-sizes-accordian">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Plan Title&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ old("standard_plan_title") }}" required name="standard_plan_title" class="form-control main-input-form" type="text" placeholder="Plan Title">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Description&nbsp;<span class="span-required">*</span></label>
                                                    <textarea required name="standard_plan_description" class="form-control main-input-textarea" placeholder="Describe Plan">{{ old("standard_plan_description") }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-6">
                                                    <label class="font-weight-bold mb-2 mt-2">Duration&nbsp;<span class="span-required">*</span></label>
                                                    <input required value="{{ old("standard_plan_duration") }}" name="standard_plan_duration" class="form-control main-input-form" type="text">
                                                </div>
                                                <div class="col align-self-end col-lg-6">
                                                    <select required name="standard_plan_duration_type" class="form-control main-input-form">
                                                        <option value="hour(s)">Hour(s)</option>
                                                        <option value="day(s)" selected="">Day(s)</option>
                                                        <option value="week(s)">week(s)</option>
                                                        <option value="month(s)">month(s)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Revisions&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ old("standard_plan_revisions") }}" required name="standard_plan_revisions" class="form-control main-input-form" type="text" placeholder="Revisions">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12"><label class="font-weight-bold mb-2">Price&nbsp;<span class="span-required">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text border-0 form-control main-input-form">$</span>
                                                        </div>
                                                        <input value="{{ old("standard_plan_price") }}" required name="standard_plan_price" class="form-control main-input-form" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-3">Extras&nbsp;<span class="span-required">*</span></label>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Fast Delivery" name="standard_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-5">
                                                        <label class="form-check-label ml-4" for="formCheck-5">Fast Delivery</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Additional Revision" name="standard_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-6">
                                                        <label class="form-check-label ml-4" for="formCheck-6">Additional Revision</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Stock Photos" name="standard_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-7">
                                                        <label class="form-check-label ml-4" for="formCheck-7">Stock Photos</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Mockup" name="standard_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-8">
                                                        <label class="form-check-label ml-4" for="formCheck-8">Mockup</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="standard-plan-Extra"></div>
                                        <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-standard-plan-extra" type="button">Add Gig Extra</button>
                                    </div>
                                </div>
                            </div>
                            {{-- END OF STANDARD PLAN --}}

                            {{-- Premium Plan  --}}
                            <div class="card">
                                <div class="card-header" role="tab">
                                    <h6 class="mb-0">
                                        <a data-toggle="collapse" aria-expanded="false" aria-controls="product-sizes-accordian .item-3" href="div#product-sizes-accordian .item-3"><i class="fa fa-dot-circle-o"></i>&nbsp; Premium Plan&nbsp;<span class="span-required">*</span></a>
                                    </h6>
                                </div>
                                <div class="collapse item-3" role="tabpanel" data-parent="#product-sizes-accordian">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Plan Title&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ old("premium_plan_title") }}" required name="premium_plan_title" class="form-control main-input-form" type="text" placeholder="Plan Title">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Description&nbsp;<span class="span-required">*</span></label>
                                                    <textarea required name="premium_plan_description" class="form-control main-input-textarea" placeholder="Describe Plan">{{ old("premium_plan_description") }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-6">
                                                    <label class="font-weight-bold mb-2 mt-2">Duration&nbsp;<span class="span-required">*</span></label>
                                                    <input required value="{{ old("premium_plan_duration") }}" name="premium_plan_duration" class="form-control main-input-form" type="text" placeholder="">
                                                </div>
                                                <div class="col align-self-end col-lg-6">
                                                    <select required name="premium_plan_duration_type" class="form-control main-input-form">
                                                            <option value="hour(s)">Hour(s)</option>
                                                            <option value="day(s)" selected="">Day(s)</option>
                                                            <option value="week(s)">week(s)</option>
                                                            <option value="month(s)">month(s)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Revisions&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ old("premium_plan_revisions") }}" required name="premium_plan_revisions" class="form-control main-input-form" type="text" placeholder="Revisions">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12"><label class="font-weight-bold mb-2">Price&nbsp;<span class="span-required">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text border-0 form-control main-input-form">$</span>
                                                        </div>
                                                        <input value="{{ old("premium_plan_price") }}" required name="premium_plan_price" class="form-control main-input-form" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-3">Extras&nbsp;<span class="span-required">*</span></label>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Fast Delivery" name="premium_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-9">
                                                        <label class="form-check-label ml-4" for="formCheck-9">Fast Delivery</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Additional Revision" name="premium_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-10">
                                                        <label class="form-check-label ml-4" for="formCheck-10">Additional Revision</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Stock Photos" name="premium_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-11">
                                                        <label class="form-check-label ml-4" for="formCheck-11">Stock Photos</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Mockup" name="premium_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-12">
                                                        <label class="form-check-label ml-4" for="formCheck-12">Mockup</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="premium-plan-Extra"></div>
                                        <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-premium-plan-extra" type="button">Add Gig Extra</button>
                                    </div>
                                </div>
                            </div>
                            {{-- END OF PREMIUM PLAN --}}
                        </div>
                    </div>
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Description</h3>
                        <div id="gig-description" class="mt-4">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2">Description&nbsp;<span class="span-required">*</span></label>
                                        <textarea  required name="description" class="form-control main-input-textarea" placeholder="Gig description">{{ old("description") }}</textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-faq" type="button">Add FAQ</button>
                    </div>
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Tell your buyer what you need to get started</h3>
                        <div id="gig-requirement" class="mt-4">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2">Requirement&nbsp;<span class="span-required">*</span></label>
                                        <textarea  required name="requirement[]" class="form-control main-input-textarea" placeholder="Gig Requirement">{{ old("description") }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-6">
                                        <label class="font-weight-bold mb-2">Answer Type&nbsp;<span class="span-required">*</span></label>
                                        <select class="form-control main-input-form d-block" name="answer_type[]">
                                            <option value="Text">Text</option>
                                            <option value="File">File</option>
                                        </select>
                                    </div>
                                    <div class="col col-lg-6">
                                        <label class="font-weight-bold mb-2">Compulsory&nbsp;<span class="span-required">*</span></label>
                                        <select class="form-control main-input-form d-block" name="compulsory[]">
                                            <option value="No">No</option>
                                            <option value="Yes">Yes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-requirement" type="button">Add Requirement</button>
                    </div>
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Header Image</h3>
                        <p class="mb-4">Choose a high quality image or video</p>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-6">
                                    <input required name="image" type="file" class="form-control border-0">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-6">
                                    <label class="font-weight-bold mb-2">Additional Images&nbsp;<span class="span-required">*</span></label>
                                    <input name="gig_images[]" type="file" class="form-control border-0" multiple="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-6">
                                    <label class="font-weight-bold mb-2">Additional Video&nbsp;<span class="span-required">*</span></label>
                                    <input name="video" type="file" class="form-control border-0" multiple="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-5 mb-5">
                        <div class="col text-center">
                            <button type="submit" class="btn btn-primary text-white fr-cl-bcs btn-lg" role="button">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col col-lg-4 col-12">
                <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                    <h3 class="mb-4">Gig Rules</h3>
                    <hr class="mb-4">
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Gig Name</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Gig Skills</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Search Tag</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                </div>
                <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                    <h3 class="mb-4">Pricing</h3>
                    <hr class="mb-4">
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Plan</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Extras</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                </div>
                <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                    <h3 class="mb-4">Quick Upload rule</h3>
                    <hr class="mb-4">
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Image Upload</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Video upload</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    //Add Basic Plan Extra
$("#add-basic-plan-extra").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12"><label class="font-weight-bold mb-2">Gig Title <span class="span-required">*</span></label><input name="basic_plan_extra[]" type="text" class="form-control main-input-form" placeholder="Gig Extra" /></div>
    </div>
</div>`

   $("#basic-plan-Extra").append($(formCode).fadeIn(5000)) ;
})

//Add Standard Plan Extra
$("#add-standard-plan-extra").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12"><label class="font-weight-bold mb-2">Gig Title <span class="span-required">*</span></label><input name="standard_plan_extra[]" type="text" class="form-control main-input-form" placeholder="Gig Extra" /></div>
    </div>
</div>`

   $("#standard-plan-Extra").append($(formCode).fadeIn(5000)) ;
})


//Add Premium Plan Extra
$("#add-premium-plan-extra").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12"><label class="font-weight-bold mb-2">Gig Title <span class="span-required">*</span></label><input name="premium_plan_extra[]" type="text" class="form-control main-input-form" placeholder="Gig Extra" /></div>
    </div>
</div>`

   $("#premium-plan-Extra").append($(formCode).fadeIn(5000)) ;
})

// Add FAQ

$("#add-faq").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12">
        <label class="font-weight-bold mb-2">FAQ Question <span class="span-required">*</span></label>
        <input name="faq_title[]" type="text" class="form-control main-input-form" placeholder="FAQ question" />
</div>
    </div>
</div>

<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12">
        <label class="font-weight-bold mb-2">Answer <span class="span-required">*</span></label>
        <textarea name="faq_answer[]" class="form-control main-input-textarea" placeholder="Answer"></textarea></div>
    </div>
</div>
`

   $("#gig-description").append($(formCode).fadeIn(5000)) ;
})

$("#add-requirement").on("click",function(){
    var formCode = `
    <div class="form-group">
        <div class="form-row">
            <div class="col col-lg-12">
                <label class="font-weight-bold mb-2">Requirement&nbsp;<span class="span-required">*</span></label>
                <textarea  required name="requirement[]" class="form-control main-input-textarea" placeholder="Gig Requirement">{{ old("description") }}</textarea>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="form-row">
            <div class="col col-lg-6">
                <label class="font-weight-bold mb-2">Answer Type&nbsp;<span class="span-required">*</span></label>
                <select class="form-control main-input-form d-block" name="answer_type[]">
                    <option value="Text">Text</option>
                    <option value="File">File</option>
                </select>
            </div>
            <div class="col col-lg-6">
                <label class="font-weight-bold mb-2">Compulsory&nbsp;<span class="span-required">*</span></label>
                <select class="form-control main-input-form d-block" name="compulsory[]">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                </select>
            </div>
        </div>
    </div>
`

   $("#gig-requirement").append($(formCode).fadeIn(5000)) ;
})

</script>
<script>
    $('select').select2();
</script>
@endsection
