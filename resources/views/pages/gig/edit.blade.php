@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Edit&nbsp;</h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Setting</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"manageGig"])
<section id="main-check-out" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 col-12">
                @include('layouts/alert')
                <form enctype="multipart/form-data" action="{{ route("gig.update",['id'=>$gig->id]) }}" method="post">
                    @csrf
                    @method("put")
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Overview</h3>
                        <div class="mt-4">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2">Gig Title&nbsp;<span class="span-required">*</span></label>
                                        <input value="{{ $gig->title }}" required name="title" class="form-control main-input-form" type="text" placeholder="e.g &quot;I make Beautiful UI/UX &quot;">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-6">
                                        <label class="font-weight-bold mb-2 mt-2">Gig Category&nbsp;<span class="span-required">*</span></label>
                                        <select required name="gig_category_id" class="form-control main-input-form">
                                            @foreach ($categories as $item)
                                                <option @if($gig->gig_category_id == $item->id ) selected @endif value="{{ $item->id }}" >{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col align-self-end col-lg-6">
                                        <select required name="gig_sub_category_id" class="form-control main-input-form">
                                            @foreach ($subCategories as $item)
                                                <option @if($gig->gig_sub_category_id == $item->id ) selected @endif value="{{ $item->id }}" >{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Skills &nbsp;<span class="span-required">*</span></label>

                                        <select required class="form-control main-input-form d-block" multiple="multiple" name="skills[]">
                                            @foreach ($skills as $item)
                                                <option @foreach($gig->skills as $skill) @if($skill->skill_id == $item->id ) selected @endif @endforeach value="{{ $item->id }}" >{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2 mt-2">Search Tags &nbsp;<span class="span-required">*</span></label>
                                        @php
                                            $search_tags = explode(",",$gig->search_tags);
                                        @endphp
                                        <select class="form-control main-input-form d-block" multiple="multiple" name="search_tags[]">
                                            <option @foreach($search_tags as $tag) @if($tag == "HTML") selected @endif @endforeach value="HTML">HTML</option>
                                            <option @foreach($search_tags as $tag) @if($tag == "Php") selected @endif @endforeach value="Php">Php</option>
                                            <option @foreach($search_tags as $tag) @if($tag == "CSS") selected @endif @endforeach value="CSS">CSS</option>
                                            <option @foreach($search_tags as $tag) @if($tag == "Photoshop") selected @endif @endforeach value="Photoshop">Photoshop</option>
                                            <option @foreach($search_tags as $tag) @if($tag == "illustrator") selected @endif @endforeach value="illustrator">illustrator</option>
                                            <option @foreach($search_tags as $tag) @if($tag == "Corel Draw") selected @endif @endforeach value="Corel Draw">Corel Draw</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3 class="mb-4">Pricing</h3>
                        <div role="tablist" id="product-sizes-accordian" class="mb-4">

                            {{-- Basic Plan  --}}
                            <div class="card">
                                <div class="card-header" role="tab">
                                    <h6 class="mb-0">
                                        <a data-toggle="collapse" aria-expanded="false" aria-controls="product-sizes-accordian .item-1" href="div#product-sizes-accordian .item-1"><i class="fa fa-dot-circle-o"></i>&nbsp; Basic Plan&nbsp;<span class="span-required">*</span></a>
                                    </h6>
                                </div>
                                <div class="collapse item-1" role="tabpanel" data-parent="#product-sizes-accordian">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Plan Title&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ $gig->basicPlan->plan_title }}" required name="basic_plan_title" class="form-control main-input-form" type="text" placeholder="Plan Title">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Description&nbsp;<span class="span-required">*</span></label>
                                                    <textarea required name="basic_plan_description" class="form-control main-input-textarea" placeholder="Describe Plan">{{ $gig->basicPlan->description }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-6">
                                                    <label class="font-weight-bold mb-2 mt-2">Duration&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ $gig->basicPlan->duration }}" required name="basic_plan_duration" class="form-control main-input-form" type="text" placeholder="2">
                                                </div>
                                                <div class="col align-self-end col-lg-6">
                                                    <select required name="basic_plan_duration_type" class="form-control main-input-form">
                                                        <option @if($gig->basicPlan->duration_type == "hour(s)") selected @endif value="hour(s)">Hour(s)</option>
                                                        <option @if($gig->basicPlan->duration_type == "day(s)") selected @endif value="day(s)">Day(s)</option>
                                                        <option @if($gig->basicPlan->duration_type == "week(s)") selected @endif value="week(s)">week(s)</option>
                                                        <option @if($gig->basicPlan->duration_type == "month(s)") selected @endif value="month(s)">month(s)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Revisions&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ $gig->basicPlan->revisions }}" required name="basic_plan_revisions" class="form-control main-input-form" type="text" placeholder="Revisions">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Price&nbsp;<span class="span-required">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text border-0 form-control main-input-form">$</span>
                                                        </div>
                                                        <input value="{{ $gig->basicPlan->price }}" required name="basic_plan_price" class="form-control main-input-form" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-3">Extras&nbsp;<span class="span-required">*</span></label>
                                                    @foreach ($gig->basicPlan->extras as $extra)
                                                        <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                            <input checked value="{{ $extra->name }}" name="basic_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="{{ $extra->id }}">
                                                            <label class="form-check-label ml-4" for="{{ $extra->id }}">{{ $extra->name }}</label>
                                                        </div>
                                                    @endforeach
                                                    {{-- <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Fast Delivery" name="basic_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-1">
                                                        <label class="form-check-label ml-4" for="formCheck-1">Fast Delivery</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Additional Revision" name="basic_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-1">
                                                        <label class="form-check-label ml-4" for="formCheck-1">Additional Revision</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Stock Photos" name="basic_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-1">
                                                        <label class="form-check-label ml-4" for="formCheck-1">Stock Photos</label>
                                                    </div>
                                                    <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                        <input value="Mockup" name="basic_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="formCheck-1">
                                                        <label class="form-check-label ml-4" for="formCheck-1">Mockup</label>
                                                    </div> --}}
                                                </div>
                                            </div>
                                        </div>
                                        <div id="basic-plan-Extra"></div>
                                        <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-basic-plan-extra" type="button">Add Gig Extra</button>
                                    </div>
                                </div>
                            </div>
                            {{--END OF Basic Plan  --}}

                            {{-- Standard Plan  --}}
                            <div class="card">
                                <div class="card-header" role="tab">
                                    <h6 class="mb-0"><a data-toggle="collapse" aria-expanded="false" aria-controls="product-sizes-accordian .item-2" href="div#product-sizes-accordian .item-2"><i class="fa fa-dot-circle-o"></i>&nbsp; Standard Plan&nbsp; <span class="span-required">*</span></a></h6>
                                </div>
                                <div class="collapse item-2" role="tabpanel" data-parent="#product-sizes-accordian">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Plan Title&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ $gig->standardPlan->plan_title }}" required name="standard_plan_title" class="form-control main-input-form" type="text" placeholder="Plan Title">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Description&nbsp;<span class="span-required">*</span></label>
                                                    <textarea required name="standard_plan_description" class="form-control main-input-textarea" placeholder="Describe Plan">{{ $gig->standardPlan->description }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-6">
                                                    <label class="font-weight-bold mb-2 mt-2">Duration&nbsp;<span class="span-required">*</span></label>
                                                    <input required value="{{ $gig->standardPlan->duration }}" name="standard_plan_duration" class="form-control main-input-form" type="text" placeholder="2">
                                                </div>
                                                <div class="col align-self-end col-lg-6">
                                                    <select required name="standard_plan_duration_type" class="form-control main-input-form">
                                                        <option @if($gig->standardPlan->duration_type == "hour(s)") selected @endif value="hour(s)">Hour(s)</option>
                                                        <option @if($gig->standardPlan->duration_type == "day(s)") selected @endif value="day(s)">Day(s)</option>
                                                        <option @if($gig->standardPlan->duration_type == "week(s)") selected @endif value="week(s)">week(s)</option>
                                                        <option @if($gig->standardPlan->duration_type == "month(s)") selected @endif value="month(s)">month(s)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Revisions&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ $gig->standardPlan->revisions }}" required name="standard_plan_revisions" class="form-control main-input-form" type="text" placeholder="Revisions">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12"><label class="font-weight-bold mb-2">Price&nbsp;<span class="span-required">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text border-0 form-control main-input-form">$</span>
                                                        </div>
                                                        <input value="{{ $gig->standardPlan->price }}" required name="standard_plan_price" class="form-control main-input-form" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-3">Extras&nbsp;<span class="span-required">*</span></label>
                                                    @foreach ($gig->standardPlan->extras as $extra)
                                                        <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                            <input checked value="{{ $extra->name }}" name="standard_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="{{ $extra->id }}">
                                                            <label class="form-check-label ml-4" for="{{ $extra->id }}">{{ $extra->name }}</label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div id="standard-plan-Extra"></div>
                                        <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-standard-plan-extra" type="button">Add Gig Extra</button>
                                    </div>
                                </div>
                            </div>
                            {{-- END OF STANDARD PLAN --}}

                            {{-- Premium Plan  --}}
                            <div class="card">
                                <div class="card-header" role="tab">
                                    <h6 class="mb-0">
                                        <a data-toggle="collapse" aria-expanded="false" aria-controls="product-sizes-accordian .item-3" href="div#product-sizes-accordian .item-3"><i class="fa fa-dot-circle-o"></i>&nbsp; Premium Plan&nbsp; <span class="span-required">*</span></a>
                                    </h6>
                                </div>
                                <div class="collapse item-3" role="tabpanel" data-parent="#product-sizes-accordian">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Plan Title&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ $gig->premiumPlan->plan_title }}" required name="premium_plan_title" class="form-control main-input-form" type="text" placeholder="Plan Title">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Description&nbsp;<span class="span-required">*</span></label>
                                                    <textarea required name="premium_plan_description" class="form-control main-input-textarea" placeholder="Describe Plan">{{ $gig->premiumPlan->description }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-6">
                                                    <label class="font-weight-bold mb-2 mt-2">Duration&nbsp;<span class="span-required">*</span></label>
                                                    <input required value="{{ $gig->premiumPlan->duration }}" name="premium_plan_duration" class="form-control main-input-form" type="text" placeholder="2">
                                                </div>
                                                <div class="col align-self-end col-lg-6">
                                                    <select required name="premium_plan_duration_type" class="form-control main-input-form">
                                                        <option @if($gig->premiumPlan->duration_type == "hour(s)") selected @endif value="hour(s)">Hour(s)</option>
                                                        <option @if($gig->premiumPlan->duration_type == "day(s)") selected @endif value="day(s)">Day(s)</option>
                                                        <option @if($gig->premiumPlan->duration_type == "week(s)") selected @endif value="week(s)">week(s)</option>
                                                        <option @if($gig->premiumPlan->duration_type == "month(s)") selected @endif value="month(s)">month(s)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-2">Revisions&nbsp;<span class="span-required">*</span></label>
                                                    <input value="{{ $gig->premiumPlan->revisions }}" required name="premium_plan_revisions" class="form-control main-input-form" type="text" placeholder="Revisions">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-row">
                                                <div class="col col-lg-12"><label class="font-weight-bold mb-2">Price&nbsp;<span class="span-required">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text border-0 form-control main-input-form">$</span>
                                                        </div>
                                                        <input value="{{ $gig->premiumPlan->price }}" required name="premium_plan_price" class="form-control main-input-form" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <label class="font-weight-bold mb-3">Extras&nbsp;<span class="span-required">*</span></label>
                                                    @foreach ($gig->premiumPlan->extras as $extra)
                                                        <div class="form-check pt-3 pb-3 bg-color rounded border-bottom shadow-sm">
                                                            <input checked value="{{ $extra->name }}" name="premium_plan_extra[]" class="form-check-input ml-0" type="checkbox" id="{{ $extra->id }}">
                                                            <label class="form-check-label ml-4" for="{{ $extra->id }}">{{ $extra->name }}</label>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                        <div id="premium-plan-Extra"></div>
                                        <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-premium-plan-extra" type="button">Add Gig Extra</button>
                                    </div>
                                </div>
                            </div>
                            {{-- END OF PREMIUM PLAN --}}
                        </div>
                    </div>
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Description</h3>
                        <div id="gig-description" class="mt-4">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col col-lg-12">
                                        <label class="font-weight-bold mb-2">Description&nbsp;<span class="span-required">*</span></label>
                                        <textarea required name="description" class="form-control main-input-textarea" placeholder="Gig description">{{ $gig->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                            @foreach($gig->faqs as $faq)
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2">FAQ Question <span class="span-required">*</span></label>
                                            <input value="{{ $faq->title }}" name="faq_title[]" type="text" class="form-control main-input-form" placeholder="FAQ question" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2">Answer <span class="span-required">*</span></label>
                                            <textarea name="faq_answer[]" class="form-control main-input-textarea" placeholder="Answer">{{ $faq->answer }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-faq" type="button">Add FAQ</button>
                    </div>
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Tell your buyer what you need to get started</h3>
                        <div id="gig-requirement" class="mt-4">
                            @forelse ($gig->requirements as $requirement)
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2">Requirement&nbsp;<span class="span-required">*</span></label>
                                            <textarea  required  name="requirement[]" class="form-control main-input-textarea" placeholder="Gig Requirement">{{ $requirement->title }}</textarea>
                                            <input type="hidden" name="id[]" value="{{ $requirement->id }}">
                                            <input type="hidden" name="new[]" value="No">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-6">
                                            <label class="font-weight-bold mb-2">Answer Type&nbsp;<span class="span-required">*</span></label>
                                            <select class="form-control main-input-form d-block" name="answer_type[]">
                                                <option @if($requirement->answer_type == "Text") selected @endif value="Text">Text</option>
                                                <option @if($requirement->answer_type == "File") selected @endif value="File">File</option>
                                            </select>
                                        </div>
                                        <div class="col col-lg-6">
                                            <label class="font-weight-bold mb-2">Compulsory&nbsp;<span class="span-required">*</span></label>
                                            <select class="form-control main-input-form d-block" name="compulsory[]">
                                                <option @if($requirement->compulsory == "No") selected @endif value="No">No</option>
                                                <option @if($requirement->compulsory == "Yes") selected @endif value="Yes">Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <p id="no-requirement">No Requirement </p>
                            @endforelse


                        </div>
                        <button class="btn btn-primary fr-cl-bcs mt-3 font-weight-normal" id="add-requirement" type="button">Add Requirement</button>
                    </div>
                    <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                        <h3>Header Image</h3>
                        <p class="mb-4">Choose a high quality image or video</p>
                        {{-- <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-6">
                                    <input required name="image" type="file" class="form-control border-0">
                                </div>
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-12">
                                    <div class="card border-0" style="background: #F4F5F8;">
                                        <div class="card-header p-0">
                                            <img class="img-fluid" src="{{ asset("storage/gig image/".$gig->image) }}">
                                        </div>
                                        <div class="card-body pt-2 pb-2 pl-3 pr-3">
                                            <input name="image" type="file" class="form-control border-0 main-input-form shadow-0 pt-2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <label class="col-form-label font-weight-bold mb-2">Additional Images&nbsp;<span class="span-required">*</span></label>
                                    </div>
                                </div>
                            <div class="form-row">
                                @foreach ($gig->images as $image)
                                    <div class="col col-lg-3 mb-2">
                                        <div class="card border-0 shadow-sm" style="background: #F4F5F8;">
                                            <div class="card-header p-0">
                                                <img class="img-fluid" src="{{ asset("storage/gig image/".$image->image) }}">
                                            </div>
                                            <div class="card-body d-flex align-items-center justify-content-between pt-1 pb-1 pl-1">
                                                <p>Delete</p>
                                                <input value="{{ $image->id }}" name="delete_images[]" type="checkbox">
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="col col-12 mt-3">
                                    <input name="gig_images[]" type="file" class="form-control border-0 main-input-form pt-2" multiple="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-12">
                                    <label class="font-weight-bold mb-2">Additional Video&nbsp;<span class="span-required">*</span></label>
                                    {{-- <input name="video" type="file" class="form-control border-0" multiple=""> --}}
                                    <div class="card border-0" style="background: #F4F5F8;">
                                        @if($gig->video !="")
                                            <div class="card-header p-0">
                                                    <video controls style="width:100%;">
                                                        <source src="{{ asset("storage/gig video/".$gig->video) }}" type="video/mp4">
                                                    </video>
                                                {{-- <img class="img-fluid" src="{{ asset("storage/gig video/".$gig->video) }}"> --}}
                                            </div>
                                        @endif
                                        <div class="card-body pt-2 pb-2 pl-1">
                                            <input name="video" type="file" type="file" class="form-control border-0 main-input-form shadow-0 pt-2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-5 mb-5">
                        <div class="col text-center">
                            <button type="submit" class="btn btn-primary text-white fr-cl-bcs btn-lg" role="button">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col col-lg-4 col-12">
                <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                    <h3 class="mb-4">Gig Rules</h3>
                    <hr class="mb-4">
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Gig Name</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Gig Skills</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Search Tag</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                </div>
                <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                    <h3 class="mb-4">Pricing</h3>
                    <hr class="mb-4">
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Plan</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Extras</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                </div>
                <div class="border rounded shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-5">
                    <h3 class="mb-4">Quick Upload rule</h3>
                    <hr class="mb-4">
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Image Upload</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-weight-normal mb-4">Video upload</h5>
                        <p>Nunc placerat mi id nisi interdum mollis. Praesent there pharetra, justo ut sceleris que the mattis interdum</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    //Add Basic Plan Extra
$("#add-basic-plan-extra").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12"><label class="font-weight-bold mb-2">Gig Title <span class="span-required">*</span></label><input name="basic_plan_extra[]" type="text" class="form-control main-input-form" placeholder="Gig Extra" /></div>
    </div>
</div>`

   $("#basic-plan-Extra").append($(formCode).fadeIn(5000)) ;
})

//Add Standard Plan Extra
$("#add-standard-plan-extra").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12"><label class="font-weight-bold mb-2">Gig Title <span class="span-required">*</span></label><input name="standard_plan_extra[]" type="text" class="form-control main-input-form" placeholder="Gig Extra" /></div>
    </div>
</div>`

   $("#standard-plan-Extra").append($(formCode).fadeIn(5000)) ;
})


//Add Premium Plan Extra
$("#add-premium-plan-extra").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12"><label class="font-weight-bold mb-2">Gig Title <span class="span-required">*</span></label><input name="premium_plan_extra[]" type="text" class="form-control main-input-form" placeholder="Gig Extra" /></div>
    </div>
</div>`

   $("#premium-plan-Extra").append($(formCode).fadeIn(5000)) ;
})

// Add FAQ

$("#add-faq").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12">
        <label class="font-weight-bold mb-2">FAQ Question <span class="span-required">*</span></label>
        <input name="faq_title[]" type="text" class="form-control main-input-form" placeholder="FAQ question" />
</div>
    </div>
</div>

<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12">
        <label class="font-weight-bold mb-2">Answer <span class="span-required">*</span></label>
        <textarea name="faq_answer[]" class="form-control main-input-textarea" placeholder="Answer"></textarea></div>
    </div>
</div>
`

   $("#gig-description").append($(formCode).fadeIn(5000)) ;
})

$("#add-requirement").on("click",function(){
    var formCode = `
    <div class="form-group">
        <div class="form-row">
            <div class="col col-lg-12">
                <label class="font-weight-bold mb-2">Requirement&nbsp;<span class="span-required">*</span></label>
                <textarea name="requirement[]" class="form-control main-input-textarea" placeholder="Gig Requirement">{{ old("description") }}</textarea>
                <input type="hidden" name="new[]" value="Yes">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="form-row">
            <div class="col col-lg-6">
                <label class="font-weight-bold mb-2">Answer Type&nbsp;<span class="span-required">*</span></label>
                <select class="form-control main-input-form d-block" name="answer_type[]">
                    <option value="Text">Text</option>
                    <option value="File">File</option>
                </select>
            </div>
            <div class="col col-lg-6">
                <label class="font-weight-bold mb-2">Compulsory&nbsp;<span class="span-required">*</span></label>
                <select class="form-control main-input-form d-block" name="compulsory[]">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                </select>
            </div>
        </div>
    </div>
`

   $("#gig-requirement").append($(formCode).fadeIn(5000)) ;
   $("#no-requirement").hide()
})

</script>
<script>
    $('select').select2();
</script>
@endsection
