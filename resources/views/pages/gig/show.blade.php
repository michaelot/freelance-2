@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h3 class="text-white mb-3 text-left"><strong>Graphic Design and Art</strong><br></h3>
                <h5 class="font-weight-normal text-white-50 mb-3 text-left">{{ $gig->title }}</h5>
                {{-- <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white">Products /&nbsp;</span><span class="text-white-50">SIngle Product</span>&nbsp;&nbsp;</p> --}}
            </div>
        </div>
    </div>
</div>
<section id="single-product-page-content" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 col-12">
                <div class="row mb-5">
                    <div class="col">
                        <div class="shadow-sm">
                            <div class="carousel slide" data-ride="carousel" id="product-images-carousel">
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <img class="w-100 d-block" src="{{ asset("storage/gig image/".$gig->image) }}" alt="Slide Image">
                                    </div>
                                    @foreach ($gig->images as $image)
                                        <div class="carousel-item">
                                            <img class="w-100 d-block" src="{{ asset("storage/gig image/".$image->image) }}" alt="Slide Image">
                                        </div>
                                    @endforeach
                                </div>
                                <div><a class="carousel-control-prev" href="#product-images-carousel" role="button" data-slide="prev"><span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next"
                                        href="#product-images-carousel" role="button" data-slide="next"><span class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a></div>
                                <ol class="carousel-indicators d-none">
                                    <li data-target="#product-images-carousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#product-images-carousel" data-slide-to="1"></li>
                                    <li data-target="#product-images-carousel" data-slide-to="2"></li>
                                </ol>
                            </div>
                            <div id="product-images-carousel-indicator" class="bg-white pt-1 pb-1">
                                <ol class="carousel-indicators">
                                    <li data-target="#product-images-carousel" data-slide-to="0">
                                        <img class="img-fluid d-block" src="{{ asset("storage/gig image/".$gig->image) }}" width="100" height="100">
                                    </li>
                                    @php
                                        $itr = 0
                                    @endphp
                                    @foreach ($gig->images as $image)
                                        <li data-target="#product-images-carousel" data-slide-to="{{ $itr=$itr+1 }}">
                                                <img class="img-fluid d-block" src="{{ asset("storage/gig image/".$image->image) }}" width="100" height="100">
                                            </li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="shadow-sm bg-white">
                            <ul class="nav nav-tabs nav-fill bg-white">
                                <li class="nav-item border-right border-bottom"><a class="nav-link active pt-4 pb-4" role="tab" data-toggle="tab" href="#tab-1">About Gig</a></li>
                                {{-- <li class="nav-item border-right border-bottom"><a class="nav-link pt-4 pb-4" role="tab" data-toggle="tab" href="#tab-2">Comments</a></li> --}}
                                <li class="nav-item border-bottom border-right"><a class="nav-link  pt-4 pb-4" role="tab" data-toggle="tab" href="#tab-2">Reviews</a></li>
                                {{-- <li class="nav-item border-right border-bottom"><a class="nav-link pt-4 pb-4" role="tab" data-toggle="tab" href="#tab-4">Tab</a></li> --}}
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active pl-4 pt-5 pr-4 pb-5" role="tabpanel" id="tab-1">
                                    <div>
                                        <h5 class="mb-2"><strong>{{ $gig->title }}</strong><br></h5>
                                        <div class="d-flex align-items-center flex-wrap mb-3">
                                            <div class="d-flex align-items-center d-inline-block mb-2 user-avatar" style="">
                                                <div class="float-left">
                                                    <img src="{{ asset("storage/profile images/".$gig->user->seller->profile_image) }}" width="25" height="25">
                                                </div>
                                                <div class="float-left pl-2">
                                                    <p class="text-14"><a style="color:#555555" href="{{ route("account.show",[$gig->user->id]) }}">{{ $gig->user->name }}</a>&nbsp;</p>
                                                </div>
                                            </div>
                                            <div class="mb-2" style="">
                                                <p class="text-info text-14"><i class="icon-layers"></i>&nbsp;{{ $gig->category->name }}</p>
                                            </div>
                                        </div>
                                        <p class="mb-4">{{ $gig->description }}<br></p>
                                    </div>
                                    <div>
                                        <h6 class="text-black-50 mb-2"><strong>Requirements :</strong></h6>
                                        @forelse ($gig->requirements as $requirement)
                                            <p class="ml-4">{{ $requirement->title }} @if($requirement->compulsory == "Yes")<span class="text-danger"> (required) </span> @endif</p>
                                        @empty
                                            <li>No Requirement</li>
                                        @endforelse
                                        {{-- <p>i create unique flat logo for your business identity</p>
                                        <p>i will make the logo express your identity and your values</p>
                                        <p>unlimited revisions of for every design.</p> --}}
                                    </div>
                                    <h6 class="text-black-50 mt-3 mb-3"><strong>Skills:</strong><br></h6>
                                    <ul>
                                        @forelse ($gig->skills as $item)
                                            <li>{{ $item->skill->name }}</li>
                                        @empty
                                            <li>No Skill</li>
                                        @endforelse
                                    </ul>

                                    <h6 class="text-black-50 mt-3 mb-3"><strong>FAQS:</strong><br></h6>
                                    <dl class="ml-4">
                                        @if(count($gig->faqs)>0)
                                            @foreach ($gig->faqs as $faq)
                                                <dt>{{ $faq->title }}</dt>
                                                <dd class="mb-3">{{ $faq->answer }}</dd>
                                            @endforeach
                                        @else
                                            <li>No FAQ</li>
                                        @endif
                                    </dl>

                                    <h6 class="text-black-50 mt-3 mb-3"><strong>Video:</strong><br></h6>
                                        @if($gig->video !="")
                                            <div class="card-header p-0 pl-4">
                                                <video controls style="width:100%;">
                                                    <source src="{{ asset("storage/gig video/".$gig->video) }}" type="video/mp4">
                                                </video>
                                            </div>
                                        @else
                                            <p>No video provided</p>
                                        @endif

                                </div>
                                <div class="tab-pane" role="tabpanel" id="tab-2">
                                    <div class="col">
                                        {{-- <div class="row comment-card pt-4 pb-4 border-bottom">
                                            <div class="col col-3 col-sm-2 text-center"><img class="rounded-circle" src="assets/img/man.png" width="60" height="60"></div>
                                            <div class="col">
                                                <h6 class="mb-1">Themexylum</h6>
                                                <p class="text-black-50 font-weight-light mt-1">9 Hours Ago</p>
                                                <p class="font-weight-normal mt-3">Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut sceleris que the mattis, leo quam aliquet congue placerat mi id nisi interdum mollis.</p>
                                            </div>
                                        </div> --}}
                                        @forelse ($orders->where("status","Completed") as $order)
                                            <div class="row comment-card pt-4 pb-4 border-bottom">
                                                @if ($order->user->type == "regular")
                                                    <div class="col text-center col-3 col-sm-2">
                                                        <img class="rounded-circle" src="{{ asset("img/man%20(1).png") }}" width="60" height="60">
                                                    </div>
                                                @endif

                                                @if ($order->user->type == "seller")
                                                    <div class="col text-center col-3 col-sm-2">
                                                        <img class="rounded-circle" src="{{ asset("storage/profile images/".$order->user->seller->profile_image) }}" width="60" height="60">
                                                    </div>
                                                @endif

                                                <div class="col">
                                                    <h6 class="mb-1">{{ $order->review->user->name }}
                                                        <small class="float-right">
                                                            @php
                                                                $current = Carbon\Carbon::now();
                                                                $dt = new Carbon\Carbon($order->review->created_at);
                                                                // $dt->addDays(3);
                                                                echo $dt->toFormattedDateString();
                                                            @endphp
                                                        </small>
                                                    </h6>
                                                    <p class="font-weight-light mt-1  ">
                                                    Service : <span class="font-weight-light rating">
                                                        @for ($i = 1; $i <= $order->review->service; $i++)
                                                            <i class="fa fa-star"></i>
                                                        @endfor
                                                        &nbsp;{{ $order->review->service }}</span>
                                                    </p>
                                                    <p class="font-weight-light mt-1">
                                                        Communication :
                                                        <span class="font-weight-light rating">
                                                            @for ($i = 1; $i <= $order->review->communication; $i++)
                                                                <i class="fa fa-star"></i>
                                                            @endfor
                                                            &nbsp;{{ $order->review->communication }}
                                                        </span>
                                                    </p>
                                                    <p class="font-weight-normal mt-3">
                                                        {{ $order->review->description }}
                                                    </p>
                                                </div>
                                            </div>
                                        @empty
                                            <p class="mt-5 pb-5">No review has been made on this gig</p>
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-lg-4">
                <div class="row">
                    <div class="col">
                        <div class="shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-4 mt-2 mt-lg-0">
                            <h3 class="text-center font-weight-bold main-bg-color pt-3 pb-3 shadow-sm rounded mb-4">Starting at {{ $setting->currency }} {{ $gig->basicPlan->price }}</h3>
                            <div role="tablist" id="product-sizes-accordian">
                                <div class="card">
                                    <div class="card-header" role="tab">
                                        <h6 class="mb-0">
                                            <a data-toggle="collapse" aria-expanded="true" aria-controls="product-sizes-accordian .item-1" href="div#product-sizes-accordian .item-1">
                                                <i class="fa fa-dot-circle-o"></i>&nbsp; Basic Plan
                                            </a>
                                        </h6>
                                    </div>
                                    <div class="collapse show item-1" role="tabpanel" data-parent="#product-sizes-accordian">
                                        <div class="card-body">
                                            <div>
                                                <h6 class="font-weight-normal mb-4">{{ $gig->basicPlan->plan_title }}<span class="font-weight-bold float-right main-color">&nbsp; {{ $gig->basicPlan->price }}</span></h6>
                                                <p class="font-weight-bold text-dark mb-2"><i class="fa fa-clock-o main-color"></i>&nbsp; {{ $gig->basicPlan->duration }} {{ $gig->basicPlan->duration_type }} Delivery</p>
                                                <ul class="list-unstyled">
                                                    @foreach ($gig->basicPlan->extras as $extra)
                                                        <li>
                                                            <i class="far fa-check-circle text-success"></i>&nbsp;
                                                            {{ $extra->name }}
                                                        </li>
                                                    @endforeach
                                                </ul>
                                                @if(auth()->id() != $gig->user->id)
                                                    <a href="{{ route("purchase.cart",['plan' => 'basic','id' => $gig->basicPlan->id]) }}" class="btn btn-primary fr-cl-bcs btn-block text-white font-weight-bold mt-4 text-wh" role="button">Continue ({{ $setting->currency }} {{ $gig->basicPlan->price }})</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab">
                                        <h6 class="mb-0"><a data-toggle="collapse" aria-expanded="false" aria-controls="product-sizes-accordian .item-2" href="div#product-sizes-accordian .item-2"><i class="fa fa-dot-circle-o"></i>&nbsp; Standard Plan</a></h6>
                                    </div>
                                    <div class="collapse item-2" role="tabpanel" data-parent="#product-sizes-accordian">
                                        <div class="card-body">
                                            <div>
                                                <h6 class="font-weight-normal mb-4">{{ $gig->standardPlan->plan_title }}<span class="font-weight-bold float-right main-color">&nbsp;{{ $gig->standardPlan->price }}</span></h6>
                                                <p class="font-weight-bold text-dark mb-2"><i class="fa fa-clock-o main-color"></i>&nbsp; {{ $gig->standardPlan->duration }} {{ $gig->standardPlan->duration_type }} Delivery</p>
                                                <ul class="list-unstyled">
                                                    @foreach ($gig->standardPlan->extras as $extra)
                                                        <li>
                                                            <i class="far fa-check-circle text-success"></i>&nbsp;
                                                            {{ $extra->name }}
                                                        </li>
                                                    @endforeach
                                                </ul>
                                                @if(auth()->id() != $gig->user->id)
                                                    <a href="{{ route("purchase.cart",['plan' => 'standard','id' => $gig->standardPlan->id]) }}" class="btn btn-primary fr-cl-bcs btn-block text-white font-weight-bold mt-4 text-wh" role="button">Continue ({{ $setting->currency }} {{ $gig->standardPlan->price }})</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab">
                                        <h6 class="mb-0"><a data-toggle="collapse" aria-expanded="false" aria-controls="product-sizes-accordian .item-3" href="div#product-sizes-accordian .item-3"><i class="fa fa-dot-circle-o"></i>&nbsp; Premium Plan</a></h6>
                                    </div>
                                    <div class="collapse item-3" role="tabpanel" data-parent="#product-sizes-accordian">
                                        <div class="card-body">
                                            <div>
                                                <h6 class="font-weight-normal mb-4">{{ $gig->premiumPlan->plan_title }}<span class="font-weight-bold float-right main-color">&nbsp;{{ $gig->premiumPlan->price }}</span></h6>
                                                <p class="font-weight-bold text-dark mb-2"><i class="fa fa-clock-o main-color"></i>&nbsp; {{ $gig->premiumPlan->duration }} {{ $gig->premiumPlan->duration_type }} Delivery</p>
                                                <ul class="list-unstyled">
                                                    @foreach ($gig->premiumPlan->extras as $extra)
                                                        <li>
                                                            <i class="far fa-check-circle text-success"></i>&nbsp;
                                                            {{ $extra->name }}
                                                        </li>
                                                    @endforeach
                                                </ul>
                                                @if(auth()->id() != $gig->user->id)
                                                    <a href="{{ route("purchase.cart",['plan' => 'premium','id' => $gig->premiumPlan->id]) }}" class="btn btn-primary fr-cl-bcs btn-block text-white font-weight-bold mt-4 text-wh" role="button">Continue ({{ $setting->currency }} {{ $gig->premiumPlan->price }})</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-4">
                            <p class="font-weight-bold pb-4 border-bottom">
                                <i class="fa fa-shopping-cart main-color"></i>&nbsp;
                                Total Sales
                                <span class="float-right main-color">{{ count($gig->orders) }}</span>
                            </p>
                            <p class="font-weight-bold pb-4 pt-4 border-bottom">
                                <i class="fa fa-heart text-info"></i>&nbsp; Views
                                <span class="float-right main-color">{{ $gig->visits }}</span>
                            </p>
                            <p class="font-weight-bold pb-4 pt-4 border-bottom">
                                <i class="fa fa-comment text-success"></i>&nbsp; Comments
                                <span class="float-right main-color">{{ count($orders->where("status","Completed")) }}</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-4">
                            <div class="text-center">
                                <img src="{{ asset("storage/profile images/".$gig->user->seller->profile_image) }}" width="100">
                                <h5 class="mt-3 mb-0">{{ $gig->user->name }}</h5>
                                <p class="mb-0" style="font-size: 13px;">{{ $gig->user->seller->tagline }}<br></p>
                                <p class="font-weight-light mt-1 rating font-weight-bold mt-0"><i class="fa fa-star"></i>&nbsp;{{ count($gig->user->reviews) }}</p>
                                <p class="text-left font-weight-bold main-color" style="font-size: 14px;">From<span class="float-right text-black-50">{{ $gig->user->seller->country }}</span></p>
                                <p class="text-left font-weight-bold main-color" style="font-size: 14px;">Age<span class="float-right text-black-50">17</span></p>
                                <p class="text-left font-weight-bold main-color" style="font-size: 14px;">Joined On<span class="float-right text-black-50">{{ $gig->user->seller->created_at }}</span></p>
                                <p class="text-left font-weight-bold main-color" style="font-size: 14px;">Languages<span class="float-right text-black-50">{{ $gig->user->seller->language }}</span></p>
                                @if(auth()->user()->id != $gig->user->id)
                                    <a href="{{ route("message",[$gig->user->id]) }}" class="btn btn-outline-success btn-block btn-sm text-success mt-4" role="button"><i class="icon ion-chatbox"></i>&nbsp; Message</a>
                                @endif
                                <a href="{{ route("account.show",[$gig->user->id]) }}" class="btn btn-outline-primary btn-block btn-sm text-primary mt-4"><i class="icon ion-ios-paperplane"></i>&nbsp; View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="home-newest-release-products" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row pt-3 pb-3">
            <div class="col col-md-12">
                <div>
                    <h3 class="mb-2 text-center">Similar Products</h3>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($gigs as $gig)
                @include("layouts.gig",['gig'=>$gig])
            @endforeach
        </div>
    </div>
</section>
@endsection
