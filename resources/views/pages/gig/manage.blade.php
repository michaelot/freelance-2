@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Manage Gigs<br></h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Dashboard</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"manageGig"])
<section id="purchases" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row">
            <div class="col col-lg-6">
                <h2>Manage Items</h2>
            </div>
            <div class="col col-lg-6 text-right">
                <h5 class="font-weight-normal">{{ count($gigs) }}</h5>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            @foreach ($gigs as $gig)
                @include("layouts.gig",['gig'=>$gig])
            @endforeach


        </div>
    </div>
</section>
@endsection
