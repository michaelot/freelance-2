@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Shopping Cart<br></h2>
                <p class="text-left">
                    <span class="text-white">Home /&nbsp;</span>
                    <span class="text-white-50">Shopping Cart</span>&nbsp;&nbsp;
                </p>
            </div>
        </div>
    </div>
</div>
<section id="main-shopping-cart-page" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-9 col-12 bg-white shadow-sm rounded mb-3">
                <div class="row pt-4 pb-4">
                    <div class="col col-lg-6 pl-3">
                        <h5 class="font-weight-normal">Product Details</h5>
                    </div>
                </div>
                <div class="row pt-5 pb-5 border-top">
                    <div class="col col-lg-4 pl-3 col-12"><img class="rounded img-fluid shadow-sm" src="assets/img/Editable-stationery-mockup.jpg"></div>
                    <div class="col col-lg-6 col-12 pt-4 pb-2 pt-lg-0 pb-lg-0">
                        <h6 class="mb-0">Finance and Consulting Business Theme</h6>
                        <h6 class="font-weight-normal mb-2 text-16">Single Site License</h6>
                        <p class="text-dark mb-2"><i class="fa fa-clock-o main-color"></i>&nbsp; 3 days Delivery</p>
                        <ul class="list-unstyled">
                            <li class="text-14"><i class="far fa-check-circle text-success"></i>&nbsp; Includes Logo Design<br></li>
                            <li class="text-14"><i class="far fa-check-circle text-success"></i>&nbsp; Color Palette<br></li>
                            <li class="text-14"><i class="far fa-check-circle text-success"></i>&nbsp; Brand Book Design<br></li>
                            <li class="text-14"><i class="far fa-check-circle text-success"></i>&nbsp; Typography Guidelines<br></li>
                        </ul>
                    </div>
                    <div class="col col-lg-2 text-center">
                        <p><span class="badge badge-primary price-badge-light">$42</span></p>
                    </div>
                </div>
            </div>
            <div class="col col-lg-3 col-12">
                <div class="shadow-sm bg-white pb-0 mb-4 mt-2 mt-lg-0">
                    <h3 class="text-center font-weight-bold main-bg-color pt-3 pb-3 shadow-sm shadow-sm">Summary</h3>
                    <p class="font-weight-normal pb-4 border-bottom ml-3 mr-3 pt-3">Subtotal:<span class="float-right main-color">$40</span></p>
                    <p class="font-weight-normal pb-4 border-bottom ml-3 mr-3 pt-3">Total Service fee:<span class="float-right main-color">$4</span></p>
                    <p class="font-weight-bold pb-4 border-bottom ml-3 mr-3 pt-3">Total:<span class="float-right main-color">$400</span></p><button class="btn btn-primary text-center font-weight-bold main-bg-color pt-3 pb-3 shadow-sm rounded-0 btn-block mt-5" type="button">Proceed To Checkout&nbsp;</button></div>
            </div>
        </div>
    </div>
</section>
@endsection
