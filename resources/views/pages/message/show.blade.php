@extends('layouts.custom')
@section('appjs')
<meta name="page" content="messages">
    <script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left"><strong>Message Box</strong><br></h2>
                <h5 class="font-weight-normal text-white-50 mb-3 text-left">A single place, millions of creative talents</h5>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Login</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
<section id="purchases" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row">
            <div class="col col-lg-6">
                <h2>Messages</h2>
            </div>
        </div>
        <div id="app" class="row pt-4 pb-4">
            <div class="col">
                <div class="bg-white pb-5 pl-3 pr-3 mb-5">
                    <div class="row pt-4 pb-4 shadow-sm mb-5">
                        <div class="col d-flex flex-lg-row align-items-center">
                            <div class="flex-1">
                                <h5 class="font-weight-normal">Message with {{ $user->name }}</h5>
                            </div>
                            <div id="fr-id-bcs" class="flex-1 text-right"><a class="active fr-cl-bcs text-white bg-danger-light mt-5 mb-2 mt-sm-0 mb-sm-0 mr-0" id="fr-id-bcs"><i class="far fa-trash-alt"></i></a></div>
                        </div>
                    </div>
                    {{-- <div class="row mb-3">
                        <div class="col col-sm-10 offset-sm-1 bg-color text-left pt-3 pb-3 rounded-lg shadow-sm">
                            <div class="media"><img class="rounded-circle img-fluid mr-3" src="assets/img/man.png" width="45">
                                <div class="media-body">
                                    <h6 class="font-weight-normal">John Timothy</h6>
                                    <p class="text-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis maximus nisl ac diam feugiat, non vestibulum libero posuere</p>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <messages-component  v-bind:user="{{ auth()->user() }}" v-bind:messages="messages"></messages-component>
                    <input type="hidden" id="userId" value="{{ auth()->id() }}">
                    <input type="hidden" id="receiverId" value="{{ $user->id }}">
                    <send-message-component v-on:message-Sent="addMessage"  v-bind:receiver="{{ $user }}" v-bind:user="{{ auth()->user() }}"></send-message-component>
                    {{-- <form class="text-left mt-5">
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-10 offset-lg-1"><textarea class="form-control main-input-textarea rounded-lg shadow-sm" placeholder="Type comment" style="height:60px"></textarea></div>
                            </div>
                            <div class="form-row">
                                <div class="col col-lg-10 offset-lg-1 text-right"><button class="btn btn-primary text-white fr-cl-bcs border-0 font-weight-normal mt-3 mr-0" id="delivery-btn" data-toggle="collapse" type="submit">Send</button></div>
                            </div>
                        </div>
                    </form> --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

