@extends('layouts.custom')
@section('appjs')
    <meta name="page" content="messageIndex">
    <script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left"><strong>Login</strong><br></h2>
                <h5 class="font-weight-normal text-white-50 mb-3 text-left">A single place, millions of creative talents</h5>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Login</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
<section id="purchases" class="section-padding bg-color">
    <div class="container" id="app">
        <div class="row section-header-row">
            <div class="col col-lg-6">
                <h2>Messages</h2>
            </div>
        </div>
        <div class="row pt-4 pb-4">
            <div class="col col-lg-5 col-12">
                <div class="bg-white pb-5 pl-3 pr-3 mb-5">
                    {{-- {{ $messages }} --}}
                    <div class="row pt-4 pb-4 shadow-sm mb-3">
                        <div class="col d-flex flex-lg-row align-items-center">
                            <div class="flex-1">
                                <h5 class="font-weight-normal">Inbox</h5>
                            </div>
                            <div id="fr-id-bcs" class="flex-1 text-right">
                                <a class="active fr-cl-bcs text-white mb-2 mt-sm-0 mb-sm-0 mr-0" id="fr-id-bcs">Compose</a>
                            </div>
                        </div>
                    </div>

                    <chat-list v-bind:users="listing"></chat-list>
                    <input type="hidden" id="userId" value="{{ auth()->id() }}">

                    {{-- <div class="row border-top border-bottom pt-3 pb-3">
                        <div class="col">
                            <div class="media"><img class="rounded-circle mr-3" width="45" height="45" src="assets/img/boy%20(1).png">
                                <div class="media-body">
                                    <h6 class="font-weight-normal d-flex justify-content-lg-between align-items-center">John Doe&nbsp;<span class="small main-color">&nbsp;2 hours ago</span></h6>
                                    <p class="text-14 text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;Lorem ipsum dolor sit ame,&nbsp;</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row border-top border-bottom pt-3 pb-3">
                        <div class="col">
                            <div class="media"><img class="rounded-circle mr-3" width="45" height="45" src="assets/img/boy%20(1).png">
                                <div class="media-body">
                                    <h6 class="font-weight-normal d-flex justify-content-lg-between align-items-center">John Doe&nbsp;<span class="small main-color">&nbsp;2 hours ago</span></h6>
                                    <p class="text-14 text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;Lorem ipsum dolor sit ame,&nbsp;</p>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
            <div class="col col-lg-7 col-12">
                {{-- <div class="bg-white pb-5 pl-3 pr-3 mb-5 d-none d-lg-block">
                    <div class="row pt-4 pb-4 shadow-sm mb-5">
                        <div class="col d-flex flex-lg-row align-items-center">
                            <div class="flex-1">
                                <h5 class="font-weight-normal">Message from john</h5>
                            </div>
                            <div id="fr-id-bcs" class="flex-1 text-right">
                                <a class="active fr-cl-bcs text-white bg-danger-light mt-5 mb-2 mt-sm-0 mb-sm-0 mr-0" id="fr-id-bcs"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col col-lg-10 offset-lg-1 bg-color text-left pt-3 pb-3 rounded-lg shadow-sm">
                            <div class="media"><img class="rounded-circle img-fluid mr-3" src="assets/img/man.png" width="45">
                                <div class="media-body">
                                    <h6 class="font-weight-normal">John Timothy</h6>
                                    <p class="text-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis maximus nisl ac diam feugiat, non vestibulum libero posuere</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col col-lg-10 offset-lg-1 bg-color text-left pt-3 pb-3 rounded-lg shadow-sm">
                            <div class="media"><img class="rounded-circle img-fluid mr-3" src="assets/img/man.png" width="45">
                                <div class="media-body">
                                    <h6 class="font-weight-normal">John Timothy</h6>
                                    <p class="text-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis maximus nisl ac diam feugiat, non vestibulum libero posuere</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col col-lg-10 offset-lg-1 bg-color text-left pt-3 pb-3 rounded-lg shadow-sm">
                            <div class="media"><img class="rounded-circle img-fluid mr-3" src="assets/img/man.png" width="45">
                                <div class="media-body">
                                    <h6 class="font-weight-normal">John Timothy</h6>
                                    <p class="text-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis maximus nisl ac diam feugiat, non vestibulum libero posuere</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> --}}
                <div class="bg-white pb-5 pl-3 pr-3 mb-5">
                    <div class="row pt-4 pb-4 shadow-sm mb-4">
                        <div class="col d-flex flex-lg-row align-items-center">
                            <div class="flex-1">
                                <h5 class="font-weight-normal">Compose New Message</h5>
                            </div>
                        </div>
                    </div>
                    <compose-message-component v-on:message-Sent="addMessage" v-bind:user="{{ auth()->user() }}" v-bind:type="'{{ auth()->user()->type }}'" v-bind:users='{{ $users }}'></compose-message-component>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection

