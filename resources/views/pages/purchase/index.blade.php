@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Purchases<br></h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Dashboard</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"purchases"])
<section id="purchases" class="section-padding bg-color">
    <div class="container">
        <div class="row section-header-row">
            <div class="col col-lg-6">
                <h2>Manage Purchases</h2>
            </div>
            <div class="col col-lg-6 text-right">
                <h5 class="font-weight-normal">{{ count($orders) }} Products</h5>
            </div>
        </div>
        <div class="row bg-white pt-4 pb-4">
            <div class="col col-lg-6">
                <h6 class="mb-0">Product Details</h6>
            </div>
            <div class="col col-lg-2 d-none d-lg-block">
                <h6 class="mb-0">Duration</h6>
            </div>
            <div class="col col-lg-2 d-none d-lg-block">
                <h6 class="mb-0">Price</h6>
            </div>
            <div class="col col-lg-2 d-none d-lg-block">
                <h3></h3>
            </div>
        </div>
        @foreach ($orders as $order)
            @if($order->payment->plan_type == "basic" )
                @php
                    $duration_type =  $order->payment->basicPlan->duration_type;
                    $duration =  $order->payment->basicPlan->duration;
                @endphp
            @endif
            @if($order->payment->plan_type == "standard" )
                @php
                    $duration_type =  $order->payment->standardPlan->duration_type;
                    $duration =  $order->payment->standardPlan->duration;
                @endphp
            @endif
            @if($order->payment->plan_type == "premium" )
                @php
                    $duration_type =  $order->payment->premiumPlan->duration_type;
                    $duration =  $order->payment->premiumPlan->duration;
                @endphp

            @endif
            <div class="row bg-white pt-5 pb-5 border-top">
                <div class="col col-lg-2 col-12 col-md-6 mb-4 mb-0">
                    <img class="img-fluid" src="{{ asset("storage/gig image/".$order->gig->image) }}">
                </div>
                <div class="col col-lg-4">
                    <h6>{{ $order->gig->title }}</h6>
                    <div class="d-flex align-items-center d-inline-block mb-3 user-avatar">
                        <div class="float-left">
                            <img src="{{ asset("storage/profile images/".$order->gig->user->seller->profile_image) }}" width="25" height="25">
                        </div>
                        <div class="float-left pl-2">
                            <p>{{ $order->gig->user->name }}</p>
                        </div>
                    </div>
                    <div class="mb-3">
                        @if($order->status == "In Progress")
                            <span class="badge badge-primary price-badge-thick mr-3 bg-primary-light text-white">In Progress</span>
                        @endif

                        @if($order->status == "Completed")
                            <span class="badge badge-primary price-badge-thick mr-3 bg-success-light text-white">Completed</span>
                        @endif

                        @if($order->status == "Delivered")
                            <span class="badge badge-primary price-badge-thick mr-3 bg-info-light text-white">Delivered</span>
                        @endif

                        @if($order->status == "Canceled")
                            <span class="badge badge-primary price-badge-thick mr-3 text-white bg-danger-light">Canceled</span>
                        @endif
                    </div>
                    <div class="d-lg-none">
                        <p class="text-14">
                            Started On:&nbsp;
                            <span class="font-weight-bold">
                                @php
                                    $current = Carbon\Carbon::now();
                                    $dt = new Carbon\Carbon($order->payment->created_at);
                                    // $dt->addDays(3);
                                    echo $dt->toFormattedDateString();
                                @endphp
                            </span>
                        </p>
                        <p class="text-14 mb-3">Due On:&nbsp;
                            <span class="font-weight-bold">
                                @php
                                    if($duration_type == "hour(s)"){
                                        $dt->addHours($duration);
                                        $diff = $dt->diffInMinutes($current);
                                    }
                                    if($duration_type == "week(s)"){
                                        $dt->addWeeks($duration);
                                        $diff = $dt->diffInDays($current);
                                    }
                                    if($duration_type == "day(s)"){
                                        $dt->addDays($duration);
                                        $diff = $dt->diffInHours($current);
                                    }
                                    if($duration_type == "month(s)"){
                                        $dt->addDays($duration);
                                        $diff = $dt->diffInWeeks($current);
                                    }
                                    echo $dt->toFormattedDateString();
                                @endphp
                            </span>
                        </p>
                        <span class="badge badge-primary price-badge-light pl-3 pr-3">{{ $order->payment->amount }}</span>
                        <a href="{{ route("order.show",[$order->id]) }}" class="btn btn-primary mt-2 fr-cl-bcs btn-block btn-white font-weight-normal" style="background: #ffffff;border: none;">View</a>
                    </div>
                </div>
                <div class="col col-lg-2 d-none d-lg-block">
                    <p class="text-14">
                        Started On:&nbsp;
                        <span class="font-weight-bold">
                            @php
                                $current = Carbon\Carbon::now();
                                $dt = new Carbon\Carbon($order->payment->created_at);
                                // $dt->addDays(3);
                                echo $dt->toFormattedDateString();
                            @endphp
                        </span>
                    </p>
                    <p class="text-14 mb-3">Due On:&nbsp;
                        <span class="font-weight-bold">
                            @php
                                if($duration_type == "hour(s)"){
                                    $dt->addHours($duration);
                                    $diff = $dt->diffInMinutes($current);
                                }
                                if($duration_type == "week(s)"){
                                    $dt->addWeeks($duration);
                                    $diff = $dt->diffInDays($current);
                                }
                                if($duration_type == "day(s)"){
                                    $dt->addDays($duration);
                                    $diff = $dt->diffInHours($current);
                                }
                                if($duration_type == "month(s)"){
                                    $dt->addDays($duration);
                                    $diff = $dt->diffInWeeks($current);
                                }
                                echo $dt->toFormattedDateString();
                            @endphp
                        </span>
                    </p>
                </div>
                <div class="col col-lg-2 d-none d-lg-block">
                    <span class="badge badge-primary price-badge-light pl-3 pr-3">{{ $order->payment->amount }}</span>
                </div>
                <div class="col col-lg-2 text-center d-none d-lg-block">
                    <a href="{{ route("order.show",[$order->id]) }}" class="btn btn-primary mt-2 fr-cl-bcs btn-block btn-white font-weight-normal" style="background: #ffffff;border: none;">View</a>
                </div>
            </div>
        @endforeach

    </section>
@endsection
