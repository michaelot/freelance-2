@extends('layouts.custom')
@section('content')
<style>
  #stripeBtn{

    font-weight: 700!important;
    display: block !important;
    width: 100% !important;
    font-family: Quicksand, sans-serif;
    color: #fff !important;
    background: none !important;
    background-color: #007bff !important;
    border-color: #007bff !important;
    height:auto !important;
    font-size: 1rem !important;
  }
</style>
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Checkout<br></h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Checkout</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"manageGig"])
<section id="main-check-out" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 col-12 mb-3 mb-lg-0">
                <div class="row">
                    <div class="col">
                        <div class="shadow-sm bg-white pb-0 mb-4 mt-2 mt-lg-0">
                            <h3 class="text-center font-weight-bold main-bg-color pt-3 pb-3 shadow-sm shadow-sm">Order Summary</h3>
                            <p class="font-weight-normal pb-4 border-bottom ml-3 mr-3 pt-3">{{ $plan->gig->title }}<span class="float-right main-color">{{ $setting->currency }} {{ $plan->price }}</span></p>
                            <p class="font-weight-normal pb-4 border-bottom ml-3 mr-3 pt-3">Estimated Taxes &amp; Fees:<span class="float-right main-color">{{ $setting->currency }} {{ $paymentSetting->tax }}</span></p>
                            <p class="font-weight-bolder pb-4 ml-3 mr-3 pt-3">Total:<span class="float-right main-color">{{ $setting->currency }} {{ $plan->price + 15 }}</span></p>
                        </div>
                    </div>
                    <div class="col">
                        <div class="shadow-sm bg-white pt-5 pb-5 pl-3 pr-3 mb-4 mt-2 mt-lg-0">
                            <h5 class="text-left font-weight-normal mb-4">Your details</h5>
                            @include('layouts.alert')
                            <form method="POST" action="{{ route('paypal') }}" class="mt-4">
                                @csrf
                                <input class='form-control card-expiry-year' placeholder='YYYY' size='4' type='hidden' name="amount" value="300">
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2">Name</label>
                                            <input required value="{{ auth()->user()->name }}" name="name" disabled class="form-control main-input-form readonly" type="text" placeholder="Enter Your Name..">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                            <label class="font-weight-bold mb-2">Gig</label>
                                            <input required value="{{ $plan->gig->title }}" name="" disabled class="form-control main-input-form" type="text" placeholder="Enter Gig Name">
                                            <input type="hidden" required value="{{ $plan->gig->id }}" name="gig_id">
                                            <input type="hidden" required value="{{ $plan->gig->title }}" name="gig_name">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-6">
                                                <input required value="{{ $plan->plan_title }}" name="plan_name" disabled class="form-control main-input-form readonly" type="text" placeholder="Enter Gig Name">
                                                <input type="hidden" required value="{{ $plan->id }}" name="plan_id">
                                                <input type="hidden" required value="{{ $type }}" name="plan_type">
                                        </div>
                                        <div class="col align-self-end col-lg-6">
                                                <input required value="{{ $setting->currency }} {{ $plan->price+$paymentSetting->tax }}" disabled class="form-control main-input-form readonly" type="text" placeholder="Enter Gig Name">
                                                <input type="hidden" required value="{{ $plan->price }}" name="amount">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col col-lg-12">
                                                <input class="btn btn-primary btn-block font-weight-bold pt-2 pb-2 " type="submit" value="Buy"  />
                                            {{-- <button class="btn btn-primary btn-block font-weight-bold pt-2 pb-2 mt-3" type="submit">Confirm Order</button> --}}
                                            {{-- <form action="{{ route('payment.charge') }}" method="POST"> --}}
                                                {{-- @csrf
                                              <script
                                                      src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                      data-key="{{ $paymentSetting->publishable_key }}"
                                                      data-amount="{{ ($plan->price+$paymentSetting->tax) * 100 }}"
                                                      data-name="Freelance App"
                                                      data-description="Payment for gig"
                                                      data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                                      data-locale="auto"
                                                      data-currency="{{ $setting->currency }}">
                                              </script>
                                              <script>
                                                $("button[type='submit'].stripe-button-el span").attr("id","stripeBtn");
                                                $("button[type='submit'].stripe-button-el span").attr("class","btn btn-primary btn-block font-weight-bold pt-2 pb-2 ");
                                                $("button[type='submit'].stripe-button-el").attr("class","btn-block border-0 p-0");

                                              </script> --}}
                                            {{-- </form> --}}
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    document.getElementById("stripeBtn").innerHTML = "Buy"
</script>
@endsection
