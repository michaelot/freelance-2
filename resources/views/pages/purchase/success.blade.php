@extends('layouts.custom')
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left">Payment Successful&nbsp;</h2>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Setting</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
@include('layouts.subNav',['page'=>"manageGig"])

<section id="become-a-seller-section-one" class="section-padding bg-color">
    <div class="container">
        <div class="row mb-3">
            <div class="col pt-4 col-lg-5 text-center">
                <div><img class="img-fluid" src="{{ asset("img/love.svg") }}" width="300"></div>
            </div>
            <div class="col col-lg-7">`
                <h1 class="font-weight-normal mt-3 mb-4">Thanks For Your Patronage</h1>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td class="border-0">Gig</td>
                                <td class="border-0">{{ $order->payment->gig->title }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Author</td>
                                <td class="border-0">{{ $order->payment->gig->user->name }}</td>
                            </tr>

                            <tr>
                                <td class="border-0">Plan</td>
                                <td class="border-0">
                                    @if($order->payment->plan_type == "basic")
                                        {{ $order->payment->basicPlan->plan_title }}
                                    @endif

                                    @if($order->payment->plan_type == "standard")
                                        {{ $order->payment->standardPlan->plan_title }}
                                    @endif

                                    @if($order->payment->plan_type == "premium")
                                        {{ $order->payment->premiumPlan->plan_title }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="border-0">Amount</td>
                                <td class="border-0">
                                    @if($order->payment->plan_type == "basic")
                                        {{ $order->payment->basicPlan->price}}
                                    @endif

                                    @if($order->payment->plan_type == "standard")
                                        {{ $order->payment->standardPlan->price }}
                                    @endif

                                    @if($order->payment->plan_type == "premium")
                                        {{ $order->payment->premiumPlan->price }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="border-0">Duration</td>
                                <td class="border-0">

                                    @if($order->payment->plan_type == "basic")
                                        {{ $order->payment->basicPlan->duration}} {{ $order->payment->basicPlan->duration_type}}
                                    @endif

                                    @if($order->payment->plan_type == "standard")
                                        {{ $order->payment->standardPlan->duration }} {{ $order->payment->basicPlan->duration_type}}
                                    @endif

                                    @if($order->payment->plan_type == "premium")
                                        {{ $order->payment->premiumPlan->duration }} {{ $order->payment->basicPlan->duration_type}}
                                    @endif

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <h5 class="font-weight-normal mb-4">Requirements</h5>
                <form class="px-3" enctype="multipart/form-data" method="POST" action="{{ route("order.requirement.save",[$order->id]) }}">
                    @csrf
                    @forelse ($order->payment->gig->requirements as $requirement)
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col col-lg-12">
                                    <label class="font-weight-normal mb-2">{{ $requirement->title }}&nbsp;<span class="span-required">*</span></label>
                                    @if($requirement->answer_type == "Text")
                                        <textarea name="requirement[{{ $requirement->id }}][answer]" @if($requirement->compulsory == "Yes") required @endif class="form-control main-input-textarea" placeholder="Answer"></textarea>
                                    @endif
                                    @if($requirement->answer_type == "File")
                                        <input name="file[{{ $requirement->id }}][]" @if($requirement->compulsory == "Yes") required @endif type="file" class="form-control border-0 bg-color" multiple>
                                    @endif
                                    <input type="hidden" value="{{ $requirement->answer_type }}" name="requirement[{{ $requirement->id }}][answer_type]">
                                    <input type="hidden" name="requirement[{{ $requirement->id }}][id]" value="{{ $requirement->id }}">
                                </div>
                            </div>
                        </div>

                    @empty
                        <p id="no-requirement">No Requirement </p>
                    @endforelse
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col col-lg-12">
                                <label class="font-weight-normal mb-2">Provide Additional Briefing&nbsp;<span class="span-required">*</span></label>
                                <textarea name="brief" class="form-control main-input-textarea" placeholder="Additional Briefing"></textarea>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group">
                        <div class="form-row">
                            <div class="col col-lg-6"><label class="font-weight-normal mb-2">Additional Video&nbsp;<span class="span-required">*</span></label><input type="file" class="form-control border-0 bg-color" multiple=""></div>
                        </div>
                    </div> --}}
                    <div class="row mb-5">
                            <div class="col col-lg-6 ">
                                <button type="submit" class="btn btn-primary text-white fr-cl-bcs btn-lg" >Submit</button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
