@extends('layouts.custom')
@section('appjs')
    <meta name="page" content="messageIndex">
    <script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@section('content')
<div class="jumbotron main-pages-jumbotron" id="products-page-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-white mb-3 text-left"><strong>Notification</strong><br></h2>
                <h5 class="font-weight-normal text-white-50 mb-3 text-left">A single place, millions of creative talents</h5>
                <p class="text-left"><span class="text-white">Home /&nbsp;</span><span class="text-white-50">Login</span>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
<section id="purchases" class="section-padding bg-color">
    <div class="container">
        <div class="row pt-4 pb-4">
            <div class="col col-12">
                <div class="bg-white pb-5 pl-3 pr-3 mb-5">
                    <div class="row pt-4 pb-4 shadow-sm mb-3">
                        <div class="col d-flex flex-lg-row align-items-center">
                            <div class="flex-1">
                                <h3 class="font-weight-normal">Notifications</h3>
                            </div>
                        </div>
                    </div>
                    @for ($i = count(auth()->user()->notifications)-1; $i >= 0; $i--)
                        <div class="row border-top border-bottom pt-3 pb-3">
                            <div class="col">
                                <div class="media">
                                    <div class="d-flex align-items-center justify-content-center mr-3" style="width: 40px;height: 40px;border-radius: 100%;background: #7347c1;font-size: 20px;color: white;">
                                        <i class="far fa-bell"></i>
                                    </div>
                                    <div class="media-body">
                                        <p>{{ auth()->user()->notifications[$i]->notification }}<br></p>
                                        <span class="text-primary">
                                            @php
                                                $current = Carbon\Carbon::now();
                                                $dt = new Carbon\Carbon(auth()->user()->notifications[$i]->created_at);
                                                // $dt->addDays(3);
                                                echo $current->diffForHumans($dt);
                                            @endphp
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
