$("#home-main-featured-products #prev").on("click",function(e){
    e.preventDefault()
    $("#carousel-1").carousel("prev")
})

$("#home-main-featured-products #next").on("click",function(e){
    e.preventDefault()
    $("#carousel-1").carousel("next")
})

$("#prev2").on("click",function(e){
    e.preventDefault()
    $("#carousel-2").carousel("prev")
})

$("#next2").on("click",function(e){
    e.preventDefault()
    $("#carousel-2").carousel("next")
})

$("#jumbotron-search-input").on("click",function(e){
    e.target.value =""
})

$("#jumbotron-search-input").on("blur",function(e){
    e.target.value ="Search for product"
})


$('#profile-picture-avatar-edit').hide()
$("#profile-picture-avatar-container").mouseenter(function(){
    $('#profile-picture-avatar-edit').show()
})
$("#profile-picture-avatar-container").mouseleave(function(){
    $('#profile-picture-avatar-edit').fadeOut(500)
})

var options = {
  chart: {
    type: 'area',
    height: 550
  },
    animations: {
        enabled: true,
        easing: 'easeinout',
        speed: 800,
        animateGradually: {
            enabled: true,
            delay: 150
        },
        dynamicAnimation: {
            enabled: true,
            speed: 350
        }
    },
  stroke: {
        curve: 'smooth'
    },
    dataLabels: {
        enabled: true
    },
    series: [{
        name: 'Daily Views',
        data: [31, 40, 28, 51, 42, 109, 100],
    }, {
        name: 'Daily Sales',
        data: [11, 32, 45, 32, 34, 52, 41]
    }],
    colors:[
        "#7345BF","#0674ec"
    ],
    xaxis: {
        type: 'date',
        categories: ["0", "5", "10", "15", "20", "25", "30"],                
    },
    tooltip: {
        x: {
            format: 'dd/MM/'
        },
    }
}

var chart = new ApexCharts(document.querySelector("#chart1"), options);

chart.render();


var options2 = {
            chart: {
                type: 'donut',
            },
            colors:[
                "#28a745","#ffc107"
            ],
            series: [55, 44],
            total:{
                enable: true,
                label: "Total Orders",
                color:["#212121"]
            },
            labels: ['Completed Orders', 'Uncompleted Orders',],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 150
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        }

       var chart2 = new ApexCharts(
            document.querySelector("#chart2"),
            options2
        );
        
        chart2.render();
    


var options3 = {
            chart: {
                height: 450,
                type: 'bar',
            },
            colors:[
                "#EB6D4A","#0674ec","#7345BF"
            ],
            plotOptions: {
                bar: {
                    horizontal: true,
                    dataLabels: {
                        position: 'top',
                },
            }  
            },
            dataLabels: {
                enabled: true,
                offsetX: -6,
                style: {
                    fontSize: '12px',
                    colors: ['#fff']
                }
            },
            stroke: {
                show: true,
                width: 1,
                colors: ['#fff']
            },
            series: [{
                name: "Total Order",
                data: [44, 55, 41, 64, 22, 43, 21]
            },{
                name: "Total Sales",
                data: [53, 32, 33, 52, 13, 44, 32]
            },{
                name: "Total Visits",
                data: [13, 10, 20, 6, 20, 16, 15]
            }],
            xaxis: {
                categories: ["MON", "TUE", "WED", "THU", "FRI"],
            },
           
        }
    

       var chart3 = new ApexCharts(
            document.querySelector("#chart3"),
            options3
        );
        
        chart3.render();

var options4 = {
            chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    horizontal: true,
                }
            },
            dataLabels: {
                enabled: false
            },
            series: [{
                data: [400, 430, 448, 470, 540, 580, 690]
            }],
            xaxis: {
                categories: ['Graphic Design', 'Writing', 'Programming', 'Lifestyle', 'Video', 'Music', 'UI/UX'],
            }
        }

       var chart4 = new ApexCharts(
            document.querySelector("#chart4"),
            options4
           
        );
        
        chart4.render();

// $('#skills').select2();
$('select').select2();

//Add Basic Plan Extra
$("#add-basic-plan-extra").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12"><label class="font-weight-bold mb-2">Gig Title <span class="span-required">*</span></label><input type="text" class="form-control main-input-form" placeholder="Gig Extra" /></div>
    </div>
</div>`
    
   $("#basic-plan-Extra").append($(formCode).fadeIn(5000)) ;
})

//Add Standard Plan Extra
$("#add-standard-plan-extra").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12"><label class="font-weight-bold mb-2">Gig Title <span class="span-required">*</span></label><input type="text" class="form-control main-input-form" placeholder="Gig Extra" /></div>
    </div>
</div>`
    
   $("#standard-plan-Extra").append($(formCode).fadeIn(5000)) ;
})


//Add Standard Plan Extra
$("#add-premium-plan-extra").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12"><label class="font-weight-bold mb-2">Gig Title <span class="span-required">*</span></label><input type="text" class="form-control main-input-form" placeholder="Gig Extra" /></div>
    </div>
</div>`
    
   $("#premium-plan-Extra").append($(formCode).fadeIn(5000)) ;
})

// Add FAQ

$("#add-faq").on("click",function(){
    var formCode = `<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12">
        <label class="font-weight-bold mb-2">FAQ Question <span class="span-required">*</span></label>
        <input type="text" class="form-control main-input-form" placeholder="FAQ question" />
</div>
    </div>
</div>

<div class="form-group">
    <div class="form-row">
        <div class="col col-lg-12"><label class="font-weight-bold mb-2">Answer <span class="span-required">*</span></label><textarea class="form-control main-input-textarea" placeholder="Answer"></textarea></div>
    </div>
</div>
`
    
   $("#gig-description").append($(formCode).fadeIn(5000)) ;
})


$("#delivery-form").on("show.bs.collapse",function(){
    
    $("#delivery-btn").removeClass("bg-success").addClass("bg-danger").html("Close");
})

$("#delivery-form").on("hide.bs.collapse",function(){   
    $("#delivery-btn").removeClass("bg-danger").addClass("bg-success").html("Deliver now");
})