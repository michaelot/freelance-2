@if (session('success'))
    <div class="row">
        <div class="col-12">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <span><strong>Success</strong>{{ session('success') }}.</span>
            </div>
        </div>
    </div>
@endif

@if (session('error'))
    <div class="row">
        <div class="col-12">
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <span><strong>Error</strong> {{ session('error') }}.</span>
            </div>
        </div>
    </div>
@endif

@if($errors->any())
    @foreach ($errors->all() as $error)
        <div class="row">
            <div class="col-12">
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <span><strong>Error</strong> {{ $error }}.</span>
                </div>
            </div>
        </div>
    @endforeach
@endif
