<nav class="navbar navbar-light navbar-expand-md" id="home-sub-navbar">
        <div class="container-fluid"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-3"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-3">
                <ul class="nav navbar-nav mx-auto">

                    @seller
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if($page == "dashboard") active @endif pl-3 pr-3" href="{{ route("dashboard.index") }}"><i class="icon-home"></i>&nbsp;Dashboard</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if($page == "orders") active @endif pl-3 pr-3" href="{{  route("order.index") }}"><i class="icon-plus"></i>&nbsp;Orders</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a href="{{ route("gig.create") }}" class="nav-link @if($page == "createGig") active @endif pl-3 pr-3" ><i class="icon-plus"></i>&nbsp;Create Gig</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if($page == "manageGig") active @endif pl-3 pr-3" href="{{  route("gig.manage") }}"><i class="icon-layers"></i>&nbsp;Manage Gigs</a>
                        </li>
                    @endSeller
                    @public
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if($page == "purchases") active @endif pl-3 pr-3" href="{{  route("purchase.index") }}"><i class="icon-basket"></i>&nbsp;Purchases</a>
                        </li>
                    @endPublic
                    @seller
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if($page == "withdrawals") active @endif" href="#"><i class="icon-film"></i>&nbsp;Withdrawals</a>
                        </li>
                    @endSeller
                    @admin
                            <li class="nav-item" role="presentation">
                            <a class="nav-link @if($page == "dashboard") active @endif pl-3 pr-3" href="{{ route("dashboard.index") }}"><i class="icon-home"></i>&nbsp;Dashboard</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if($page == "users") active @endif pl-3 pr-3" href="{{  route("user.index") }}"><i class="icon-user"></i>&nbsp;Users</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if($page == "orders") active @endif pl-3 pr-3" href="{{  route("order.index") }}"><i class="icon-plus"></i>&nbsp;Orders</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if($page == "payments") active @endif pl-3 pr-3" href="{{  route("payment.index") }}"><i class="icon-basket"></i>&nbsp;Payments</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if($page == "withdrawal") active @endif pl-3 pr-3" href="{{  route("withdraw.index") }}"><i class="icon-film"></i>&nbsp;Withdrawals</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if($page == "managePages") active @endif pl-3 pr-3" href="{{  route("account.edit.pages") }}"><i class="icon-film"></i>&nbsp;Manage Pages</a>
                        </li>
                    @endAdmin
                    <li class="nav-item" role="presentation">
                        <a class="nav-link @if($page == "setting") active @endif" href="{{  route("account.edit") }}"><i class="icon-settings"></i>&nbsp;Setting</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
