<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    @yield('appjs')
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <script src="{{ asset('js/jquery.flipper-responsive.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/bs-animation.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    {{-- <script src="https://github.com/kimmobrunfeldt/progressbar.js/blob/master/dist/progressbar.js"></script> --}}
    <script src="{{ asset('js/script.js') }}"></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=ABeeZee">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abel">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=latin-ext,vietnamese">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome-iconpicker.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/line-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/simple-line-icons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/fontawesome5-overrides.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    {{-- <script src="https://js.stripe.com/v3/"></script> --}}
</head>
<body>
    <nav class="navbar navbar-light @guest navbar-expand-sm @endguest @auth navbar-expand @endauth" id="home-main-navbar">
        <div class="container">
            <a class="navbar-brand" href="#">Freelance 2</a>
            <button data-toggle="collapse" class="navbar-toggler text-20 text-white" data-target="#navcol-1"><i class="icon-menu"></i></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav ml-auto">
                    @guest
                        <li class="nav-item d-xl-flex align-items-xl-center " role="presentation">
                            <a href="{{ route('account.one') }}" class="nav-link active d-block fr-cl-bcs text-white mt-5  mb-2 mt-sm-0 mb-sm-0" id="fr-id-bcs">Become a Seller</a>
                        </li>
                        <li class="nav-item d-xl-flex align-items-xl-center" role="presentation">
                            <a href="{{ route("register") }}" class="nav-link active d-block fr-cl-bcs text-white mt-2  mb-2 mt-sm-0 mb-sm-0" id="fr-id-bcs" style="background: #7345BF;">Join</a>
                        </li>
                        <li class="nav-item d-xl-flex align-items-xl-center" role="presentation"><a href="{{ route("login") }}" class="nav-link active d-block fr-cl-bcs text-white" id="fr-id-bcs mb-2 mt-sm-0 mb-sm-0">Login</a></li>
                    @else
                        @regular
                        <li class="nav-item d-xl-flex align-items-xl-center d-none d-lg-block d-lg-flex align-items-lg-center" role="presentation"><a href="{{ route('account.one') }}" class="nav-link active d-block fr-cl-bcs text-white" id="fr-id-bcs">Become a Seller</a></li>
                        @endRegular
                        <li class="nav-item dropdown d-flex align-items-center d-lg-flex" id="fl-id-notification">
                            <a class="dropdown-toggle nav-link d-none d-lg-block pl-3 pr-3" data-toggle="dropdown" aria-expanded="false">
                                <div>
                                    <i class="icon-bell"></i>
                                    <span class="pulse animated infinite fr-nc bg-danger">{{ count(auth()->user()->notifications) }}</span>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right pulse animated fr-cl-nds" role="menu">
                                <div class="fr-cl-nds-container">
                                    <div class="row fr-cl-nds-container-header-row">
                                        <div class="col col-md-8">
                                            <h4>My Notification</h4>
                                        </div>
                                        <div class="col col-md-4 text-right"><a href="{{ route("notification.index") }}">View All</a></div>
                                    </div>
                                    @php
                                        $theCount = 0;
                                    @endphp
                                    @for ($i = count(auth()->user()->notifications)-1; $i >= 0; $i--)
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col col-md-3 d-flex justify-content-center align-items-center">
                                                <div class="d-flex align-items-center justify-content-center" style="width: 40px;height: 40px;border-radius: 100%;background: #7347c1;font-size: 20px;color: white;">
                                                    <i class="far fa-bell"></i>
                                                </div>
                                            </div>
                                            <div class="col col-md-9 pl-0">
                                                <p>{{ auth()->user()->notifications[$i]->notification }}<br></p>
                                                <span class="text-primary">
                                                    @php
                                                        $current = Carbon\Carbon::now();
                                                        $dt = new Carbon\Carbon(auth()->user()->notifications[$i]->created_at);
                                                        // $dt->addDays(3);
                                                        echo $current->diffForHumans($dt);
                                                    @endphp
                                                </span>
                                            </div>
                                        </div>
                                        @php
                                            $theCount++
                                        @endphp
                                        @if ($theCount == 4)
                                            @php
                                                break;
                                            @endphp
                                        @endif
                                    @endfor
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown d-flex align-items-center d-lg-flex" id="fl-id-message">
                            <a class="dropdown-toggle nav-link d-none d-lg-block pl-3 pr-3" data-toggle="dropdown" aria-expanded="false" href="#">
                                <div>
                                    <i class="icon-envelope"></i>
                                    <span class="pulse animated infinite fr-nc bg-success">{{ count($chats) }}</span>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right pulse animated fr-cl-nds" role="menu">
                                <div class="fr-cl-nds-container">
                                    <div class="row fr-cl-nds-container-header-row">
                                        <div class="col col-md-8">
                                            <h4>My Messages</h4>
                                        </div>
                                        <div class="col col-md-4 text-right"><a href="{{ route("message.index") }}">View All</a></div>
                                    </div>
                                    @php
                                        $cCount = 1;
                                    @endphp
                                    @foreach ($chats as $user)
                                        @if ($cCount > 5)
                                            @php
                                                break;
                                            @endphp
                                        @endif
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col col-md-3">
                                                <div class="d-flex align-items-center justify-content-center" style="width: 40px;height: 40px;border-radius: 100%;background: #77de51;font-size: 20px;color: white;">
                                                    <i class="far fa-envelope"></i>
                                                </div>
                                            </div>
                                            <div class="col col-md-9 pl-0">
                                                <p><span>{{ $user->name }}</span><br></p>
                                                <p>{{ $user->message }}</p>
                                                <span class="text-primary">
                                                    @php
                                                        $current = Carbon\Carbon::now();
                                                        $dt = new Carbon\Carbon($user->at);
                                                        echo $current->diffForHumans($dt);
                                                    @endphp
                                                </span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown d-none d-lg-block" id="fl-id-user">
                            <a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#">
                                <div class="d-block">
                                    @seller
                                        <div class="float-left">
                                            <img src='{{ asset("storage/profile images/".auth()->user()->seller->profile_image) }}' width="45">
                                        </div>
                                    @endSeller
                                    @regular
                                        <div class="float-left">
                                                <img class="rounded-circle" src="{{ asset("img/man%20(1).png") }}" width="45" height="45">
                                        </div>
                                    @endRegular
                                    <div class="float-left pl-3">
                                        <p id="fi-id-user-name">{{  auth()->user()->name }}</p>
                                        @seller
                                            <p class="text-primary">{{ $currency }} {{ $balance }}</p>
                                        @endSeller
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right fr-cl-nds" role="menu">
                                <div class="fr-cl-nds-container col-12">
                                    @seller
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("account.show",[auth()->id()]) }}">
                                                    <p><i class="icon-user"></i>&nbsp; &nbsp;Profile</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("dashboard.index") }}">
                                                    <p><i class="icon-home"></i>&nbsp; &nbsp;Dashboard</p>
                                                </a>
                                            </div>
                                        </div>
                                    @endSeller
                                    <div class="row fr-cl-nds-container-row">
                                        <div class="col">
                                            <a href="{{ route("account.edit") }}">
                                                <p><i class="icon-settings"></i>&nbsp; &nbsp;Setting</p>
                                            </a>
                                        </div>
                                    </div>
                                    @public
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("purchase.index") }}">
                                                    <p><i class="icon-basket"></i>&nbsp; &nbsp;Purchases</p>
                                                </a>
                                            </div>
                                        </div>
                                    @endPublic
                                    {{-- <div class="row fr-cl-nds-container-row">
                                        <div class="col">
                                            <a href="#">
                                                <p><i class="icon-like"></i>&nbsp; &nbsp;Favourite</p>
                                            </a>
                                        </div>
                                    </div> --}}
                                    @seller
                                    <div class="row fr-cl-nds-container-row">
                                        <div class="col">
                                            <a href="{{ route("order.index") }}">
                                                <p><i class="icon-chart"></i>&nbsp; Orders</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row fr-cl-nds-container-row">
                                        <div class="col">
                                            <a href="{{ route("withdraw.index") }}">
                                                <p><i class="icon-briefcase"></i>&nbsp; &nbsp;Withdrawals</p>
                                            </a>
                                        </div>
                                    </div>
                                    @endSeller


                                    @admin
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("dashboard.index") }}">
                                                    <p><i class="icon-home"></i>&nbsp; &nbsp;Dashboard</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("user.index") }}">
                                                    <p><i class="icon-user"></i>&nbsp; Users</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("order.index") }}">
                                                    <p><i class="icon-chart"></i>&nbsp; Orders</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("payment.index") }}">
                                                    <p><i class="icon-basket"></i>&nbsp; &nbsp;Payments</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("withdraw.index") }}">
                                                    <p><i class="icon-briefcase"></i>&nbsp; &nbsp;Withdrawals</p>
                                                </a>
                                            </div>
                                        </div>
                                    @endAdmin
                                    <div class="row fr-cl-nds-container-row">
                                        <div class="col">
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                                <p><i class="icon-logout"></i>&nbsp; &nbsp;Logout</p>
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endguest
                </ul>
                @auth
                    <ul class="nav navbar-nav ml-auto d-block d-lg-none">
                        <li class="nav-item d-flex align-items-center" role="presentation" id="fr-id-toggler">
                            <a class="nav-link active" href="#fr-id-side-nav" data-toggle="collapse"><i class="icon-user"></i></a>
                        </li>
                    </ul>
                @endauth
            </div>
        </div>
    </nav>

    @auth
        <section class="pulse animated collapse" id="fr-id-side-nav">
            <div class="container-fluid">
                <div class="row justify-content-end">
                    <div class="col col-8 col-sm-5 col-md-4" id="side-nav-content">
                        <span class="times">
                            <button id="close" ><i class="la la-times"></i></button>
                        </span>
                        <div class="row">
                            <div class="col p-0">
                                {{-- <div class="d-block user-details">
                                    <div class="float-left pl-3">
                                        <img src="../assets/img/usr_avatar.png">
                                    </div>
                                    <div class="float-left pl-3">
                                        <p id="fr-id-user-name">Jhon Doe</p>
                                        <p id="fr-id-user-amount" class="text-primary">$20.45</p>
                                    </div>
                                </div> --}}
                                <div class="d-block user-details">
                                    @seller
                                        <div class="float-left pl-3">
                                            <img src='{{ asset("storage/profile images/".auth()->user()->seller->profile_image) }}' width="45">
                                        </div>
                                    @endSeller
                                    <div class="float-left pl-3">
                                        <p id="fi-id-user-name">{{  auth()->user()->name }}</p>
                                        @seller
                                            <p id="fr-id-user-amount" class="text-primary">{{ $currency }} {{ $balance }}</p>
                                        @endSeller
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col side-nav-icons text-center">
                                <a class="d-inline-block" href="{{ route("notification.index") }}">
                                    <div><i class="icon-bell"></i><span class="pulse animated infinite fr-nc bg-danger">{{ count(auth()->user()->notifications) }}</span></div>
                                </a>
                                <a class="d-inline-block" href="{{ route("message.index") }}">
                                    <div><i class="icon-envelope"></i><span class="pulse animated infinite fr-nc bg-success">{{ count($chats) }}</span></div>
                                </a>
                                {{-- <a class="d-inline-block pt-2 pb-2" href="#">
                                    <div><i class="icon-basket"></i><span class="pulse animated infinite fr-nc bg-primary">5</span></div>
                                </a> --}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="fr-cl-nds-container">
                                    @seller
                                            <div class="row fr-cl-nds-container-row">
                                                <div class="col">
                                                    <a href="{{ route("account.show",[auth()->id()]) }}">
                                                        <p><i class="icon-user"></i>&nbsp; &nbsp;Profile</p>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row fr-cl-nds-container-row">
                                                <div class="col">
                                                    <a href="{{ route("dashboard.index") }}">
                                                        <p><i class="icon-home"></i>&nbsp; &nbsp;Dashboard</p>
                                                    </a>
                                                </div>
                                            </div>
                                    @endSeller
                                    <div class="row fr-cl-nds-container-row">
                                        <div class="col">
                                            <a href="{{ route("account.edit") }}">
                                                <p><i class="icon-settings"></i>&nbsp; &nbsp;Setting</p>
                                            </a>
                                        </div>
                                    </div>
                                    @public
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("purchase.index") }}">
                                                    <p><i class="icon-basket"></i>&nbsp; &nbsp;Purchases</p>
                                                </a>
                                            </div>
                                        </div>
                                    @endPublic
                                    @seller
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("withdraw.index") }}">
                                                    <p><i class="icon-briefcase"></i>&nbsp; &nbsp;Withdrawals</p>
                                                </a>
                                            </div>
                                        </div>
                                    @endSeller

                                    @admin
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("dashboard.index") }}">
                                                    <p><i class="icon-home"></i>&nbsp; &nbsp;Dashboard</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("user.index") }}">
                                                    <p><i class="icon-user"></i>&nbsp; &nbsp;Users</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("order.index") }}">
                                                    <p><i class="icon-basket"></i>&nbsp; &nbsp;Orders</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("payment.index") }}">
                                                    <p><i class="icon-basket"></i>&nbsp; &nbsp;Payments</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row fr-cl-nds-container-row">
                                            <div class="col">
                                                <a href="{{ route("withdraw.index") }}">
                                                    <p><i class="icon-briefcase"></i>&nbsp; &nbsp;Withdrawals</p>
                                                </a>
                                            </div>
                                        </div>
                                    @endAdmin

                                    <div class="row fr-cl-nds-container-row">
                                        <div class="col">
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                                <p><i class="icon-logout"></i>&nbsp; &nbsp;Logout</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @regular
                            <div class="row">
                                <div class="col pt-2">
                                    <a href="{{ route('account.one') }}" class="active d-block fr-cl-bcs text-white">Become a Seller</a>
                                </div>
                            </div>
                        @endRegular
                    </div>
                </div>
            </div>
        </section>
    @endauth

    <nav class="navbar navbar-light navbar-expand" id="home-sub-navbar">
        <div class="container"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-2"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-2">
                <ul class="nav navbar-nav mx-auto">
                    <li class="nav-item" role="presentation"><a class="nav-link {{ Route::is('index') ? 'active' : '' }}" href="/">HOME</a></li>
                    {{-- <li class="nav-item dropdown">
                        <a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#">ALL PRODUCT</a>
                        <div class="dropdown-menu pulse animated" role="menu">
                            <a class="dropdown-item" role="presentation" href="#">Product 1</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 2</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 3</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 4</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 5</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 6</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 7</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 8</a>
                        </div>
                    </li> --}}
                    {{-- <li class="nav-item dropdown">
                        <a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#">&nbsp;CATEGORIES</a>
                        <div class="dropdown-menu pulse animated" role="menu">
                            <a class="dropdown-item" role="presentation" href="#">Product 1</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 2</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 3</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 4</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 5</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 6</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 7</a>
                            <a class="dropdown-item" role="presentation" href="#">Product 8</a>
                        </div>
                    </li> --}}
                    <li class="nav-item" role="presentation"><a class="nav-link {{ Route::is('gigs') ? 'active' : '' }}" href="{{ route("gigs") }}">GIGS</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link {{ Route::is('categories') ? 'active' : '' }}" href="{{ route("categories") }}">CATEGORIES</a></li>
                </ul>
            </div>
        </div>
    </nav>


    <main class="">
        @yield('content')
    </main>



    <footer id="main-footer" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col d-flex justify-content-center align-items-center col-lg-2">
                    <h1 class="text-white">Logo</h1>
                </div>
                <div class="col col-lg-2">
                    <p class="text-white font-weight-bold mb-3">PRODUCT</p>
                    <p class="text-light font-weight-light">Payment Solution</p>
                    <p class="text-light font-weight-light">Featurees</p>
                </div>
                <div class="col col-lg-2">
                    <p class="text-white font-weight-bold mb-3">DEVELOPERS</p>
                    <p class="text-light font-weight-light">Register</p>
                    <p class="text-light font-weight-light">Login</p>
                    <p class="text-light font-weight-light">Request</p>
                </div>
                <div class="col col-lg-2">
                    <p class="text-white font-weight-bold mb-3">HELP</p>
                    <p class="text-light font-weight-light">Customer Support</p>
                    <p class="text-light font-weight-light">Contact</p>
                </div>
                <div class="col col-lg-2">
                    <p class="text-white font-weight-bold mb-3">SOCIAL</p>
                    <p class="text-light font-weight-light">Linkedin</p>
                    <p class="text-light font-weight-light">Facebook</p>
                    <p class="text-light font-weight-light">Telegram</p>
                </div>
                <div class="col col-lg-2">
                    <p class="text-white font-weight-bold mb-3">LEGAL</p>
                    <p class="text-light font-weight-light">Terms of Use</p>
                    <p class="text-light font-weight-light">Privacy Policy</p>
                    <p class="text-light font-weight-light">Legal Notice</p>
                </div>
            </div>
        </div>
    </footer>
    <section id="main-bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <p class="text-white-50">© 2018 Freelance. All right reserved. Created at Ghost</p>
                </div>
            </div>
        </div>
    </section>
</body>

<script>
        $("#close").on("click",function(){
            $("#fr-id-side-nav").collapse("hide");
        })
    </script>
</html>
