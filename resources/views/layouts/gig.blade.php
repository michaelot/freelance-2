<div class="col col-lg-3 mb-3 col-md-4 col-12">
    <div class="row">
        <div class="col">
            @auth
                @if(auth()->id() == $gig->user_id)
                    <div class="dropdown manage-items-dropdown">
                        <button class="btn btn-primary dropdown-toggle shadow-sm fr-cl-bcs text-20 mb-2" data-toggle="dropdown" aria-expanded="false" type="button">
                            <i class="la la-gear"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right shadow-lg bg-light border-0" role="menu">
                            <a class="dropdown-item text-14" href="{{ route('gig.edit',[$gig->id]) }}" href="#">
                                <i class="far fa-edit"></i>
                                &nbsp;Edit
                            </a>
                            <a class="dropdown-item text-14" role="presentation" href="{{ route("gig.show",[$gig->id]) }}">
                                <i class="far fa-eye"></i>
                                &nbsp;View
                            </a>
                            <a class="dropdown-item text-14 text-danger" role="presentation" href="{{ route("gig.delete",[$gig->id]) }}">
                                <i class="far fa-trash-alt"></i>
                                &nbsp;Delete
                            </a>
                        </div>
                    </div>
                @endif
            @endauth

            <img class="img-fluid d-block" src="{{ asset("storage/gig image/".$gig->image) }}">
            <div class="bg-white p-3 custom-box-shadow">
                <h6 class="mb-2"><a style="color: #212121" href="{{ route("gig.show",[$gig->id]) }}">{{ $gig->title }}</a></h6>
                <div class="d-flex align-items-center flex-wrap">
                    <div class="d-flex align-items-center d-inline-block mb-2 user-avatar" style="">
                        <div class="float-left">
                            <img src="{{ asset("storage/profile images/".$gig->user->seller->profile_image) }}" width="25" height="25">
                        </div>
                        <div class="float-left pl-2">
                            <p class="text-14"><a style="color:#555555" href="{{ route("account.show",[$gig->user->id]) }}">{{ $gig->user->name }}</a>&nbsp;</p>
                        </div>
                    </div>
                    <div class="mb-2" style="">
                        <p class="text-info text-14"><i class="icon-layers"></i>&nbsp;{{ $gig->category->name }}</p>
                        {{-- <p class="text-info text-14"><i class="icon-layers"></i>&nbsp;{{ $gig->gig_category_id }}</p> --}}
                    </div>
                </div>
                <p class="mt-0 mb-2 text-14">{{ substr($gig->description,0,60) }}..</p>
                <hr class="mt-0 mb-2">
                <div class="d-flex justify-content-between footer-section">
                    <span class="badge badge-primary price-badge-light mr-3">{{ $currency }} {{ $gig->basicPlan->price }} - {{ $currency }} {{ $gig->premiumPlan->price }}</span>
                    <span class="badge badge-primary price-badge-pink-light mr-3"><i class="icon-heart"></i>&nbsp; {{ count($gig->orders) }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
