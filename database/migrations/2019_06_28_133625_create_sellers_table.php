<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sellers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("profile_image");
            $table->string("cover_image");
            $table->string("user_id");
            $table->string("full_name");
            $table->mediumText('description');
            $table->string('tagline', 250);
            $table->string('country', 100);
            $table->string('language', 100);
            $table->string('website', 100)->nullable();
            $table->integer("experience");
            $table->string('occupation', 100);
            $table->string('facebook')->nullable();
            $table->string('google')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitter')->nullable();
            $table->string('github')->nullable();
            $table->string('dribble')->nullable();
            $table->string('behance')->nullable();
            $table->string('stack_overflow')->nullable();
            $table->string('vimeo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellers');
    }
}
