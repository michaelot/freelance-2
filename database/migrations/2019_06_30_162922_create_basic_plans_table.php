<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basic_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('gig_id');
            $table->string("plan_title");
            $table->mediumText('description');
            $table->integer('duration');
            $table->string('duration_type');
            $table->string('revisions')->nullable();
            $table->string('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basic_plans');
    }
}
